import { Repository } from 'typeorm';
import { Service } from 'typedi';
import { Farm } from './../models/farm';
import L from '../../common/logger';
import { getCodeError } from '../utils/errorCodes';
import { getRepository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class FarmsService {

  constructor(
    @InjectRepository(Farm)
    private farmRepository: Repository<Farm>
  ) {}

  public async getAll(): Promise<Array<Farm>> {
    L.debug('Get all farms');
    try {
      const farms: Farm[] = await this.farmRepository.find({
         relations: ["sidewalk","sidewalk.municipality","sidewalk.municipality.department"] ,
         where: {isRemove: false} 
      });
      if (farms && farms.length) {
        return farms;
      } else {
        return Promise.reject(getCodeError('FARMS_NOT_FOUND'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async getByRegisterNumber(registerNumber: string): Promise<Farm> {
    L.debug('Get farm by register number');
    try {
      const farm = await this.farmRepository.findOne({ 
          where: {registerNumber, isRemove: false}
      });
      if (farm) {
        return farm;
      } else {
        return Promise.reject(getCodeError('FARM_NOT_FOUND'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async create(farmData): Promise<Farm> {
    L.debug(farmData);
    try {
      const farmExists = await this.farmRepository.findOne({ 
        where: {registerNumber: farmData.registerNumber}
      });

      if (farmExists) {
        L.debug('Ya existe el número de registro');
        return Promise.reject(getCodeError('FARM_ALREADY_EXITS'));
      }

      L.debug('Create farm');
      const farm = new Farm(farmData);
      const farmCreated = await this.farmRepository.save(farm);
      if (farmCreated) {
        return farmCreated;
      } else {
        return Promise.reject(getCodeError('FARM_NOT_CREATED'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async update(farmRegisterNumber: string, farmData: Farm): Promise<any> {
    L.debug('Update farm');
    try {
      const farm = await this.farmRepository.findOne({
        where: {
          registerNumber: farmRegisterNumber,
          isRemove: false
        }
      });
      if(farm){
        const farmSet = evaluateFarmData(farmData);
        farm.setData(farmSet);
        const farmUpdated = await this.farmRepository.save(farm);
        if (farmUpdated) {
          return {
            updated: true
          };
        } else {
          return Promise.reject(getCodeError('FARM_NOT_UPDATED'));
        }
      } else {
        return Promise.reject(getCodeError('FARM_NOT_FOUND'));
      }

    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async remove(registerNumber: string): Promise<any> {
    L.debug('Remove farm');
    try {
      const farm = await this.farmRepository.findOne({
        where: {
          registerNumber: registerNumber, 
          isRemove: false
        }
      });
      farm.setData({ isRemove: true });
      const farmRemoved = await this.farmRepository.save(farm);
      if (farmRemoved) {
        return {
          removed: true,
        };
      } else {
        return Promise.reject(getCodeError('FARM_NOT_REMOVED'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

}

function evaluateFarmData(farm: Farm) {
  const farmSet: any = {};
  if (farm) {
    if (farm.area) {
      farmSet.area = farm.area;
    }
    if (farm.latitude) {
      farmSet.latitude = farm.latitude;
    }
    if (farm.longitude) {
      farmSet.longitude = farm.longitude;
    }
    if (farm.registerNumber) {
      farmSet.registerNumber = farm.registerNumber;
    }
    if (farm.sidewalkId) {
      farmSet.sidewalkId = farm.sidewalkId;
    }
    if (farm.ownerId) {
      farmSet.ownerId = farm.ownerId;
    }
    if (farm.name) {
      farmSet.name = farm.name;
    }
    if (farm.officeId) {
      farmSet.officeId = farm.officeId;
    }
    if (farm.ownerId) {
      farmSet.ownerId = farm.ownerId;
    }
  }
  return farmSet;
}