import { Repository } from 'typeorm';
import { Specie, SpeciesConstants } from './../models/specie';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Service } from 'typedi';
import { getCodeError } from '../utils/errorCodes';
import { log } from 'util';
@Service()
export class SpeciesService {

    constructor(
        @InjectRepository(Specie)
        private speciesRepository: Repository<Specie>
    ) {

    }

    public async getAll(): Promise<Specie[]> {
        try {
            return this.speciesRepository.find();
        } catch(error) {
            return Promise.reject(getCodeError('DB', error));
        }
    }

    public async insertInitData() {
        try {
            console.log(SpeciesConstants.species);
            await this.insertSpecie(1, SpeciesConstants.species.bovine);
            await this.insertSpecie(2, SpeciesConstants.species.bisons);
            await this.insertSpecie(3, SpeciesConstants.species.porcine);
            await this.insertSpecie(4, SpeciesConstants.species.sheepGoat);
            await this.insertSpecie(5, SpeciesConstants.species.equine);
            await this.insertSpecie(6, SpeciesConstants.species.poultry);
            await this.insertSpecie(7, SpeciesConstants.species.zoocria);
            await this.insertSpecie(8, SpeciesConstants.species.layingBirds);
            await this.insertSpecie(9, SpeciesConstants.species.fatteningBirds);
            await this.insertSpecie(10, SpeciesConstants.species.geneticMaterial);
            console.log('species created');
        } catch(error) {
            console.error(error);
        }
    }

    private async insertSpecie(id: number, name: string): Promise<Specie> {
        try {
            const specie: Specie = new Specie();
            specie.id = id;
            specie.description = name;
            specie.createdAt = new Date();
            return this.speciesRepository.save(specie);
        } catch (error) {
            console.error(error);
        }
    }
}