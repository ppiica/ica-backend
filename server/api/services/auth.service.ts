import { InjectRepository } from 'typeorm-typedi-extensions';
import { User } from './../models/user';
import { UsersService } from './users.service';
import { Utils } from './../utils/utils';
import * as jwt from 'jsonwebtoken';
import l from '../../common/logger';
import { getCodeError } from '../utils/errorCodes';
import { getRepository, Repository } from 'typeorm';
import { Service, Inject } from 'typedi';
import { sendEmail } from '../utils/email';

// const UserRepository = getRepository(User);

@Service()
export class AuthService {

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @Inject(type => UsersService)
    private usersService: UsersService
  ) { }

  public async hashPassword(password: string): Promise<any> {
    return Utils.hashText(password);
  }

  public async login(email: string, password: string): Promise<any> {
    try {
      const user = (await this.userRepository.findOne({ email, isRemove: false }));
      if (user) {
        email = email.trim().toLowerCase();
        const isAuthorized = await Utils.compareHash(password, user.password);
        l.debug(`isAuthorized: ${isAuthorized}`);
        if (isAuthorized) {
          const token = jwt.sign({ id: user.id, email: user.email }, process.env.SESSION_SECRET);
          jwt.verify(token, process.env.SESSION_SECRET, (err, decode) => {
            l.debug(`err: ${err}`);
            l.debug(`decode: ${decode}`);
          })
          delete user.password;
          return Promise.resolve({
            userId: user.email,
            token,
            user
          });
        }
      }
      return Promise.reject(getCodeError('USER_NOT_ALLOWED'));
    } catch (exception) {
      l.error(exception);
      return Promise.reject(exception);
    }
  }

  public async getUserByToken(token: string): Promise<User> {
    try {
      const decoded: any = jwt.verify(token, process.env.SESSION_SECRET);
      l.debug(JSON.stringify(decoded));
      const user = await this.userRepository.findOne({ where: { email: decoded.id } });
      console.log('user by token: \n', user)
      l.debug(user);
      return this.usersService.getById(decoded.id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  public async recoverPassword(email: string) {
    if (email) {
      try {
        const user = await this.userRepository.findOne({ where: { email, isRemove: false } });
        if (user) {
          const payload = {
            id: user.id,
            email
          };
          const secret = process.env.RECOVER_PASSWORD_SECRET;
          const token = jwt.sign(payload, secret);
          const sendHTML = `<a href="${process.env.FRONTEND_URL}/login/recoverPassword/${token}" >reset password</a>`;
          console.log(token);

          const mailRes = await sendEmail(email, 'Recover password', undefined, sendHTML);
          console.log(mailRes);

          return true;
        }
      } catch (error) {
        return Promise.reject(error);
      }
    }
  }

  public async checkTokenToResetPassword(token: string): Promise<User> {
    try {
      const secret = process.env.RECOVER_PASSWORD_SECRET;
      const decoded: any = jwt.verify(token, secret);
      if (!decoded) {
        return Promise.reject(getCodeError('INVALID_TOKEN'));
      }
      return this.usersService.getById(decoded.id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  public async resetPassword(token: string, password: string) {
    try {
      if (token) {
        const secret = process.env.RECOVER_PASSWORD_SECRET;
        const decoded: any = jwt.verify(token, secret);
        l.debug(JSON.stringify(decoded));
        const user = await this.userRepository.findOne({ where: { email: decoded.email } });
        user.password = await Utils.hashText(password);
        const userSaved = await this.userRepository.save(user);
        l.debug(user);
        if (user) {
          delete user.password;
          return Promise.resolve(user);
        } else {
          return Promise.reject(getCodeError('USER_NOT_ALLOWED'));
        }
      }
      return Promise.reject(getCodeError('NO_TOKEN_PROVIDER'));
    } catch (error) {
      return Promise.reject(error);
    }
  }

}