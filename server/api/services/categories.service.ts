import { Item } from './../models/item';
import { Service } from 'typedi';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Category } from './../models/category';
import { requirementList } from './../models/data/evaluations-array';

@Service()
export class CategoriesService {
    
    constructor(
        @InjectRepository(Category)
        private categoriesRepository: Repository<Category>,
        @InjectRepository(Item)
        private itemRepository: Repository<Item>
    ) {
        
    }
    
    public async getCategories(): Promise<Category[]> {
        return await this.categoriesRepository.find({
            where: {isRemove: false},
            join: {
                alias: "category",
                leftJoinAndSelect: {
                    "items": "category.items"
                }
            }
        }); 
    }
    
    
    public async generateCategories() {
        try {
            const sanitaryCategory = this.generateCategory(1, "SANITARY", requirementList.sanitary, requirementList.sanitary[0].maxScore, 0.3);
            const sanitationCategory = this.generateCategory(2, "SANITATION", requirementList.sanitation, requirementList.sanitation[0].maxScore, 0.2);
            const bestPracticesCategory = this.generateCategory(3, "BEST_PRACTICES", requirementList.bestPractices, requirementList.bestPractices[0].maxScore, 0.3);
            const goodnesCategory = this.generateCategory(4, "GOODNESS", requirementList.goodness, requirementList.goodness[0].maxScore, 0.2);
            await this.categoriesRepository.save([sanitaryCategory, sanitationCategory, bestPracticesCategory, goodnesCategory]);
            await this.itemRepository.save(sanitaryCategory.items);
            await this.itemRepository.save(sanitationCategory.items);
            await this.itemRepository.save(bestPracticesCategory.items);
            await this.itemRepository.save(goodnesCategory.items);
        } catch (exception) {
            console.log(exception);
        }
    }

    
    private generateCategory(categoryId: number, categoryDescription: string, items: any[], maxScore: number, categoryPercent: number): Category {
        const category: Category = new Category();
        category.description = categoryDescription;
        category.id = categoryId;
        category.maxScore = maxScore;
        category.items = [];
        category.categoryPercent = categoryPercent;
        for (let i = 0; i < items.length; i++) {
            const currentItem = items[i];
            const item = new Item();
            item.id = currentItem.id;
            item.description = currentItem.description;
            item.categoryId = categoryId;
            category.items.push(item);
        }
        return category;
    }
    
}