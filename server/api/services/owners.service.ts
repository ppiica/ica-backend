import { getRepository } from 'typeorm';
import { Utils } from './../utils/utils';
import { Owner } from './../models/owner';
import L from '../../common/logger';
import { getCodeError } from '../utils/errorCodes';
import { Service } from 'typedi';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class OwnersService {

  constructor(
    @InjectRepository(Owner)
    private ownerRepository: Repository<Owner>
  ) { }

  public async getAll(): Promise<Array<Owner>> {
    L.info('Get all owners');
    try {
      const owners: Owner[] = await this.ownerRepository.find({
        where: { isRemove: false }
      });
      if (owners && owners.length) {
        const result = [];
        for (let i = 0; i < owners.length; i++) {
          const owner = owners[i];
          L.debug(owner);
          result.push(owner);
        }

        return result;
      } else {
        return Promise.reject(getCodeError('OWNERS_NOT_FOUND'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async getById(id: string): Promise<Owner> {
    L.info('Get owner by id');
    try {
      const owner = await this.ownerRepository.findOne({
        where: { id, isRemove: false }
      });
      if (owner) {
        return owner;
      } else {
        L.error('no f')
        return Promise.reject(getCodeError('OWNER_NOT_FOUND'));
      }
    } catch (error) {
      L.error('si f', error)

      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async create(ownerData): Promise<Owner> {
    L.info('Create owner', ownerData);
    try {
      console.log(ownerData);
      const ownerExists = await this.ownerRepository.findOne({
        where: { id: ownerData.id }
      });
      L.debug(ownerExists);
      if (ownerExists) {
        return Promise.reject(getCodeError('OWNER_ALREADY_EXISTS'));
      }
      ownerData.isRemove = false;
      const owner = new Owner(ownerData);
      const ownerCreated = await this.ownerRepository.save(owner);
      if (ownerCreated) {
        return ownerCreated;
      } else {
        return Promise.reject(getCodeError('OWNER_NOT_CREATED'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async update(ownerId: string, ownerData): Promise<any> {
    L.info('Update owner');
    try {
      const owner = await this.ownerRepository.findOne({
        where: {
          id: ownerId,
          isRemove: false
        }
      });
      if (owner) {
        const ownerSet = evaluateOwnerData(ownerData);
        owner.setData(ownerSet)
        const ownerUpdated = await this.ownerRepository.save(owner);
        if (ownerUpdated) {
          return {
            updated: true,
          };
        } else {
          return Promise.reject(getCodeError('OWNER_NOT_UPDATED'));
        }
      } else {
        L.error('not found')
        return Promise.reject(getCodeError('OWNER_NOT_FOUND'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async remove(ownerId: string): Promise<any> {
    L.info('Remove owner');
    try {
      const owner = await this.ownerRepository.findOne({
        where: { id: ownerId, isRemove: false }
      });
      owner.setData({ isRemove: true })
      const ownerRemoved = await this.ownerRepository.save(owner);
      if (ownerRemoved) {
        return {
          removed: true
        };
      } else {
        return Promise.reject(getCodeError('OWNER_NOT_REMOVED'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }
}
  
function evaluateOwnerData(owner: Owner) {
  const ownerSet: any = {};
  if (owner) {
    if (owner.firstName) {
      ownerSet.firstName = owner.firstName;
    }
    if (owner.lastNameOne) {
      ownerSet.lastNameOne = owner.lastNameOne;
    }
    if (owner.lastNameTwo) {
      ownerSet.lastNameTwo = owner.lastNameTwo;
    }
    if (owner.email) {
      ownerSet.email = owner.email;
    }
    if (owner.phone) {
      ownerSet.phone = owner.phone;
    }
    if (owner.address) {
      ownerSet.address = owner.address;
    }
  }
  return ownerSet;
}