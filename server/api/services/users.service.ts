import { UserRoleForm } from "./../models/user-role-form";
import { RoleForm } from "./../models/role-form";
import { Technical } from "./../models/technical";
import { getRepository } from "typeorm";
import { Utils } from "./../utils/utils";
import { User, USER_TYPES_OBJECTS, UserConstants } from "./../models/user";
import L from "../../common/logger";
import { getCodeError } from "../utils/errorCodes";
import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { Manager } from "../models/manager";
import { Coordinator } from "../models/coordinator";
import { Role } from "../models/role";

@Service()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(Manager) private managerRepository: Repository<Manager>,
    @InjectRepository(Coordinator)
    private coordinatorRepository: Repository<Coordinator>,
    @InjectRepository(Technical)
    private technicalRepository: Repository<Technical>,
    @InjectRepository(Role) private roleRepository: Repository<Role>,
    @InjectRepository(UserRoleForm)
    private userFormsRepository: Repository<UserRoleForm>,
    @InjectRepository(RoleForm)
    private roleFormsRepository: Repository<RoleForm>
  ) { }

  public async getAll(): Promise<Array<User>> {
    L.info("Get all users");
    try {
      const users: User[] = await this.userRepository.find({
        relations: ["role"],
        where: { isRemove: false }
      });
      if (users && users.length) {
        const result = [];
        for (let i = 0; i < users.length; i++) {
          const user = users[i];
          await this.setUserData(user);
          L.debug(user);
          result.push(user);
        }

        return result;
      } else {
        return Promise.reject(getCodeError("USERS_NOT_FOUND"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }

  public async getById(id: string): Promise<User> {
    L.info("Get user by id", id);
    try {
      const user = await this.userRepository.findOne({
        relations: ["role"],
        where: { id, isRemove: false }
      });
      if (user) {
        await this.setUserData(user);
        return user;
      } else {
        L.error("no f");
        return Promise.reject(getCodeError("USER_NOT_FOUND"));
      }
    } catch (error) {
      L.error("si f", error);

      return Promise.reject(getCodeError("DB", error));
    }
  }

  public async create(userData): Promise<User> {
    L.info("Create user", userData);
    try {
      const userExists = await this.userRepository.findOne({
        where: { id: userData.id }
      });
      L.debug(userExists);
      if (userExists) {
        return Promise.reject(getCodeError("USER_ALREADY_EXISTS"));
      }
      userData.password = await Utils.hashText(userData.password);
      userData.email = userData.email.trim().toLowerCase();
      userData.isRemove = false;
      const user = new User(userData);
      const userCreated = await this.userRepository.save(user);
      if (userCreated) {
        delete userCreated.password;
        await this.setUserProfile(user.profile, user.roleId);
        await this.setPermissions(userCreated);
        return userCreated;
      } else {
        return Promise.reject(getCodeError("USER_NOT_CREATED"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }

  public async update(userId: string, userData): Promise<any> {
    L.info("Update user");
    try {
      const user = await this.userRepository.findOne({
        where: {
          id: userId,
          isRemove: false
        }
      });
      if (user) {
        const userSet = evaluateUserData(userData);
        user.setData(userSet);
        const userUpdated = await this.userRepository.save(user);
        if (userUpdated) {
          await this.setUserProfile(userData.profile, user.roleId);
          await this.setPermissions(userUpdated);
          return {
            updated: true
          };
        } else {
          return Promise.reject(getCodeError("USER_NOT_UPDATED"));
        }
      } else {
        L.error("not found");
        return Promise.reject(getCodeError("USER_NOT_FOUND"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }

  public async remove(userId: string): Promise<any> {
    L.info("Remove user");
    try {
      const user = await this.userRepository.findOne({
        where: { id: userId, isRemove: false }
      });
      user.setData({ isRemove: true });
      const userRemoved = await this.userRepository.save(user);
      if (userRemoved) {
        return {
          removed: true
        };
      } else {
        return Promise.reject(getCodeError("USER_NOT_REMOVED"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }

  public getTypes(): Array<any> {
    return USER_TYPES_OBJECTS;
  }

  private async getUserProfile(
    userId: number,
    roleId: number
  ): Promise<Manager | Coordinator | Technical> {
    let profile: Manager | Coordinator | Technical;
    const role: Role = await this.roleRepository.findOne({ id: roleId });
    try {
      switch (role.description.toUpperCase()) {
        case UserConstants.ROLE_MANAGER:
          profile = await this.managerRepository.findOne({
            where: { id: userId, isRemove: false }
          });
          break;
        case UserConstants.ROLE_COORDINATOR:
          profile = await this.coordinatorRepository.findOne({
            where: { id: userId, isRemove: false }
          });
          break;
        case UserConstants.ROLE_TECHNICIAN:
          profile = await this.technicalRepository.findOne({
            where: { id: userId, isRemove: false }
          });
          break;
        default:
          profile = await this.managerRepository.findOne({
            where: { id: userId, isRemove: false }
          });
          break;
      }
      return profile;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  private async setUserProfile(
    profileData: Manager | Coordinator | Technical,
    roleId: number
  ): Promise<Manager | Coordinator | Technical> {
    let profile: Manager | Coordinator | Technical;
    const role: Role = await this.roleRepository.findOne({ id: roleId });
    try {
      switch (role.description.toUpperCase()) {
        case UserConstants.ROLE_MANAGER:
          if (profileData.id) {
            let existingProfile = await this.managerRepository.findOne({
              where: { id: profileData.id, isRemove: false }
            });
            if (!existingProfile) {
              existingProfile = new Manager();
            }
            existingProfile.setData(profileData as Manager);
            profile = await this.managerRepository.save(existingProfile);
          } else {
            profile = await this.managerRepository.save(profileData);
          }
          break;
        case UserConstants.ROLE_COORDINATOR:
          if (profileData.id) {
            let existingProfile = await this.coordinatorRepository.findOne({
              where: { id: profileData.id, isRemove: false }
            });
            if (!existingProfile) {
              existingProfile = new Coordinator();
            }
            existingProfile.setData(profileData as Coordinator);
            profile = await this.coordinatorRepository.save(existingProfile);
          } else {
            profile = await this.coordinatorRepository.save(profileData);
          }
          break;
        case UserConstants.ROLE_TECHNICIAN:
          if (profileData.id) {
            let existingProfile = await this.technicalRepository.findOne({
              where: { id: profileData.id, isRemove: false }
            });
            if (!existingProfile) {
              existingProfile = new Technical();
            }
            existingProfile.setData(profileData as Technical);
            profile = await this.technicalRepository.save(existingProfile);
          } else {
            profile = await this.technicalRepository.save(profileData);
          }
          break;
        default:
          profile = await this.managerRepository.save(profileData);
          break;
      }
      return profile;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  private async setUserData(user) {
    user.profile = await this.getUserProfile(user.id, user.roleId);
    let permissions = await this.userFormsRepository.find({
      where: { userEmail: user.email },
      relations: ["form"]
    });
    user.UserRoleForm = permissions.map<any>(permission => {
      delete permission.createdAt;
      delete permission.updatedAt;
      delete permission.user;
      delete permission.userEmail;
      delete permission.form.roleForms;
      delete permission.form.createdAt;
      delete permission.form.updatedAt;
      delete permission.form.id;
      return permission;
    });
  }

  private async setPermissions(user: User): Promise<UserRoleForm[]> {
    const forms = await this.roleFormsRepository.find({
      where: { roleId: user.roleId }
    });
    const currentUserForms = await this.userFormsRepository.find({
      where: { userEmail: user.email }
    });
    let userForms = forms.map((form): UserRoleForm => {
      let userRoleForm = currentUserForms.find(f => f.formId === form.formId);
      if (!userRoleForm) {
        userRoleForm = new UserRoleForm();
        userRoleForm.userEmail = user.email;
        userRoleForm.formId = form.formId;
      }
      userRoleForm.delete = form.delete;
      userRoleForm.update = form.delete;
      userRoleForm.create = form.create;
      return userRoleForm;
    });
    userForms = await this.userFormsRepository.save(userForms);
    console.log(userForms);
    return userForms;
  }
}

function evaluateUserData(user: User) {
  const userSet: any = {};
  if (user) {
    // if (user.firstName) {
    //   userSet.firstName = user.firstName;
    // }
    // if (user.lastNameOne) {
    //   userSet.lastNameOne = user.lastNameOne;
    // }
    // if (user.lastNameTwo) {
    //   userSet.lastNameTwo = user.lastNameTwo;
    // }
    // if (user.email) {
    //   userSet.email = user.email;
    // }
    // // if (typeof user.idUserType === 'number') {
    // //   userSet.idUserType = user.idUserType;
    // // }
    // if (user.phone) {
    //   userSet.phone = user.phone;
    // }
    // if (user.officeId) {
    //   userSet.officeId = user.officeId;
    // }
    // if (user.techniciansIds) {
    //   userSet.techniciansIds = user.techniciansIds;
    // }
  }
  return userSet;
}
