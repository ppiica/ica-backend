import { Repository } from 'typeorm';
import { Role } from './../models/role';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Service } from 'typedi';

@Service()
export class RolesService {

    constructor(
        @InjectRepository(Role)
        private rolesRepository: Repository<Role>){
    }

    public async getAll(): Promise<Role[]> {
        return this.rolesRepository.find();
    }

}