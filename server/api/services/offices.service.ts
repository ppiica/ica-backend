import { municipalitiesArray } from './../models/data/municipality-array';
import { departmentsArray } from './../models/data/departments-array';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Service } from 'typedi';
import { getRepository } from 'typeorm';
import L from '../../common/logger';
import { Department } from '../models/department';
import { Municipality } from '../models/municipality';
import { Sidewalk } from '../models/sidewalk';
import { sidewalksArray } from '../models/data/sidewalk-array';
import { Office } from '../models/office';
import { getCodeError } from '../utils/errorCodes';

@Service()
export class OfficesService {

  constructor(
    @InjectRepository(Office)
    private officeRepository: Repository<Office>) {
  }

  public async getOffices(): Promise<Array<Office>>  {
    L.info('Get all offices');
    try {
      const offices: Office[] = await this.officeRepository.find({
        where: { isRemove: false }
      });
      if (offices && offices.length) {
        const result = [];
        for (let i = 0; i < offices.length; i++) {
          const office = offices[i];
          L.debug(office);
          result.push(office);
        }
        return result;
      } else {
        return Promise.reject(getCodeError('OFFICES_NOT_FOUND'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async getById(id: string): Promise<Office> {
    L.info('Get office by id');
    try {
      const office = await this.officeRepository.findOne({
        where: { id, isRemove: false }
      });
      if (office) {
        return office;
      } else {
        L.error('no f')
        return Promise.reject(getCodeError('OFFICE_NOT_FOUND'));
      }
    } catch (error) {
      L.error('si f', error)

      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async create(officeData): Promise<Office> {
    L.info('Create office', officeData);
    try {
      console.log(officeData);
      const officeExists = await this.officeRepository.findOne({
        where: { id: officeData.id }
      });
      L.debug(officeExists);
      if (officeExists) {
        return Promise.reject(getCodeError('OFFICE_ALREADY_EXISTS'));
      }
      officeData.isRemove = false;
      const office = new Office(officeData);
      const officeCreated = await this.officeRepository.save(office);
      if (officeCreated) {
        return officeCreated;
      } else {
        return Promise.reject(getCodeError('OFFICE_NOT_CREATED'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async update(officeId: string, officeData): Promise<any> {
    L.info('Update owner');
    try {
      const office = await this.officeRepository.findOne({
        where: {
          id: officeId,
          isRemove: false
        }
      });
      if (office) {
        const officeSet = evaluateOfficeData(officeData);
        office.setData(officeSet)
        const officeUpdated = await this.officeRepository.save(office);
        if (officeUpdated) {
          return {
            updated: true,
          };
        } else {
          return Promise.reject(getCodeError('OFFICE_NOT_UPDATED'));
        }
      } else {
        L.error('not found')
        return Promise.reject(getCodeError('OFFICE_NOT_FOUND'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }

  public async remove(officeId: string): Promise<any> {
    L.info('Remove owner');
    try {
      const office = await this.officeRepository.findOne({
        where: { id: officeId, isRemove: false }
      });
      office.setData({ isRemove: true })
      const ownerRemoved = await this.officeRepository.save(office);
      if (ownerRemoved) {
        return {
          removed: true
        };
      } else {
        return Promise.reject(getCodeError('OFFICE_NOT_REMOVED'));
      }
    } catch (error) {
      return Promise.reject(getCodeError('DB', error));
    }
  }
}

function evaluateOfficeData(office: Office) {
  const officeSet: any = {};
  if (office) {
    if (office.name) {
      officeSet.name = office.name;
    }
  }
  return officeSet;
}