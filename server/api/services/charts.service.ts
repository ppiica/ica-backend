import { Qualification } from "./../models/qualification";
import { VisitStatus, VisitStatusConstants } from "./../models/visitStatus";
import { Repository } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import { getRepository } from "typeorm";
import { Visit } from "./../models/visit";
import L from "../../common/logger";
import { getCodeError } from "../utils/errorCodes";
import { Service } from "typedi";
import { Farm } from "../models/farm";
import { Utils } from "../utils/utils";

@Service()
export class ChartService {

    constructor(
        @InjectRepository(Visit)
        private visitRepository: Repository<Visit>,
    ) { }

    public async getTopTenChart(descriptionStatus: string): Promise<Visit[]> {
        try {
            const chartData = await getRepository(Visit)
                .createQueryBuilder("visit")
                .select("COUNT(*)", "quantity_farms_approved")
                .addSelect("MONTHNAME(visit.date)", "date")
                .addSelect("department.name")
                .innerJoin("visit.farm", "farm")
                .innerJoin("visit.status", "status")
                .innerJoin("farm.sidewalk", "sidewalk")
                .innerJoin("sidewalk.municipality", "municipality")
                .innerJoin("municipality.department", "department")
                .where("status.description = :description", { description: descriptionStatus })
                .groupBy("department.name")
                .addGroupBy("date_format(visit.date, '%m-%Y')")
                .orderBy("visit.date")
                .addOrderBy("department.name")
                .getRawMany();

            return this.structureDataForChart(chartData);
        } catch (error) {
            return Promise.reject(getCodeError('DB'));
        }
    }

    public async getTotalVisitsByStateChart(): Promise<Visit[]> {
        try {
            const chartData = await getRepository(Visit)
                .createQueryBuilder("visit")
                .select("COUNT(*)", "value")
                .addSelect("status.description", "name")
                .innerJoin("visit.status", "status")
                .groupBy("visit.statusId")
                .getRawMany();

            return chartData;
        } catch (error) {
            return Promise.reject(getCodeError('DB'));
        }
    }

    public structureDataForChart(chartData) {
        let array = [];
        let result = chartData.reduce(function (r, a) {
            r[a.date] = r[a.date] || [];
            r[a.date].push({
                name: a.department_name,
                value: parseInt(a.quantity_farms_approved)
            });
            return r;
        }, Object.create(null));

        for (var i in result) {
            let structureChart = {};
            structureChart['name'] = i.toUpperCase();
            structureChart['series'] = result[i];
            array.push(structureChart);
        }
        return array;
    }
}
