import { Repository } from 'typeorm';
import { Coordinator } from './../models/coordinator';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Technical } from '../models/technical';
import { getCodeError } from '../utils/errorCodes';
import L from '../../common/logger';

export class CoordinatorsService {
    
    constructor(
        @InjectRepository(Coordinator) private coordinatorsRepository: Repository<Coordinator>,
        @InjectRepository(Technical) private technicalsRepository: Repository<Technical>,
    ){
        
    }
    
    public async assignTechnicals(coordinatorId: string, technicalsIds: string[]): Promise<{coordinatorId: string, technicals: Technical[]}> {
        try {
            await this.technicalsRepository.createQueryBuilder()
                .update(Technical)
                .set({coordinatorId: null})
                .where("coordinatorId = :coordinatorId", {coordinatorId: coordinatorId})
                .execute();
            for (let i = 0; i < technicalsIds.length; i++) {
                const id = technicalsIds[i];
                const technical: Technical = await this.technicalsRepository.findOne({id: id});
                technical.coordinatorId = coordinatorId;
                await this.technicalsRepository.save(technical);
            }
            const technicals = await this.getTechnicalsByCoordinator(coordinatorId);
            return { coordinatorId, technicals };
        } catch (error) {
            console.error(error);
            return Promise.reject(getCodeError('DB'));
        }
    }

    public async getTechnicalsByCoordinator(coordinatorId: string): Promise<Technical[]>{
        try {
            const technicals = await this.technicalsRepository.find({coordinatorId: coordinatorId});
            return technicals;
        } catch (error) {
            return Promise.reject(getCodeError('DB'));
        }
    }
    
}