import { Category } from './../models/category';
import { ProductiveActivity } from './../models/productive-activity';
import { Qualification } from "./../models/qualification";
import { VisitStatus, VisitStatusConstants } from "./../models/visitStatus";
import { Repository, In } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import { getRepository } from "typeorm";
// import { FarmRepository } from './../models/farm';
import { Visit } from "./../models/visit";
import L from "../../common/logger";
import { getCodeError } from "../utils/errorCodes";
import { Service } from "typedi";
import { Farm } from "../models/farm";
import { Utils } from "../utils/utils";
import { Item } from '../models/item';

// const VisitRepository = getRepository(Visit);

@Service()
export class VisitsService {

  private static readonly joinData = {
    alias: "visit",
    leftJoinAndSelect: {
      farm: "visit.farm",
      owner: "farm.owner",
      sidewalk: "farm.sidewalk",
      municipality: "sidewalk.municipality",
      department: "municipality.department",
      technical: "visit.technical",
      status: "visit.status",
      qualifications: "visit.qualifications",
      productiveActivities: "visit.productiveActivities",
      item: "qualifications.item",
      specie: "productiveActivities.specie"
    }
  };

  constructor(
    @InjectRepository(Visit) private visitRepository: Repository<Visit>,
    @InjectRepository(VisitStatus) private visitStatusRepository: Repository<VisitStatus>,
    @InjectRepository(Farm) private farmsRepository: Repository<Farm>,
    @InjectRepository(Qualification) private qualificationsRepository: Repository<Qualification>,
    @InjectRepository(ProductiveActivity) private productiveActivityRepository: Repository<ProductiveActivity>,
    @InjectRepository(Item) private itemsRepository: Repository<Item>,
    @InjectRepository(Category) private categoriesRepository: Repository<Category>
  ) {}

  public async getAll(): Promise<Array<Visit>> {
    L.info("Get all visits");
    try {
      const visits: Visit[] = await this.visitRepository.find({
        where: { isRemove: false },
        join: VisitsService.joinData
      });
      if (visits && visits.length) {
        return visits;
      } else {
        return Promise.reject(getCodeError("VISITS_NOT_FOUND"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }

  public async getById(id: string): Promise<Visit> {
    L.info("Get visit by id");
    try {
      const visit = await this.visitRepository.findOne({
        where: { id, isRemove: false },
        join: VisitsService.joinData
      });
      if (visit) {
        return await visit;
      } else {
        L.error("no f");
        return Promise.reject(getCodeError("UNKNOWN_ERROR"));
      }
    } catch (error) {
      L.error("si f", error);

      return Promise.reject(getCodeError("DB", error));
    }
  }

  public async getVisitsByTechnical(technicalId: number): Promise<Visit[]> {
    try {
      console.log(arguments);
      const status: VisitStatus = await this.getStatusData(VisitStatusConstants.STATUS_IN_PROCESS);
      const visits: Visit[] = await this.visitRepository.find({
        where: { isRemove: false, statusId: status.id, technicalId: technicalId },
        join: VisitsService.joinData
      });
      return visits;
    } catch (error) {
      return Promise.reject(getCodeError('DB'));
    }
  }

  public async create(visitData): Promise<Visit> {
    L.info("Create visit");
    try {
      L.debug(JSON.stringify(visitData));
      const visitStatus: VisitStatus = await this.getStatusData(VisitStatusConstants.STATUS_IN_PROCESS);
      visitData.statusId = visitStatus.id;
      const visit = new Visit(visitData);
      console.log(visit);
      console.log(visitData);
      const visitCreated = await this.visitRepository.save(visitData);
      if (visitCreated) {
        return visitCreated;
      } else {
        return Promise.reject(getCodeError("UNKNOWN_ERROR"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }

  public async update(visitId: string, visitData: Visit): Promise<any> {
    L.info("Update visit");
    try {
      const visitSet = evaluateVisitData(visitData);
      const visit = await this.visitRepository.findOne({
        where: {
          id: visitId,
          isRemove: false
        }
      });
      visit.setData(visitSet);
      const visitUpdated = await this.visitRepository.save(visit);
      if (visitUpdated) {
        return {
          updated: true
        };
      } else {
        return Promise.reject(getCodeError("UNKNOWN_ERROR"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }



  public async finishVisit(visitId: number, VisitData: Visit) {
    try {
      const visitSet = evaluateVisitData(VisitData);
      const evaluation: number = await this.evaluateQualifications(visitSet.qualifications);
      const visitStatus: VisitStatus = await this.getStatusByEvaluation(evaluation);;
      visitSet.statusId = visitStatus.id;
      const currentVisit: Visit = await this.visitRepository.findOne({ id: visitId });
      await this.saveFiles(visitSet);
      currentVisit.setData(visitSet);
      const finishedVisit: Visit = await this.visitRepository.save(currentVisit);
      await this.saveQualifications(visitSet.qualifications, visitId);
      await this.saveProductiveActivities(visitSet.productiveActivities, visitId);
      if (finishedVisit) {
        return await this.visitRepository.findOne({
          where: { id: visitId },
          join: {
            alias: "visit",
            leftJoinAndSelect: {
              farm: "visit.farm",
              owner: "farm.owner",
              technical: "visit.technical",
              productiveActivities: "visit.productiveActivities",
              qualifications: "visit.qualifications",
              status: "visit.status"
            }
          }
        });
      }
    } catch (exception) {
      return Promise.reject(getCodeError("DB", exception));
    }
  }

  public async remove(visitId: number): Promise<any> {
    L.info("Remove visit");
    try {
      const visit = await this.visitRepository.findOne({
        where: {
          id: visitId,
          isRemove: false
        }
      });
      visit.setData({ isRemove: true });
      visit.isRemove = true;
      const visitRemoved = await this.visitRepository.save(visit);
      if (visitRemoved) {
        return {
          removed: true
        };
      } else {
        return Promise.reject(getCodeError("UNKNOWN_ERROR"));
      }
    } catch (error) {
      return Promise.reject(getCodeError("DB", error));
    }
  }

  private async saveQualifications(qualifications: Qualification[], visitId: number): Promise<Qualification[]> {
    await this.qualificationsRepository.delete({visitId: visitId});
    qualifications.forEach( qualification => {
        qualification.visitId = visitId;
    });
    return this.qualificationsRepository.save(qualifications);
  }

  private async saveProductiveActivities(productiveActivities: ProductiveActivity[], visitId: number): Promise<ProductiveActivity[]> {
    await this.productiveActivityRepository.delete({visitId: visitId});
    productiveActivities.forEach( productiveActivity => {
        productiveActivity.visitId = visitId;
    });
    return this.productiveActivityRepository.save(productiveActivities);
  }

  // private async prepareVisits(visits: Visit[]): Promise<Visit[]> {
  //   const visitResults = [];
  //   for (let i = 0; i < visits.length; i++) {
  //     const visit: any = visits[i];
  //     L.debug(visit.farmRegisterNumber);
  //     const farm = await FarmRepository.findOne({ registerNumber: visit.farmRegisterNumber });
  //     L.debug('farm', farm);
  //     visit.farm = farm;
  //     visitResults.push(visit);
  //   }
  //   L.debug(visitResults);
  //   return visitResults;
  // }

  private async saveFiles(visit: Visit): Promise<any> {
    try {
      if (visit.qualifications) {
        await this.saveQualificationsFiles(visit.qualifications, visit.id);
      }
      if (visit.ownerFarmSign) {
        console.log('ownerFarmSign')
        visit.ownerFarmSign = await Utils.saveFile(visit.ownerFarmSign, `${visit.id}`, Utils.SIGNATURES_FOLDER);
      }
      if (visit.inspectorSign) {
        visit.inspectorSign = await Utils.saveFile(visit.inspectorSign, `${visit.id}`, Utils.SIGNATURES_FOLDER);
      }
    } catch (error) {
      return Promise.reject(error);
    }
    return visit;
  }

  private async saveQualificationsFiles(qualifications: Qualification[], visitId: number): Promise<any> {
    for (let i: number = 0; i < qualifications.length; i++) {
      const qualification = qualifications[i];
      if (qualification.evidence) {
        try {
          qualification.evidence = await Utils.saveFile(qualification.evidence, `${visitId}`, Utils.EVIDENCES_FOLDER);
        } catch (error) {
          console.error("couldn't save evidence");
          return Promise.reject(error);
        }
      }
    }
    return qualifications;
  }

  private async getStatusData(statusDescription: string): Promise<VisitStatus> {
    try {
      const visitStatus = await this.visitStatusRepository.findOne(
        { description: statusDescription }
      );
      return visitStatus;
    } catch (error) {
      return Promise.reject(getCodeError('DB'));
    }
  }

  private async evaluateQualifications(qualifications: Qualification[]): Promise<number> {
    const itemsIds: number[] = qualifications.map(qualification => qualification.itemId);
    const items: Item[] = await this.itemsRepository.find({id: In(itemsIds)});
    const categoriesIds: number[] = items.map(item => item.categoryId);
    const categories: Category[] = await this.categoriesRepository.find({id: In(categoriesIds)});
    let sum = 0;
    let total = 0;
    items.forEach(item => {
      const category = categories.find(category => category.id === item.categoryId);
      total += category.maxScore * category.categoryPercent;
      sum += qualifications.find(qualification => qualification.itemId === item.id).score * category.categoryPercent;
    });
    return Math.floor(sum * 100 / total);
  }

  private async getStatusByEvaluation(evaluation: number): Promise<VisitStatus> {
    let statusDescription: string;
    if (evaluation <= 59) {
      statusDescription = VisitStatusConstants.STATUS_NOT_APROVED;
    } else if (evaluation <= 79){
      statusDescription = VisitStatusConstants.STATUS_CONDITIONED;
    } else {
      statusDescription = VisitStatusConstants.STATUS_APROVED;
    }
    return await this.getStatusData(statusDescription);
  }

}


function evaluateVisitData(visit: Visit) {
  const visitSet: Visit = new Visit();
  if (visit) {
    if (visit.id) {
      visitSet.id = visit.id;
    }
    if (visit.date) {
      visitSet.date = visit.date;
    }
    if (visit.nextVisitDate) {
      visitSet.nextVisitDate = visit.nextVisitDate;
    }
    if (visit.observations) {
      visitSet.observations = visit.observations;
    }
    if (visit.gradualCompliancePlan) {
      visitSet.gradualCompliancePlan = visit.gradualCompliancePlan;
    }
    if (visit.ownerFarmSign) {
      visitSet.ownerFarmSign = visit.ownerFarmSign;
    }
    if (visit.inspectorSign) {
      visitSet.inspectorSign = visit.inspectorSign;
    }
    if (visit.farmRegisterNumber) {
      visitSet.farmRegisterNumber = visit.farmRegisterNumber;
    }
    if (visit.statusId) {
      visitSet.statusId = visit.statusId;
    }
    if (visit.technicalId) {
      visitSet.technicalId = visit.technicalId;
    }
    if (visit.personWhoAttendsId) {
      visitSet.personWhoAttendsId = visit.personWhoAttendsId;
    }
    if (visit.personWhoAttendsName) {
      visitSet.personWhoAttendsName = visit.personWhoAttendsName;
    }
    if (visit.personWhoAttendsPhone) {
      visitSet.personWhoAttendsPhone = visit.personWhoAttendsPhone;
    }
    if (visit.productiveActivities) {
      visitSet.productiveActivities = visit.productiveActivities;
    }
    if (visit.qualifications) {
      visitSet.qualifications = visit.qualifications;
    }
    // if (visit.qualificationArray) {
    //   visitSet.qualificationArray = visit.qualificationArray;
    // }
    // if (visit.productiveActivityArray) {
    //   visitSet.productiveActivityArray = visit.productiveActivityArray;
    // }
  }
  return visitSet;
}