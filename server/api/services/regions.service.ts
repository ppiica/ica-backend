import { municipalitiesArray } from './../models/data/municipality-array';
import { departmentsArray } from './../models/data/departments-array';
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Service } from 'typedi';
import { getRepository } from 'typeorm';
import L from '../../common/logger';
import { Department } from '../models/department';
import { Municipality } from '../models/municipality';
import { Sidewalk } from '../models/sidewalk';
import { sidewalksArray } from '../models/data/sidewalk-array';

@Service()
export class RegionsService {
  
  constructor(
    @InjectRepository(Department)
    private departmentRepository: Repository<Department>,
    @InjectRepository(Municipality)
    private municipalityRepository: Repository<Municipality>,
    @InjectRepository(Sidewalk)
    private sidewalkRepository: Repository<Sidewalk>,
  ) {
  }
  
  public async getDepartments() {
    return await this.departmentRepository.find({
      where: { isRemove: false }
    });
  }
  
  public async getMunicipalitiesByDepartmentId(departmentId: string) {
    return await this.municipalityRepository.find({
      where: { isRemove: false, departmentId: departmentId }
    });
  }
  
  public async getSideWalksByMunicipalityId(municipalityId: string) {
    return await this.sidewalkRepository.find({
      where: { isRemove: false, municipalityId: municipalityId }
    });
  }
  
  public async insertInitData() {
    try {
      const oneDepartment = await this.departmentRepository.findOne();
      if (!oneDepartment) {
        await this.departmentRepository.insert(departmentsArray);
        const municipalitiesMapped = municipalitiesArray.map<any>((m) => {
          return { id: m.id, departmentId: m.departmentId, name: m.name };
        });
        await this.municipalityRepository.insert(municipalitiesMapped);
        this.insertSideWalks();
        return;
      }
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  }
  
  
  private async insertSideWalks() {
    for(var i: number = 0; i < sidewalksArray.length; i++) {
        const sidewalkData = sidewalksArray[i];
        const sidewalk = new Sidewalk();
        sidewalk.id = sidewalkData.id;
        sidewalk.name = sidewalkData.name;
        sidewalk.municipalityId = sidewalkData.municipalityId;
        try {
          await this.sidewalkRepository.save(sidewalk);
        } catch(err) {
          console.error(err);
        }
      }
      console.log(`sidewalks inserted`);
  }
  
}