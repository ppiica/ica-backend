import { VisitsService } from './visits.service';
import { Visit } from './../models/visit';
import { Technical } from './../models/technical';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Service, Container } from "typedi";
import { Repository } from 'typeorm';

@Service()
export class TechnicalsService {

    constructor (
        @InjectRepository(Technical)
        private technicalsRepository: Repository<Technical>,
    ) {
    }

    public async getAllTechnicals() {
        return this.technicalsRepository.find();
    }

}