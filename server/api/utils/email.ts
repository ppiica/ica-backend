import * as nodemailer from 'nodemailer';

export async function sendEmail(to, subject, contentEmail?, html?) {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD
    }
  });
  const mailOptions = {
    from: 'PPI-ICA <' + process.env.EMAIL_ADDRESS + '>',
    to: to,
    subject,
    text: contentEmail,
    html
  };
  try {
    const sended = await transporter.sendMail(mailOptions);
    console.log(sended);
  } catch (error) {
    return Promise.reject(error);
  }
}
