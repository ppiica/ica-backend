import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import l from "../../common/logger";
import * as mime from "mime";
import * as fs from "fs";

export class Utils {
  public static readonly UPLOADS_FOLDER = "./public/uploads";
  public static readonly VISITS_FOLDER = Utils.UPLOADS_FOLDER + "/visits";
  public static readonly EVIDENCES_FOLDER = "evidences";
  public static readonly SIGNATURES_FOLDER = "signatures";

  public static hashText(plainText: string): Promise<any> {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(8, (saltErr, salt) => {
        if (saltErr) {
          reject(saltErr);
        } else {
          bcrypt.hash(plainText, salt, (hashErr, hash) => {
            if (hashErr) {
              reject(hashErr);
            } else {
              resolve(hash);
            }
          });
        }
      });
    });
  }

  public static compareHash(plainText: string, hash: string): Promise<any> {
    return new Promise((resolve, reject) => {
      bcrypt.compare(plainText, hash, (err, res) => {
        if (err) {
          l.error(err);
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
  }

  public static async saveFile(base64: string, visitId: string, folder: string): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      try {
        const visitFolder = await Utils.generateVisitFolder(visitId);
        const imageBuffer = this.decodeBase64Image(base64);
        const extension = mime.getExtension(imageBuffer.type);
        const filepath: string = `${visitFolder}/${folder}/${new Date().getTime()}.${extension}`;
        fs.writeFile(filepath, imageBuffer.data, err => {
          if (err) {
            reject(err);
          } else {
            resolve(filepath);
          }
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  public static decodeBase64Image(dataString): { type: string; data: Buffer } {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

    if (matches.length !== 3) {
      throw new Error("Invalid String format");
    }

    const response = {
      type: matches[1],
      data: new Buffer(matches[2], "base64")
    };

    return response;
  }

  private static async generateVisitFolder(visitId: string): Promise<string> {
    try {
      const uploadsFolder = await this.generateFolder(Utils.UPLOADS_FOLDER);
      const visitsFolder = await this.generateFolder(Utils.VISITS_FOLDER);
      const currentVisitFolder = await this.generateFolder(`${Utils.VISITS_FOLDER}/${visitId}`);
      const evidencesFolder = await this.generateFolder(`${currentVisitFolder}/${Utils.EVIDENCES_FOLDER}`);
      const signaturesFolder = await this.generateFolder(`${currentVisitFolder}/${Utils.SIGNATURES_FOLDER}`);
      console.log(currentVisitFolder);
      return currentVisitFolder;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  public static async generateFolder(folder: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const dir = folder;
      fs.exists(dir, (exist: boolean) => {
        if (!exist) {
          fs.mkdir(dir, err => {
            if (err) {
              reject(err);
            } else {
              resolve(dir);
            }
          });
        } else {
          resolve(dir);
        }
      });
    });
  }
}
