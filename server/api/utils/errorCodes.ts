import L from '../../common/logger';
import { Response } from 'express';

export const ERROR_CODES = {
  UNKNOWN_ERROR: {
    code: 0,
    httpStatus: 500,
    message: 'Unknown Error'
  },

  DB: {
    code: 100,
    httpStatus: 500,
    message: 'Database error'
  },
  USER_NOT_FOUND: {
    code: 200,
    httpStatus: 404,
    message: 'User not found'
  },
  USERS_NOT_FOUND: {
    code: 201,
    httpStatus: 404,
    message: 'Users not found'
  },
  USER_NOT_CREATED: {
    code: 202,
    httpStatus: 400,
    message: 'User not created'
  },
  USER_NOT_UPDATED: {
    code: 203,
    httpStatus: 400,
    message: 'Users not updated'
  },
  USER_NOT_REMOVED: {
    code: 204,
    httpStatus: 400,
    message: 'Users not removed'
  },
  USER_ALREADY_EXISTS: {
    code: 204,
    httpStatus: 409,
    message: 'Users already exists'
  },
  FARM_NOT_FOUND: {
    code: 301,
    httpStatus: 404,
    message: 'Farm not found'
  },
  FARMS_NOT_FOUND: {
    code: 302,
    httpStatus: 404,
    message: 'Farms not found'
  },
  FARM_NOT_REMOVED: {
    code: 204,
    httpStatus: 400,
    message: 'Farm not removed'
  },
  FARM_NOT_UPDATED: {
    code: 205,
    httpStatus: 400,
    message: 'Farm not updated'
  },
  FARM_ALREADY_EXITS: {
    code: 303,
    httpStatus: 410,
    message: 'Farms already exists'
  },
  PERSON_NOT_UPDATED: {
    code: 401,
    httpStatus: 400,
    message: 'Farm not updated'
  },
  USER_NOT_ALLOWED: {
    code: 303,
    httpStatus: 401,
    message: 'User not allowed'
  },
  VISITS_NOT_FOUND: {
    code: 501,
    httpStatus: 404,
    message: 'Visits not found'
  },
  OWNERS_NOT_FOUND: {
    code: 405,
    httpStatus: 404,
    message: 'Owners not found'
  },
  OWNER_NOT_FOUND: {
    code: 406,
    httpStatus: 404,
    message: 'Owner not found'
  },
  OWNER_ALREADY_EXISTS: {
    code: 407,
    httpStatus: 410,
    message: 'Owner already exists'
  },
  OWNER_NOT_CREATED: {
    code: 408,
    httpStatus: 400,
    message: 'Owner not created'
  },
  OWNER_NOT_UPDATED: {
    code: 409,
    httpStatus: 400,
    message: 'Owner nor updated'
  },
  OWNER_NOT_REMOVED: {
    code: 410,
    httpStatus: 400,
    message: 'Owner not removed'
  },  
  INVALID_TOKEN: {
    code: 622,
    httpStatus: 402,
    message: 'Invalid token'
  },
  OFFICE_NOT_FOUND: {
    code: 406,
    httpStatus: 404,
    message: 'office not found'
  },
  OFFICE_ALREADY_EXISTS: {
    code: 407,
    httpStatus: 410,
    message: 'office already exists'
  },
  OFFICE_NOT_CREATED: {
    code: 408,
    httpStatus: 400,
    message: 'office not created'
  },
  OFFICE_NOT_UPDATED: {
    code: 409,
    httpStatus: 400,
    message: 'office nor updated'
  },
  OFFICE_NOT_REMOVED: {
    code: 404,
    httpStatus: 400,
    message: 'office not removed'
  },
  OFFICES_NOT_FOUND: {
    code: 406,
    httpStatus: 404,
    message: 'office not found'
  }
};

export interface ErrorWithCode {
  code: number;
  message: string;
  httpStatus: number;
  error?: any;
};

function evaluateErrorToStack(error) {
  if (error) {
    if (error.stack) {
      L.error(error.stack);
    } else {
      L.error(error)
    }
  }
}

export function getCodeError(errorId: string, error?: any): ErrorWithCode {
  if (errorId) {
    let errorObject: ErrorWithCode = ERROR_CODES[errorId];
    if (!errorObject) {
      errorObject = ERROR_CODES.UNKNOWN_ERROR;
    }
    if (error) {
      errorObject.error = error;
      evaluateErrorToStack(error);
    }
    return errorObject;
  }
}

export function catchError(res: Response): (any: any) => any {
  return (err: ErrorWithCode) => {
    let headerCode: number = 500;
    L.debug(err)
    if (err && err.httpStatus) {
      headerCode = err.httpStatus;
    }
    L.debug('' + headerCode);
    res
      .status(headerCode)
      .json(err);
  }
}