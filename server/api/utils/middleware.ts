import l from "../../common/logger";
import * as jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from "express";

export class Middleware {
    
    public static decodeToken(req: Request, res: Response, next: NextFunction) {
        const token = req.headers['token'];
        l.debug('HELOOOOOOOOOOOOOO');
        if (!token) {
            res.status(400).send({ auth: false, message: 'No token provided.' });
        }
        else {
            jwt.verify(<string>token, process.env.SESSION_SECRET, function(err, decoded) {
                if (err) {
                    res.status(400).send({err});
                } else {
                    next();
                }
            });
        }
    }
    
}