import { Middleware } from './../../utils/middleware';
import * as express from 'express';
import { UserController } from './users.controller'

const userController = new UserController();

export default express.Router()
    .get('/types', (req, res) => userController.getTypes(req, res))
    .get('/', (req, res) => userController.getAll(req, res))
    .post('/', (req, res) => userController.create(req, res))
    .get('/:id', (req, res) => userController.getById(req, res))
    .put('/:id', (req, res) => userController.update(req, res))
    .delete('/:id', (req, res) => userController.remove(req, res));
