import { User } from './../../models/user';
import { UsersService } from './../../services/users.service';
import { Request, Response, NextFunction } from 'express';
import { catchError } from '../../utils/errorCodes';
import { Inject, Container } from 'typedi';

export class UserController {

  private usersService: UsersService;

  constructor(
  ) {
  }

  public getAll(req: Request, res: Response): void {
    this.initServices();
    this.usersService.getAll()
      .then(users => res.json(users))
      .catch(catchError(res));
  }

  public getById(req: Request, res: Response): void {
    this.initServices();
    this.usersService.getById(req.params.id)
      .then(user => {
        res.json(user);
      })
      .catch(catchError(res));

  }

  public create(req: Request, res: Response): void {
    this.initServices();
    const user = {
      id: req.body.id,
      profile: {
        id: req.body.id,
        firstName: req.body.profile.firstName,
        lastNameOne: req.body.profile.lastNameOne,
        lastNameTwo: req.body.profile.lastNameTwo,
        phone: req.body.profile.phone,
        officeId: req.body.profile.officeId,
        professionalRegistration: req.body.profile.professionalRegistration
      },
      email: req.body.email,
      password: req.body.password,
      roleId: req.body.roleId
    };
    this.usersService.create(user)
      .then(user => res.json(user))
      .catch(catchError(res));
  }

  public update(req: Request, res: Response): void {
    this.initServices();
    const user = {
      id: req.body.id,
      profile: {
        id: req.body.id,
        firstName: req.body.profile.firstName,
        lastNameOne: req.body.profile.lastNameOne,
        lastNameTwo: req.body.profile.lastNameTwo,
        phone: req.body.profile.phone,
        officeId: req.body.profile.officeId,
        professionalRegistration: req.body.profile.professionalRegistration
      },
      email: req.body.email,
      password: req.body.password,
      roleId: req.body.roleId
    };
    this.usersService.update(req.params.id, user)
      .then(user => res.json(user))
      .catch(catchError(res));
  }

  public remove(req: Request, res: Response): void {
    this.initServices();
    this.usersService.remove(req.params.id)
      .then(user => res.json(user))
      .catch(catchError(res));
  }

  public getTypes(req: Request, res: Response): void {
    this.initServices();
    res.json(this.usersService.getTypes());
  }

  private initServices() {
    if (!this.usersService) {
      this.usersService = Container.get(UsersService);
    }
  }

}