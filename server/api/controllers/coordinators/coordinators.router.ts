import { CoordinatorsController } from './coordinators.controller';
import * as express from 'express';

const coordinatorsController = new CoordinatorsController();

export default express.Router()
    .get('/:coordinatorId/technicals', (req, res) => {coordinatorsController.getTechnicalsByCoordinator(req, res)})
    .post('/:coordinatorId/technicals', (req, res) => {coordinatorsController.assignTechnicals(req, res)});