import { Request, Response } from 'express';
import { Container } from 'typedi';
import { CoordinatorsService } from "./../../services/coordinator.service";
import { catchError } from '../../utils/errorCodes';
import L from '../../../common/logger';

export class CoordinatorsController {
    private coordinatorsService: CoordinatorsService;
    
    constructor() {}
    
    public assignTechnicals(request: Request, response: Response) {
        this.initServices();
        this.coordinatorsService.assignTechnicals(request.params.coordinatorId, request.body.techniciansIds)
            .then(result => {
                console.log(result);
                response.json(result);
            })
            .catch(catchError(response));
    }

    public getTechnicalsByCoordinator(request: Request, response: Response) {
        this.initServices();
        this.coordinatorsService.getTechnicalsByCoordinator(request.params.coordinatorId)
            .then(result => response.json(result))
            .catch(catchError(response));
    }
    
    private initServices() {
        if (!this.coordinatorsService) {
            this.coordinatorsService = Container.get(CoordinatorsService);
        }
    }
}
