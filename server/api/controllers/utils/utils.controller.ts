import { Utils } from './../../utils/utils';
import { Request, Response, NextFunction } from 'express';
import { catchError } from '../../utils/errorCodes';

export class UtilsController {
  
  public static ping(req: Request, res: Response): void {
    res.status(200).send({ pong: true });
  }

  public static saveFile(req: Request, res: Response): void {
      Utils.saveFile(req.body.image, 'a', 'test')
        .then(filename => res.json({filename}))
        .catch(catchError(res));
  } 
    
}