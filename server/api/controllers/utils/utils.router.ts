import * as express from 'express';
import { UtilsController } from './utils.controller'

export default express.Router()
  .get('/ping', UtilsController.ping);
  //.post('/savefile', UtilsController.saveFile);