import { Technical } from './../../models/technical';
import { Container } from 'typedi';
import { VisitsService } from './../../services/visits.service';
import { Request, Response, NextFunction } from 'express';
import { catchError } from '../../utils/errorCodes';
import { ChartService } from '../../services/charts.service';
import { VisitStatusConstants } from '../../models/visitStatus';

export class VisitController {

  private visitsService: VisitsService;
  private chartsService: ChartService;


  constructor() {

  }

  public getAll(req: Request, res: Response): void {
    this.initServices();
    this.visitsService.getAll()
      .then(visits => res.json(visits))
      .catch(catchError(res));
  }

  public getById(req: Request, res: Response): void {
    this.initServices();
    this.visitsService.getById(req.params.id)
      .then(visit => {
        res.json(visit);
      })
      .catch(catchError(res));
  }

  public create(req: Request, res: Response): void {
    this.initServices();
    const visit = {
      id: req.body.id,
      date: new Date(req.body.date).toISOString(),
      farmRegisterNumber: req.body.farmRegisterNumber,
      technicalId: req.body.technicalId
    };
    this.visitsService.create(visit)
      .then(visit => res.json(visit))
      .catch(catchError(res));
  }

  public update(req: Request, res: Response): void {
    this.initServices();
    const visit: any = {
      id: req.body.id,
      date: req.body.date,
      nextVisitDate: req.body.nextVisitDate,
      observations: req.body.observations,
      gradualCompliancePlan: req.body.gradualCompliancePlan,
      ownerFarmSign: req.body.ownerFarmSign,
      inspectorSign: req.body.inspectorSign,
      farmRegisterNumber: req.body.farmRegisterNumber,
      statusId: req.body.statusId,
      technicalId: req.body.technicalId,
      personWhoAttendsId: req.body.personWhoAttendsId,
      personWhoAttendsName: req.body.personWhoAttendsName,
      personWhoAttendsPhone: req.body.personWhoAttendsPhone,
      productiveActivities: req.body.productiveActivities,
      qualifications: req.body.qualifications
    };
    this.visitsService.update(req.params.id, visit)
      .then(visit => res.json(visit))
      .catch(catchError(res));
  }

  public remove(req: Request, res: Response): void {
    this.initServices();
    this.visitsService.remove(req.params.id)
      .then(visit => res.json(visit))
      .catch(catchError(res));
  }

  public getTopTenChart(req: Request, res: Response) {
    this.initServices();
    this.chartsService.getTopTenChart(VisitStatusConstants.STATUS_APROVED)
      .then(data => res.json(data))
      .catch(catchError(res));
  }

  public getTotalVisitsByStateChart(req: Request, res: Response) {
    this.initServices();
    this.chartsService.getTotalVisitsByStateChart()
      .then(data => res.json(data))
      .catch(catchError(res));
  }

  public finishVisit(req: Request, res: Response): void {
    this.initServices();
    const visit: any = {
      id: req.body.id,
      date: new Date(req.body.date),
      nextVisitDate: new Date(req.body.nextVisitDate),
      observations: req.body.observations,
      gradualCompliancePlan: req.body.gradualCompliancePlan,
      ownerFarmSign: req.body.ownerFarmSign,
      inspectorSign: req.body.inspectorSign,
      farmRegisterNumber: req.body.farmRegisterNumber,
      statusId: req.body.statusId,
      technicalId: req.body.technicalId,
      personWhoAttendsId: req.body.personWhoAttendsId,
      personWhoAttendsName: req.body.personWhoAttendsName,
      personWhoAttendsPhone: req.body.personWhoAttendsPhone,
      productiveActivities: req.body.productiveActivities,
      qualifications: req.body.qualifications
    };
    this.visitsService.finishVisit(req.params.id, visit)
      .then(visit => res.json(visit))
      .catch(catchError(res));
  }

  private initServices() {
    if (!this.visitsService) {
      this.visitsService = Container.get(VisitsService);
    }
    if (!this.chartsService) {
      this.chartsService = Container.get(ChartService);
    }
  }

}