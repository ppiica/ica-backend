import * as express from 'express';
import { VisitController } from './visits.controller'
import Container from 'typedi';

const visitController = new VisitController();

export default express.Router()
    .get('/status-chart', (req, res) => visitController.getTotalVisitsByStateChart(req, res))
    .get('/topten-chart', (req, res) => visitController.getTopTenChart(req, res))
    .get('/', (req, res) => visitController.getAll(req, res))
    .post('/', (req, res) => visitController.create(req, res))
    .get('/:id', (req, res) => visitController.getById(req, res))
    .put('/:id', (req, res) => visitController.update(req, res))
    .delete('/:id', (req, res) => visitController.remove(req, res))
    .post('/:id/finish', (req, res) => visitController.finishVisit(req, res));
