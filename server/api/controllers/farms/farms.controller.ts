import { Farm } from './../../models/farm';
import { FarmsService } from './../../services/farms.service';
import { Request, Response, NextFunction } from 'express';
import { catchError } from '../../utils/errorCodes';
import { Inject, Container } from 'typedi';

export class FarmsController {

  private farmsService: FarmsService

  constructor(
  ) {}

  public getAll(req: Request, res: Response): void {
    this.initServices();
    this.farmsService.getAll()
      .then(farms => res.json(farms))
      .catch(catchError(res));
  }

  public getByRegisterNumber(req: Request, res: Response): void {
    this.initServices();
    this.farmsService.getByRegisterNumber(req.params.registerNumber)
      .then(farms => res.json(farms))
      .catch(catchError(res));
  }

  public create(req: Request, res: Response): void {
    this.initServices();
    const farm = {
      name: req.body.name,      
      registerNumber: req.body.registerNumber,
      sidewalkId: req.body.sidewalkId,
      ownerId: req.body.ownerId,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      area: req.body.area,
      officeId: req.body.officeId,
    };
    this.farmsService.create(farm)
      .then(farm => res.json(farm))
      .catch(catchError(res));
  }

  public update(req: Request, res: Response): void {
    this.initServices();
    const farm: any = {
      name: req.body.name,
      sidewalkId: req.body.sidewalkId,
      ownerId: req.body.ownerId,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      area: req.body.area,
      officeId: req.body.officeId,      
    };
    this.farmsService.update(req.params.registerNumber, farm)
      .then(farm => res.json(farm))
      .catch(catchError(res));
  }

  public remove(req: Request, res: Response): void {
    this.initServices();
    this.farmsService.remove(req.params.registerNumber)
      .then(farm => res.json(farm))
      .catch(catchError(res));
  }

  private initServices() {
    if (!this.farmsService) {
      this.farmsService = Container.get(FarmsService);
    }
  }

}