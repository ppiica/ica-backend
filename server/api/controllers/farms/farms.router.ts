import { Container } from 'typedi';
import { Middleware } from './../../utils/middleware';
import * as express from 'express';
import { FarmsController } from './farms.controller';

const farmsController = new FarmsController();;

export default express.Router()
.get('/',  (req, res) => farmsController.getAll(req, res))
.post('/',  (req, res) => farmsController.create(req, res))
.get('/:registerNumber', (req, res) => farmsController.getByRegisterNumber(req, res))
.put('/:registerNumber', (req, res) => farmsController.update(req, res))
.delete('/:registerNumber', (req, res) => farmsController.remove(req, res));
