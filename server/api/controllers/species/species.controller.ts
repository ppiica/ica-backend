import { Response } from 'express';
import { Request } from 'express';
import { Container } from 'typedi';
import { SpeciesService } from "../../services/species.service";
import { catchError } from '../../utils/errorCodes';

export class SpeciesController {
  private speciesService: SpeciesService;

  constructor() {

  }

  public getAll(req: Request, res: Response) {
    this.initServices();
    this.speciesService.getAll()
        .then(result => res.json(result))
        .catch(catchError(res));
  }

  private initServices() {
      if (!this.speciesService) {
          this.speciesService = Container.get(SpeciesService);
      }
  }

}
