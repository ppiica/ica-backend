import * as express from 'express';
import { SpeciesController } from './species.controller';

const speciesController = new SpeciesController();

export default express.Router()
    .get('/', (req, res) => speciesController.getAll(req, res));
