import { Owner } from './../../models/owner';
import { OwnersService } from './../../services/owners.service';
import { Request, Response, NextFunction } from 'express';
import { catchError } from '../../utils/errorCodes';
import { Inject, Container } from 'typedi';

export class OwnerController {

  private ownersService: OwnersService;

  constructor(        
  ) {
  }

  public getAll(req: Request, res: Response): void {
    this.initServices();
    this.ownersService.getAll()
      .then(owners => res.json(owners))
      .catch(catchError(res));
  }

  public getById(req: Request, res: Response): void {
    this.initServices();
    this.ownersService.getById(req.params.id)
      .then(owner => {
        res.json(owner);
      })
      .catch(catchError(res));

  }

  public create(req: Request, res: Response): void {
    this.initServices();
    const owner = {
      id: req.body.id,
      firstName: req.body.firstName,
      lastNameOne: req.body.lastNameOne,
      lastNameTwo: req.body.lastNameTwo,
      email: req.body.email,
      phone: req.body.phone,
      address: req.body.address
    };
    this.ownersService.create(owner)
      .then(owner => res.json(owner))
      .catch(catchError(res));
  }

  public update(req: Request, res: Response): void {
    this.initServices();
    const owner = {
      id: req.body.id,
      firstName: req.body.firstName,
      lastNameOne: req.body.lastNameOne,
      lastNameTwo: req.body.lastNameTwo,
      email: req.body.email,
      phone: req.body.phone,
      address: req.body.address
    };
    this.ownersService.update(req.params.id, owner)
      .then(owner => res.json(owner))
      .catch(catchError(res));
  }

  public remove(req: Request, res: Response): void {
    this.initServices();
    this.ownersService.remove(req.params.id)
      .then(owner => res.json(owner))
      .catch(catchError(res));
  }

  private initServices() {
    if (!this.ownersService) {
      this.ownersService = Container.get(OwnersService);
    }
  }

}