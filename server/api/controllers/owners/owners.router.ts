import { Middleware } from './../../utils/middleware';
import * as express from 'express';
import { OwnerController } from './owners.controller'

const ownerController = new OwnerController();

export default express.Router()
    .get('/', (req, res) => ownerController.getAll(req, res))
    .post('/', (req, res) => ownerController.create(req, res))
    .get('/:id', (req, res) => ownerController.getById(req, res))
    .put('/:id', (req, res) => ownerController.update(req, res))
    .delete('/:id', (req, res) => ownerController.remove(req, res));
