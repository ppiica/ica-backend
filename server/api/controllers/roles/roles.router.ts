import { Middleware } from './../../utils/middleware';
import { RolesController } from './roles.controller';
import * as express from 'express';
import { Container } from 'typedi';
import { Role } from '../../models/role';

const rolesController: RolesController = new RolesController();

export default express.Router()
    .get('/', Middleware.decodeToken, (req, res) => rolesController.getAll(req, res));