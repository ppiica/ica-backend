import { RolesService } from './../../services/roles.service';
import { Request, Response } from 'express';
import { catchError } from '../../utils/errorCodes';
import { Container } from 'typedi';
export class RolesController {
    
    private rolesService: RolesService;
    
    constructor() {
    }
    
    public getAll(request: Request, response: Response) {
        this.initServices();
        this.rolesService.getAll()
        .then(roles => response.json(roles))
        .catch(catchError);
    }
    
    private initServices() {
        if (!this.rolesService) {
            this.rolesService = Container.get(RolesService);
        }
    }
}