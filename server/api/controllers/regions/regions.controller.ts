import { Container } from 'typedi';
import { RegionsService } from './../../services/regions.service';
import { Request, Response, NextFunction } from 'express';

export class RegionsController {

  private regionsService: RegionsService;

  constructor(
  ) { }

  public getDepartments(req: Request, res: Response): void {
    this.initServices();
    this.regionsService.getDepartments()
      .then(departments => res.json(departments))
      .catch(err => res.json(err));
  }

  public getMunicipalitiesByDepartmentId(req: Request, res: Response): void {
    this.initServices();
    this.regionsService.getMunicipalitiesByDepartmentId(req.params.departmentId)
      .then(municipalities => res.json(municipalities))
      .catch(err => res.json(err));
  }

  public getSidewalksByMunicipalityId(req: Request, res: Response): void {
    this.initServices();
    this.regionsService.getSideWalksByMunicipalityId(req.params.municipalityId)
      .then(sidewalks => res.json(sidewalks))
      .catch(err => res.json(err));
  }

  private initServices() {
    if (!this.regionsService) {
      this.regionsService = Container.get(RegionsService);
    }
  }

}