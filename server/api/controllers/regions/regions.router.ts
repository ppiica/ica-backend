import { Container } from 'typedi';
import { Middleware } from './../../utils/middleware';
import * as express from 'express';
import { RegionsController } from './regions.controller';

const regionsController = new RegionsController();

export default express.Router()
    .get('/departments', (res, req) => regionsController.getDepartments(res, req))
    .get('/municipalities/:departmentId', (res, req) => regionsController.getMunicipalitiesByDepartmentId(res, req))
    .get('/sidewalks/:municipalityId', (res, req) => regionsController.getSidewalksByMunicipalityId(res, req));