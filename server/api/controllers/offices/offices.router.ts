import { Container } from 'typedi';
import { Middleware } from './../../utils/middleware';
import * as express from 'express';
import { OfficesController } from './offices.controller';

const officesController = new OfficesController();

export default express.Router()
    .get('/', (res, req) => officesController.getOffices(res, req))
    .post('/', (req, res) => officesController.create(req, res))
    .get('/:id', (req, res) => officesController.getById(req, res))
    .put('/:id', (req, res) => officesController.update(req, res))
    .delete('/:id', (req, res) => officesController.remove(req, res));