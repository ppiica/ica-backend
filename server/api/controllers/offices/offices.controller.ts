import { Container } from 'typedi';
import { OfficesService } from './../../services/offices.service';
import { Request, Response, NextFunction } from 'express';
import { catchError } from '../../utils/errorCodes';

export class OfficesController {

  private officesService: OfficesService;

  constructor(
  ) { }

  public getOffices(req: Request, res: Response): void {
    this.initServices();
    this.officesService.getOffices()
      .then(offices => res.json(offices))
      .catch(catchError(res));
  }

  public getById(req: Request, res: Response): void {
    this.initServices();
    this.officesService.getById(req.params.id)
      .then(offices => {
        res.json(offices);
      })
      .catch(catchError(res));

  }

  public create(req: Request, res: Response): void {
    this.initServices();
    const office = {
      id: req.body.id,
      name: req.body.name
    };
    this.officesService.create(office)
      .then(office => res.json(office))
      .catch(catchError(res));
  }

  public update(req: Request, res: Response): void {
    this.initServices();
    const office = {
      id: req.body.id,
      name: req.body.name
    };
    this.officesService.update(req.params.id, office)
      .then(office => res.json(office))
      .catch(catchError(res));
  }

  public remove(req: Request, res: Response): void {
    this.initServices();
    this.officesService.remove(req.params.id)
      .then(office => res.json(office))
      .catch(catchError(res));
  }

  private initServices() {
    if (!this.officesService) {
      this.officesService = Container.get(OfficesService);
    }
  }

}