import * as express from 'express';
import { CategoriesController } from './categories.controller';

const categoriesController = new CategoriesController();

export default express.Router()
    .get('/', (req, res) => categoriesController.getCategories(req, res));