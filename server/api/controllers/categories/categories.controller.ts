import { Container } from 'typedi';
import { CategoriesService } from './../../services/categories.service';
import { Request, Response } from 'express';

export class CategoriesController {
    
    private categoriesService: CategoriesService;

    constructor(
    ) {

    }

    public getCategories(req: Request, res: Response): void {
        this.initServices();
        this.categoriesService.getCategories()
        .then(categories => res.json(categories))
        .catch();
    }

    private initServices() {
        if (!this.categoriesService) {
            this.categoriesService = Container.get(CategoriesService);
        }
    }

}