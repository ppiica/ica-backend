import { Request, Response, NextFunction } from 'express';
import { AuthService } from '../../services/auth.service';
import { catchError, getCodeError } from '../../utils/errorCodes';
import l from '../../../common/logger';
import Container from 'typedi';

export class AuthController {

    private authService: AuthService;

    constructor(
    ) {

    }

    public login(req: Request, res: Response): void {
        this.initServices();
        this.authService.login(req.body.email, req.body.password)
            .then((result) => {
                res.json(result);
            })
            .catch(catchError(res));
    }

    public getLoggedUser(req: Request, res: Response) {
        const token: any = req.headers['token'];
        console.log(token);
        
        if (token) {
            this.initServices();
            this.authService.getUserByToken(token)
                .then(result => {
                    res.json(result);
                })
                .catch(catchError(res));
        } else {
            const err = getCodeError('USER_NOT_FOUND');
            catchError(res)(err);
        }
    }

    public recoverPassword(req: Request, res: Response) {
        this.initServices();
        this.authService.recoverPassword(req.body.email)
            .then(s => res.send(true))
            .catch(catchError(res));
    }

    public checkTokenToResetPassword(req: Request, res: Response) {
        this.initServices();
        this.authService.checkTokenToResetPassword(req.body.token)
            .then(user => res.send(user))
            .catch(catchError(res));
    }

    public resetPassword(req: Request, res: Response) {
        this.initServices();
        this.authService.resetPassword(req.body.token, req.body.password)
            .then(s => res.send(true))
            .catch(catchError(res));
    }

    private initServices() {
        if (!this.authService) {
            this.authService = Container.get(AuthService);
        }
    }

}