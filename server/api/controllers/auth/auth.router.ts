import { Middleware } from './../../utils/middleware';
import { AuthController } from './auth.controller';
import * as express from 'express';
import { Container } from 'typedi';

const authController = new AuthController();

export default express.Router()
  .post('/login', (req, res) => authController.login(req, res))
  .get('/loggedUser', (req, res) => authController.getLoggedUser(req, res))
  .post('/recoverPassword', (req, res) => authController.recoverPassword(req, res))
  .post('/checkTokenToResetPassword', (req, res) => authController.checkTokenToResetPassword(req, res))
  .put('/resetPassword', (req, res) => authController.resetPassword(req, res));