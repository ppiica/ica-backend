import { Middleware } from './../../utils/middleware';
import { TechnicalsController } from './technicals.controller';
import * as express from 'express';

const technicalsController = new TechnicalsController();

export default express.Router()
    .get('/', (req, res) => {technicalsController.getAllTechnicals(req, res)})
    .get('/:id/visits', (req, res) => {technicalsController.getVistisByTechnicalId(req, res)});