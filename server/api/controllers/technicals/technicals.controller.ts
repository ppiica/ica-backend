import { VisitsService } from './../../services/visits.service';
import { Technical } from './../../models/technical';
import { Container } from 'typedi';
import { TechnicalsService } from './../../services/technicals.service';
import { Request, Response } from 'express';
import { catchError } from '../../utils/errorCodes';


export class TechnicalsController {

    private technicalsService: TechnicalsService; 
    private visitsService: VisitsService;

    constructor() {

    }

    public getAllTechnicals(request: Request, response: Response) {
        this.initServices();
        this.technicalsService.getAllTechnicals()
            .then(technicals => response.json(technicals))
            .catch(catchError(response));
    }

    public getVistisByTechnicalId(request: Request, response: Response) {
        this.initServices();
        this.visitsService.getVisitsByTechnical(request.params.id)
            .then(visits => response.json(visits))
            .catch(catchError(response));
    }

    private initServices() {
        if (!this.technicalsService) {
            this.technicalsService = Container.get(TechnicalsService);
        }
        if (!this.visitsService) {
            this.visitsService = Container.get(VisitsService);
        }
    }

}