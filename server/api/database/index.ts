import { RegionsService } from './../services/regions.service';
import { UserRoleForm } from './../models/user-role-form';
import { Form } from './../models/form';
import { RoleForm } from './../models/role-form';
import { Technical } from './../models/technical';
import { Item } from './../models/item';
import { Department } from './../models/department';
import "reflect-metadata";

import { createConnection, ConnectionOptions, useContainer } from "typeorm";
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { Visit } from './../models/visit';
import { Specie } from './../models/specie';
import { Qualification } from './../models/qualification';
import { ProductiveActivity } from './../models/productive-activity';
import { Office } from './../models/office';
import { Farm } from './../models/farm';
import { Owner } from './../models/owner';
import { User } from './../models/user';
import Container from "typedi";
import { Municipality } from '../models/municipality';
import { Sidewalk } from '../models/sidewalk';
import { Category } from '../models/category';
import { VisitStatus } from '../models/visitStatus';
import { Coordinator } from '../models/coordinator';
import { Manager } from '../models/manager';
import { Role } from '../models/role';


export async function initDB() {

  const connectionOptions: MysqlConnectionOptions = {
    type: 'mysql',
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: [
      Farm,
      Office,
      Owner,
      Qualification,
      Specie,
      User,
      Visit,
      ProductiveActivity,
      Department,
      Municipality,
      Sidewalk,
      Item,
      Category,
      VisitStatus,
      Manager,
      Coordinator,
      Technical,
      RoleForm,
      UserRoleForm,
      Role,
      Form,
    ],
    synchronize: (process.env.DB_SYNCHRONIZE === 'true'),
    logging: (process.env.DB_LOGGING === 'true'),
    connectTimeout: 99999
  };
  try {
    useContainer(Container);
    const connection = await createConnection(connectionOptions);

    const regionsService: RegionsService = Container.get(RegionsService);

    regionsService.insertInitData()
      .catch(console.error);

    return connection;
  } catch (error) {
    console.error(error);
  }
}
