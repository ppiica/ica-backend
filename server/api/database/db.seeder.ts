import { SpeciesService } from './../services/species.service';
import { CategoriesService } from './../services/categories.service';
import { requirementList } from './../models/data/evaluations-array';
import { Qualification } from './../models/qualification';
import { Category } from './../models/category';
import { VisitStatusConstants, VisitStatus } from './../models/visitStatus';
import { Technical } from './../models/technical';
import { Owner } from './../models/owner';
import { Office } from './../models/office';
import { Manager } from './../models/manager';
import { UserRoleForm } from "./../models/user-role-form";
import { Form, FormsConstants } from "./../models/form";
import { User, UserConstants } from "./../models/user";
import { RoleForm } from "./../models/role-form";
import { Connection, Repository } from "typeorm";
import { Role } from "../models/role";
import { throws } from "assert";
import { Utils } from "../utils/utils";
import { Coordinator } from '../models/coordinator';
import { Item } from '../models/item';
import { Container } from 'typedi';

let usersId = 0;

export async function seedDB(connection: Connection) {
    const categoriesService = Container.get(CategoriesService);
    const speciesService = Container.get(SpeciesService);
    const usersRepository = connection.getRepository<User>(User);
    const rolesRepository = connection.getRepository<Role>(Role);
    const roleFormsRepository = connection.getRepository<RoleForm>(RoleForm);
    const userFormsRepository = connection.getRepository<UserRoleForm>(UserRoleForm);
    let forms: Form[] = await generateForms(connection);
    await generateMaster(connection, rolesRepository, roleFormsRepository, userFormsRepository, usersRepository, forms);
    const offices = await generateTestOffices(connection);
    await generateTestOwners(connection);
    try {
        const manager = await generateDefaultManager(connection, rolesRepository, roleFormsRepository, userFormsRepository, usersRepository, forms);
        const coordinator = await generateDefaultCoordinator(connection, rolesRepository, roleFormsRepository, userFormsRepository, usersRepository, forms, manager.id, getRandomElement(offices).id);
        await generateDefaultTechnical(connection, rolesRepository, roleFormsRepository, userFormsRepository, usersRepository, forms, coordinator.id);
    } catch(exception) {
        console.error(exception);
    }
    await generateVisitsStatus(connection);
    await categoriesService.generateCategories();
    await speciesService.insertInitData();
}

async function addUserForms(roleId: number, email: string, roleFormsRepository: Repository<RoleForm>, userFormsRepository: Repository<UserRoleForm>) {
    const roleForms = await roleFormsRepository.find({roleId: roleId});
    for (let i = 0; i < roleForms.length; i++) {
        const form = roleForms[i];
        const userForm = new UserRoleForm();
        userForm.create = form.create;
        userForm.update = form.update;
        userForm.delete = form.delete;
        userForm.formId = form.formId;
        userForm.userEmail = email;
        await userFormsRepository.save(userForm);
    }
}

async function generateDefaultManager(connection: Connection, rolesRepository: Repository<Role>, roleFormsRepository: Repository<RoleForm>, userFormsRepository: Repository<UserRoleForm>, usersRepository: Repository<User>, forms: Form[]): Promise<Manager> {
    const managerRole = await generateMaganerRole(connection, forms, rolesRepository, roleFormsRepository);
    const defaultManager = await generateManager(connection, (++usersId) + '', 'Jane', 'Doe');
    const defaultManagerUser = await generateUser(connection, defaultManager.id, managerRole.id, 'Manager@yopmail.com', 'manager123', usersRepository);
    const formsCount: number = await userFormsRepository.count({userEmail: defaultManagerUser.email});
    if (!formsCount) {
        await addUserForms(managerRole.id, defaultManagerUser.email, roleFormsRepository, userFormsRepository);
    }
    return defaultManager;
}

async function generateDefaultCoordinator(connection: Connection, rolesRepository: Repository<Role>, roleFormsRepository: Repository<RoleForm>, userFormsRepository: Repository<UserRoleForm>, usersRepository: Repository<User>, forms: Form[], managerId: string, officeId: number): Promise<Coordinator> {
    const coordinatorRole = await generateCoordinatorRole(connection, forms, rolesRepository, roleFormsRepository);
    const defaultCoordinator = await generateCoordinator(connection, (++usersId) + '', 'John', 'Doe', managerId, officeId);
    const defaultCoordinatorUser = await generateUser(connection, defaultCoordinator.id, coordinatorRole.id, 'coordinator@yopmail.com', 'coordinator123', usersRepository);
    const formsCount: number = await userFormsRepository.count({userEmail: defaultCoordinatorUser.email});
    if (!formsCount) {
        await addUserForms(coordinatorRole.id, defaultCoordinatorUser.email, roleFormsRepository, userFormsRepository);
    }
    return defaultCoordinator;
}

async function generateDefaultTechnical(connection: Connection, rolesRepository: Repository<Role>, roleFormsRepository: Repository<RoleForm>, userFormsRepository: Repository<UserRoleForm>, usersRepository: Repository<User>, forms: Form[], coordinatorId): Promise<Technical> {    
    const technicalRole = await generateTechnicalRole(connection, forms, rolesRepository, roleFormsRepository);
    const defaultTechnical = await generateTechnical(connection, (++usersId) + '', 'Jane', 'Roe', coordinatorId, 'AGJ17022');
    const defaultTechnicalUser = await generateUser(connection, defaultTechnical.id, technicalRole.id, 'technical@yopmail.com', 'technical123', usersRepository);
    const formsCount: number = await userFormsRepository.count({userEmail: defaultTechnicalUser.email});
    if (!formsCount) {
        await addUserForms(technicalRole.id, defaultTechnicalUser.email, roleFormsRepository, userFormsRepository);
    }
    return defaultTechnical;
}

async function generateMaster(connection: Connection, rolesRepository: Repository<Role>, roleFormsRepository: Repository<RoleForm>, userFormsRepository: Repository<UserRoleForm>, usersRepository: Repository<User>, forms: Form[]) {
    let masterRole = await generateRole(UserConstants.ROLE_MASTER, connection, rolesRepository);
    let manager: Manager = await generateManager(connection, (++usersId) + '', 'Richard', 'Roe');
    let masterUser: User = await generateUser(connection, manager.id, masterRole.id, UserConstants.MASTER_EMAIL, 'MASTER123', usersRepository);
    await addForms(forms, masterUser, masterRole, roleFormsRepository, userFormsRepository);
}

async function generateRole(roleDescription: string, connection: Connection, rolesRepository: Repository<Role>): Promise<Role> {
    let masterRole = await rolesRepository.findOne({
        description: roleDescription.toLowerCase()
    });
    if (!masterRole) {
        const role: Role = new Role();
        role.description = roleDescription.toLowerCase();
        role.createdAt = new Date();
        masterRole = await rolesRepository.save(role);
    }
    return masterRole;
}

async function generateForms(connection: Connection): Promise<Form[]> {
    const formsRepository = connection.getRepository<Form>(Form);
    const forms: Form[] = [];
    for (let i: number = 0; i < FormsConstants.FORMS.length; i++) {
        const form = FormsConstants.FORMS[i];
        let formData: Form = await formsRepository.findOne({
            description: form.description
        });
        if (!formData) {
            formData = new Form();
            formData.description = form.description;
            formData.createdAt = new Date();
            formData = await formsRepository.save(formData);
        }
        forms.push(formData);
    }
    return forms;
}

async function generateUser(connection: Connection, profileId: string, roleId: number, email: string, password: string, usersRepository: Repository<User>): Promise<User> {
    let masterUser = await usersRepository.findOne({email: email.toLowerCase()});
    if (await !masterUser) {
        const user = new User();
        user.roleId = roleId;
        user.email = email.toLowerCase();
        user.password = await Utils.hashText(password.toLowerCase());
        user.id = profileId;
        user.createdAt = new Date();
        masterUser = await usersRepository.save(user);
    }
    return masterUser;
}


async function addForm(formId: number, roleId: number, repository: Repository<RoleForm>, canCreate: boolean, canUpdate: boolean, canDelete: boolean) {
    let roleForm: RoleForm = await repository.findOne({
        where: {
            formId: formId,
            roleId: roleId
        }
    });
    if (!roleForm) {
        roleForm = new RoleForm();
        roleForm.create = canCreate;
        roleForm.delete = canUpdate;
        roleForm.update = canCreate;
        roleForm.formId = formId;
        roleForm.roleId = roleId;
        roleForm.createdAt = new Date();    
        await repository.save(roleForm);  
    }   
}

async function addUserForm(formId: number, userEmail: string, repository: Repository<UserRoleForm>){
    let userForm: UserRoleForm = await repository.findOne({
        where: {
            formId: formId,
            userEmail: userEmail
        }
    });
    if (!userForm) {
        userForm = new UserRoleForm();
        userForm.create = true;
        userForm.delete = true;
        userForm.update = true;
        userForm.formId = formId;
        userForm.userEmail = userEmail;
        userForm.createdAt = new Date();
        await repository.save(userForm);
    }
}

async function addForms(forms: Form[], user: User, role: Role, roleFormsRepository: Repository<RoleForm>, userFormRepository: Repository<UserRoleForm>) {
    for(let i: number = 0; i < forms.length; i++) {
        const form = forms[i];
        await addForm(form.id, role.id, roleFormsRepository, true, true, true);
        await addUserForm(form.id, user.email, userFormRepository)
    }
}

async function generateManager(connection: Connection, id: string, name: string, lastName: string): Promise<Manager> {
    const managersRepository = connection.getRepository<Manager>(Manager);
    let manager: Manager = await managersRepository.findOne({id: UserConstants.DEFAULT_MANAGER_ID + ''});
    if (!manager) {
        manager = new Manager();
        manager.id = UserConstants.DEFAULT_MANAGER_ID + '';
        manager.firstName = 'Palito';
        manager.lastNameOne = 'Ortega';
        manager.createdAt = new Date();
        managersRepository.save(manager);
    }
    return manager;
}

async function generateMaganerRole(connection: Connection, forms: Form[], repository: Repository<Role>, roleFormRepository: Repository<RoleForm>) : Promise<Role>{
    let managerRole = await generateRole(UserConstants.ROLE_MANAGER, connection, repository);
    for(let i: number = 0; i < forms.length; i++) {
        const form = forms[i];
        let roleForm = await roleFormRepository.findOne({roleId: managerRole.id, formId: form.id});
        if (!roleForm) {
            switch(form.description) {   
                case FormsConstants.FORM_NAMES.USERS:
                await addForm(form.id, managerRole.id, roleFormRepository, true, true, true);
                break;
                case FormsConstants.FORM_NAMES.FARMS:
                await addForm(form.id, managerRole.id, roleFormRepository, true, true, true);
                break;
                case FormsConstants.FORM_NAMES.VISITS:
                await addForm(form.id, managerRole.id, roleFormRepository, true, true, true);
                break;
                case FormsConstants.FORM_NAMES.OWNERS:
                await addForm(form.id, managerRole.id, roleFormRepository, true, true, true);
                break;
                case FormsConstants.FORM_NAMES.CRUDS:
                await addForm(form.id, managerRole.id, roleFormRepository, true, true, true);
                break;
                case FormsConstants.FORM_NAMES.USER_ASIGN:
                await addForm(form.id, managerRole.id, roleFormRepository, true, true, true);
                break;
            }
        }
    }
    return managerRole;
}

async function generateCoordinatorRole(connection: Connection, forms: Form[], repository: Repository<Role>, roleFormRepository: Repository<RoleForm>): Promise<Role> {
    let coordinatorRole = await generateRole(UserConstants.ROLE_COORDINATOR, connection, repository);
    for(let i: number = 0; i < forms.length; i++) {
        const form = forms[i];
        let roleForm = await roleFormRepository.findOne({roleId: coordinatorRole.id, formId: form.id});
        if (!roleForm) {
            switch(form.description) {   
                case FormsConstants.FORM_NAMES.VISITS:
                await addForm(form.id, coordinatorRole.id, roleFormRepository, true, true, true);
                break;
                case FormsConstants.FORM_NAMES.OWNERS:
                await addForm(form.id, coordinatorRole.id, roleFormRepository, true, true, true);
                break;
            }
        }
    }
    return coordinatorRole;
}

async function generateTechnicalRole(connection: Connection, forms: Form[], repository: Repository<Role>, roleFormRepository: Repository<RoleForm>) : Promise<Role> {
    let technicalRole = await generateRole(UserConstants.ROLE_TECHNICIAN, connection, repository);
    for(let i: number = 0; i < forms.length; i++) {
        const form = forms[i];
        let roleForm = await roleFormRepository.findOne({roleId: technicalRole.id, formId: form.id});
        if (!roleForm) {
            switch(form.description) {   
                case FormsConstants.FORM_NAMES.VISITS:
                await addForm(form.id, technicalRole.id, roleFormRepository, false, true, true);
                break;
                case FormsConstants.FORM_NAMES.OWNERS:
                await addForm(form.id, technicalRole.id, roleFormRepository, true, true, true);
                break;
            }
        }
    }
    return technicalRole;
}

async function generateTestOffices(connection: Connection): Promise<Office[]> {
    let offices: Office[] = [];
    const officesRepository = connection.getRepository<Office>(Office);
    for (let i = 1; i <= 10; i++) {
        let office: Office = new Office();
        office.id = `${i}`;
        office.name = `officina ${i}`;
        office.createdAt = new Date();
        office = await officesRepository.save(office);
        offices.push(office);
    }
    return offices;
}

async function generateTechnical(connection: Connection, id: string, name: string, lastName: string, coordinatorId: string, profesionalRegistration: string): Promise<Technical> {
    const technicalsRepository = connection.getRepository<Technical>(Technical);
    const count: number = await technicalsRepository.count();
    if (!count) {
        let technical: Technical = new Technical();
        technical.id = id;
        technical.professionalRegistration = profesionalRegistration;
        technical.coordinatorId = coordinatorId;
        technical.firstName = name;
        technical.lastNameOne = lastName;
        technical.createdAt = new Date();
        return technicalsRepository.save(technical);
    }
    return Promise.reject('there are technicals already created');
}

async function generateCoordinator(connection: Connection, id: string, name: string, lastName: string, managerId: string, officeId: number): Promise<Coordinator> {
    const coordinatorsRepository = connection.getRepository<Coordinator>(Coordinator);
    const count: number = await coordinatorsRepository.count();
    if (!count) {
        let coordinator: Coordinator = new Coordinator();
        coordinator.id = id;
        coordinator.officeId = officeId;
        coordinator.managerId = managerId;
        coordinator.firstName = name;
        coordinator.lastNameOne = lastName;
        coordinator.createdAt = new Date();
        return coordinatorsRepository.save(coordinator);
    }
    return Promise.reject('there are coordinators already created');
}

async function generateTestOwners(connection: Connection) {
    const ownersRepository = connection.getRepository<Owner>(Owner);
    const count: number = await ownersRepository.count();
    if (!count) {
        const addressElements = ['cra', 'clle', 'cr', 'cl'];
        const addressDirection = ['norte', 'sur', 's', 'n', '-'];
        const addressNumerals = ['#', 'No', 'nro']
        const names = ['James', 'Tobias', 'Jonathan', 'Johny', 'Heath', 'Taylor', 'Alissa', 'Angela', 'Kirk', 'Johnny'];
        const lastNames = ['Hetfield', 'Forge', 'Davis', 'Cash', 'Ledger', 'Momsem', 'White', 'Goslow', 'Hammet', 'Rotten'];
        for (let i = 0; i < 20; i++) {
            const owner = new Owner();
            owner.firstName = getRandomElement(names);
            owner.lastNameOne = getRandomElement(lastNames);
            owner.lastNameTwo = getRandomElement(lastNames);
            owner.email = `${owner.firstName}_${owner.lastNameOne}@test.com`;
            owner.phone = 2000000 + getRandom(2500000);
            owner.id =  (80000000 + getRandom(1135999999)) + '';
            owner.address = `${getRandomElement(addressElements)} ${5 + getRandom(120)} ${getRandomElement(addressNumerals)} ${5 + getRandom(100)} ${getRandomElement(addressDirection)} ${5 + getRandom(100)}`;
            ownersRepository.save(owner)
        }
    }
}

function getRandom(limit: number): number {
    return Math.floor(Math.random() * limit);
}

function getRandomElement(array: any[]) : any {
    return array[getRandom(array.length)];
}

async function generateVisitsStatus(connection: Connection) {
    const visitStatusRepository = connection.getRepository<VisitStatus>(VisitStatus);
    for (let i = 0; i < VisitStatusConstants.VisitsStatus.length; i++) {
        const visitStatusConstant = VisitStatusConstants.VisitsStatus[i];
        let visitStatus = await visitStatusRepository.findOne({description: visitStatusConstant.description});
        if (!visitStatus) {
            visitStatus = new VisitStatus();
            visitStatus.description = visitStatusConstant.description;
            await visitStatusRepository.save(visitStatus);
        }
    }
}