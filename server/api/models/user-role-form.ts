import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, RelationId, CreateDateColumn, UpdateDateColumn, PrimaryColumn } from 'typeorm';
import { Form } from './form';
import { User } from './user';

@Entity()
export class UserRoleForm {

  @Column({})
  public create: boolean;

  @Column({})
  public update: boolean;

  @Column({})
  public delete: boolean;

  @ManyToOne(type => User, user => user.userRoleForms, { primary: true })
  public user: User;

  @ManyToOne(type => Form, form => form.roleForms, { primary: true })
  public form: Form;

  @RelationId((userRoleForm: UserRoleForm) => userRoleForm.user)
  @PrimaryColumn()
  public userEmail: string

  @RelationId((userRoleForm: UserRoleForm) => userRoleForm.form)
  @PrimaryColumn()
  public formId: number;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

}