import { RoleForm } from "./role-form";
import { OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { UserRoleForm } from "./user-role-form";

@Entity()
export class Form {
  @PrimaryGeneratedColumn({ type: "int" })
  public id: number;

  @Column({ length: 45 })
  public description: string;

  @OneToMany(type => RoleForm, roleForm => roleForm.form)
  public roleForms: RoleForm[];

  @OneToMany(type => UserRoleForm, userRoleForm => userRoleForm.form)
  public userRoleForms: UserRoleForm[];

  @CreateDateColumn() public createdAt?: Date;

  @UpdateDateColumn() public updatedAt?: Date;
}

export class FormsConstants {
  static readonly FORM_NAMES = {
    USERS: "USUARIOS",
    FARMS: "PREDIOS",
    VISITS: "VISITAS",
    OWNERS: "PROPIETARIOS",
    CRUDS: "MAESTROS",
    USER_ASIGN: "ASIGNACION_USUARIOS"
  };
  static readonly FORMS = [
    {
      id: 0,
      description: FormsConstants.FORM_NAMES.USERS
    },
    {
      id: 1,
      description: FormsConstants.FORM_NAMES.FARMS
    },
    {
      id: 2,
      description: FormsConstants.FORM_NAMES.VISITS
    },
    {
      id: 3,
      description: FormsConstants.FORM_NAMES.OWNERS
    },
    {
      id: 4,
      description: FormsConstants.FORM_NAMES.CRUDS
    },
    {
      id: 5,
      description: FormsConstants.FORM_NAMES.USER_ASIGN
    }
  ];
}
