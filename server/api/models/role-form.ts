import { Role } from './role';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, RelationId, PrimaryColumn } from 'typeorm';
import { Form } from './form';

@Entity()
export class RoleForm {

  @Column({})
  public create: boolean;

  @Column({})
  public update: boolean;

  @Column({})
  public delete: boolean;

  @RelationId((roleForm: RoleForm) => roleForm.role)
  @PrimaryColumn()  
  public roleId: number;

  @RelationId((roleForm: RoleForm) => roleForm.form)
  @PrimaryColumn()  
  public formId: number;

  @ManyToOne(type => Role, role => role.roleForms, { primary: true })
  public role: Role;

  @ManyToOne(type => Form, form => form.roleForms, { primary: true })
  public form: Form;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

}