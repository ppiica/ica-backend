import { Visit } from './visit';
import { OneToMany } from 'typeorm';
import { Office } from './office';
import { Owner } from './owner';
import { Sidewalk } from './sidewalk';
import { getRepository, ManyToOne, CreateDateColumn, UpdateDateColumn, RelationId } from 'typeorm';
import { Column, PrimaryColumn } from 'typeorm';
import { Entity } from 'typeorm';

@Entity()
export class Farm {

  constructor(data?: any) {
    if (data) this.setData(data);
  }

  @PrimaryColumn()
  public registerNumber: number;

  @Column({ nullable: false })
  public name: string;

  @Column()
  public latitude?: number;

  @Column()
  public longitude?: number;

  @Column()
  public area?: number;

  @RelationId((farm: Farm) => farm.sidewalk)
  @Column()  
  public sidewalkId: string;

  @RelationId((farm: Farm) => farm.owner)
  @Column()  
  public ownerId: string;

  @RelationId((farm: Farm) => farm.office)
  @Column()  
  public officeId: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @ManyToOne(type => Sidewalk, sidewalk => sidewalk.farms)
  public sidewalk: Sidewalk;

  @ManyToOne(type => Owner, owner => owner.farms)
  public owner: Owner;

  @ManyToOne(type => Office, office => office.farms)
  public office: Office;

  @OneToMany(type => Visit, visit => visit.farm)
  public visits: Visit[];

  public setData(data) {
    if (data) {
      if (data.registerNumber) this.registerNumber = data.registerNumber;
      if (data.name) this.name = data.name;
      if (data.latitude) this.latitude = data.latitude;
      if (data.longitude) this.longitude = data.longitude;
      if (data.area) this.area = data.area;
      if (data.sidewalkId) this.sidewalkId = data.sidewalkId;
      if (data.ownerId) this.ownerId = data.ownerId;
      if (data.officeId) this.officeId = data.officeId;
    }

    if (typeof data.isRemove === 'boolean') {
      this.isRemove = data.isRemove;
    }
  }
}
