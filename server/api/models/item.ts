import { Qualification } from './qualification';
import { Category } from './category';
import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn, RelationId } from 'typeorm';

@Entity()
export class Item {

  @PrimaryColumn({ type: 'int' })
  public id: number;

  @Column({length: 1500})
  public description: string;

  @ManyToOne(type => Category, category => category.items)
  public category: Category;

  @OneToMany(type => Qualification, qualification => qualification.item)
  public qualifications: Qualification[];

  @RelationId((item: Item) => item.category)
  @Column()  
  public categoryId: number;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

}
