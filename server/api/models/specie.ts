import { getRepository, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ProductiveActivity } from './productive-activity';

@Entity()
export class Specie {

  @PrimaryColumn({ type: 'int' })
  public id: number;

  @Column()
  public description: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @OneToMany(type => ProductiveActivity, productiveActivity => productiveActivity.specie)
  public productiveActivities: ProductiveActivity[];
}

export class SpeciesConstants {
  static readonly species = {
    bovine: 'BOVINA',
    bisons: 'BUFFALINA',
    porcine: 'PORCINA',
    sheepGoat: 'OVINO CAPRINA',
    equine: 'EQUINO',
    poultry: 'AVE DE CORRAL',
    zoocria: 'ZOOCRIA',
    layingBirds: 'AVE DE POSTURA',
    fatteningBirds: 'AVE DE ENGORDE',
    geneticMaterial: 'MATERIAL GENETICO'
  }
}
