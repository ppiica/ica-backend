import { Farm } from './farm';
import { OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { Entity, Column, PrimaryColumn, getRepository } from 'typeorm';
import { Manager } from './manager';

@Entity()
export class Owner {

  constructor(data?: any) {
    if (data) {
      this.setData(data);
    }
  }

  @PrimaryColumn({ length: 12 })
  public id?: string;

  @Column({ nullable: false, length: 45 })
  public firstName: string;

  @Column({ nullable: false, length: 45 })
  public lastNameOne?: string;

  @Column({ length: 45, nullable: true })
  public lastNameTwo?: string;

  @Column({ nullable: false, type: 'int' })
  public phone?: number;

  @Column({ nullable: false, length: 254 })
  public email?: string;

  @Column({ nullable: true, length: 100 })
  public address?: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @OneToMany(type => Farm, farm => farm.owner)
  public farms: Farm[];
  
  @Column({asExpression: "concat(`firstName`, ' ', `lastNameOne`, IFNULL(concat(' ', `lastNameTwo`), ''))"})
  public fullName: string;

  public setData(data) {    
      if (data.id) this.id = data.id;
      if (data.firstName) this.firstName = data.firstName;
      if (data.lastNameOne) this.lastNameOne = data.lastNameOne;
      if (data.lastNameTwo) this.lastNameTwo = data.lastNameTwo;
      if (data.phone) this.phone = data.phone;
      if (data.email) this.email = data.email;
      if (data.address) this.address = data.address;
      if (typeof data.isRemove === 'boolean') {
        this.isRemove = data.isRemove;
      }
  }

}
