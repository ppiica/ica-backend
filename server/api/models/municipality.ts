import { Department } from './department';
import { Entity, PrimaryColumn, Column, OneToMany, ManyToOne, CreateDateColumn, UpdateDateColumn, RelationId } from 'typeorm';
import { Sidewalk } from './sidewalk';

@Entity()
export class Municipality {

  @PrimaryColumn({ type: 'int' })
  public id: number;

  @Column({ length: 50 })
  public name: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @ManyToOne(type => Department, department => department.municipalities, { nullable: false })
  public department?: Department;

  @OneToMany(type => Sidewalk, sidewalk => sidewalk.municipality )
  public sidewalks: Sidewalk[];

  @RelationId((municipality: Municipality) => municipality.department)
  @Column()
  public departmentId: number;

}