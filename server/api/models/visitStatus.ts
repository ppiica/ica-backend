import { Visit } from './visit';
import { PrimaryGeneratedColumn, Column, Entity, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class VisitStatus {
  
  @PrimaryGeneratedColumn({ type: 'int' })
  public id: number;
  
  @Column({ length: 45, unique: true})
  public description: string;
  
  @OneToMany(type => Visit, visit => visit.status)
  public visits: Visit[];
  
  @CreateDateColumn()
  public createdAt?: Date;
  
  @UpdateDateColumn()
  public updatedAt?: Date;
  
}

export class VisitStatusConstants {
  static readonly STATUS_IN_PROCESS = 'STATUS_IN_PROCESS';
  static readonly STATUS_FINISHED = 'STATUS_FINISHED';
  static readonly STATUS_APROVED = 'STATUS_APROVED';
  static readonly STATUS_CONDITIONED = 'STATUS_CONDITIONED';
  static readonly STATUS_NOT_APROVED = 'STATUS_NOT_APROVED';
  
  static readonly VisitsStatus= [
    {
      id: 1,
      description: VisitStatusConstants.STATUS_IN_PROCESS
    },
    {
      id: 2,
      description: VisitStatusConstants.STATUS_FINISHED
    },
    {
      id: 3,
      description: VisitStatusConstants.STATUS_APROVED
    },
    {
      id: 4,
      description: VisitStatusConstants.STATUS_CONDITIONED
    },
    {
      id: 5,
      description: VisitStatusConstants.STATUS_NOT_APROVED
    }
  ];
}