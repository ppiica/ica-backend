import { Technical } from './technical';
import { Coordinator } from './coordinator';
import { Role } from './role';
import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, getRepository, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn, RelationId } from "typeorm";
import { UserRoleForm } from './user-role-form';
import { Manager } from './manager';

enum USER_TYPES {
  ADMIN = "0",
  COORDINATOR = "1",
  TECHNICIAN = "2"
};

@Entity()
export class User {

  constructor(data?: any) {
    if (data) {
      this.setData(data);
    }
  }

  @PrimaryColumn({ unique: true, length: 254 })
  public email?: string;

  @Column({ length: 12 })
  public id: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @Column({ nullable: false, length: 100 })
  public password: string;

  @RelationId((user: User) => user.role)
  @Column()
  public roleId: number;

  @ManyToOne(type => Role, role => role.users)
  public role?: Role;

  @OneToMany(type => UserRoleForm, userRoleForm => userRoleForm.user)
  public userRoleForms?: UserRoleForm[];

  public setData(data) {
    if (data.id) this.id = data.id;
    if (data.email) this.email = data.email;
    if (data.password) this.password = data.password;
    if (data.roleId) this.roleId = data.roleId;
    if (data.profile) {
      this.profile = data.profile;
      this.profile.id = data.id;
    };
    if (typeof data.isRemove === 'boolean') {
      this.isRemove = data.isRemove;
    }
  }

  public profile?: Coordinator | Manager | Technical;

}


export const USER_TYPES_OBJECTS = [
  {
    id: USER_TYPES.ADMIN,
    name: USER_TYPES[0]
  },
  {
    id: USER_TYPES.COORDINATOR,
    name: USER_TYPES[1]
  },
  {
    id: USER_TYPES.TECHNICIAN,
    name: USER_TYPES[2]
  }
];

export class UserConstants  {
  static readonly DEFAULT_MANAGER_ID: number = 1;
  static readonly MASTER_EMAIL: string = 'MASTER@MASTER.com';
  static readonly ROLE_MASTER: string = 'MASTER';
  static readonly ROLE_MANAGER: string = 'MANAGER';
  static readonly ROLE_TECHNICIAN: string = 'TECHNICIAN'
  static readonly ROLE_COORDINATOR: string = 'COORDINATOR'
}