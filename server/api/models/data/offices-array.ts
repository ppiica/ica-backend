export const officesAray =[
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "EL ENCANTO",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "LA CHORRERA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "LA PEDRERA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "LA VICTORIA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "LETICIA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "MIRITI PARANA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "PUERTO ALEGRIA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "PUERTO ARICA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "PUERTO NARINO",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "PUERTO SANTANDER",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "AMAZONAS",
        "MUNICIPIO": "TARAPACA",
        "OFICINALOCAL": "LETICIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ABEJORRAL",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ABRIAQUI",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ALEJANDRIA",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "AMAGA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "AMALFI",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ANDES",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ANGELOPOLIS",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ANGOSTURA",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ANORI",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ANZA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "APARTADO",
        "OFICINALOCAL": "CHIGORODO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ARBOLETES",
        "OFICINALOCAL": "ARBOLETES"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ARGELIA",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ARMENIA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "BARBOSA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "BELLO",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "BELMIRA",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "BETANIA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "BETULIA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "BRICENO",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "BURITICA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CACERES",
        "OFICINALOCAL": "CAUCASIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CAICEDO",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CALDAS",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CAMPAMENTO",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CANASGORDAS",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CARACOLI",
        "OFICINALOCAL": "PUERTO BERRIO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CARAMANTA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CAREPA",
        "OFICINALOCAL": "CHIGORODO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CAROLINA",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CAUCASIA",
        "OFICINALOCAL": "CAUCASIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CHIGORODO",
        "OFICINALOCAL": "CHIGORODO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CISNEROS",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CIUDAD BOLIVAR",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "COCORNA",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CONCEPCION",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "CONCORDIA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "COPACABANA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "DABEIBA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "DON MATIAS",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "EBEJICO",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "EL BAGRE",
        "OFICINALOCAL": "CAUCASIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "EL CARMEN DE VIBORAL",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "EL SANTUARIO",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ENTRERRIOS",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ENVIGADO",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "FREDONIA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "FRONTINO",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "GIRALDO",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "GIRARDOTA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "GOMEZ PLATA",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "GRANADA",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "GUADALUPE",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "GUARNE",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "GUATAPE",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "HELICONIA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "HISPANIA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ITAGUI",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ITUANGO",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "JARDIN",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "JERICO ",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "LA CEJA",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "LA ESTRELLA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "LA PINTADA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "LA UNION",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "LIBORINA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "MACEO",
        "OFICINALOCAL": "PUERTO BERRIO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "MARINILLA",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "MEDELLIN",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "MONTEBELLO",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "MURINDO",
        "OFICINALOCAL": "TURBO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "MUTATA",
        "OFICINALOCAL": "CHIGORODO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "NARINO",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "NECHI",
        "OFICINALOCAL": "CAUCASIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "NECOCLI",
        "OFICINALOCAL": "NECOCLI"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "OLAYA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "PENOL",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "PEQUE",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "PUEBLORRICO",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "PUERTO BERRIO",
        "OFICINALOCAL": "PUERTO BERRIO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "PUERTO NARE",
        "OFICINALOCAL": "PUERTO BERRIO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "PUERTO TRIUNFO",
        "OFICINALOCAL": "PUERTO BERRIO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "REMEDIOS",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "RETIRO",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "RIONEGRO",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SABANALARGA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SABANETA",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SALGAR",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN ANDRES DE CUERQUIA",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN CARLOS",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN FRANCISCO",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN JERONIMO",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN JOSE DE LA MONTANA",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN JUAN DE URABA",
        "OFICINALOCAL": "ARBOLETES"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN LUIS",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN PEDRO DE LOS MILAGROS",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN PEDRO DE URABA",
        "OFICINALOCAL": "SAN PEDRO DE URABA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN RAFAEL",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN ROQUE",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SANTA BARBARA ",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SANTA FE DE ANTIOQUIA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SANTA ROSA DE OSOS",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SANTO DOMINGO",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SAN VICENTE FERRER",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SEGOVIA",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SONSON",
        "OFICINALOCAL": "RIONEGRO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "SOPETRAN",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "TAMESIS",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "TARAZA",
        "OFICINALOCAL": "CAUCASIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "TARSO",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "TITIRIBI",
        "OFICINALOCAL": "MEDELLIN (TULIO OSPINA)"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "TOLEDO",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "TURBO",
        "OFICINALOCAL": "TURBO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "URAMITA",
        "OFICINALOCAL": "SANTAFE DE ANTIOQUIA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "URRAO",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "VALDIVIA",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "VALPARAISO",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "VEGACHI",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "VENECIA",
        "OFICINALOCAL": "LA PINTADA"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "VIGIA DEL FUERTE",
        "OFICINALOCAL": "TURBO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "YALI",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "YARUMAL",
        "OFICINALOCAL": "SANTA ROSA DE OSOS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "YOLOMBO",
        "OFICINALOCAL": "CISNEROS"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "YONDO",
        "OFICINALOCAL": "PUERTO BERRIO"
    },
    {
        "DEPARTAMENTO": "ANTIOQUIA",
        "MUNICIPIO": "ZARAGOZA",
        "OFICINALOCAL": "CAUCASIA"
    },
    {
        "DEPARTAMENTO": "ARAUCA",
        "MUNICIPIO": "ARAUCA",
        "OFICINALOCAL": "ARAUCA"
    },
    {
        "DEPARTAMENTO": "ARAUCA",
        "MUNICIPIO": "ARAUQUITA",
        "OFICINALOCAL": "ARAUQUITA"
    },
    {
        "DEPARTAMENTO": "ARAUCA",
        "MUNICIPIO": "CRAVO NORTE",
        "OFICINALOCAL": "CRAVO NORTE"
    },
    {
        "DEPARTAMENTO": "ARAUCA",
        "MUNICIPIO": "FORTUL",
        "OFICINALOCAL": "SARAVENA"
    },
    {
        "DEPARTAMENTO": "ARAUCA",
        "MUNICIPIO": "PUERTO RONDON",
        "OFICINALOCAL": "PUERTO RONDON"
    },
    {
        "DEPARTAMENTO": "ARAUCA",
        "MUNICIPIO": "SARAVENA",
        "OFICINALOCAL": "SARAVENA"
    },
    {
        "DEPARTAMENTO": "ARAUCA",
        "MUNICIPIO": "TAME",
        "OFICINALOCAL": "TAME"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "BARANOA",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "BARRANQUILLA",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "CAMPO DE LA CRUZ",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "CANDELARIA",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "GALAPA",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "JUAN DE ACOSTA",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "LURUACO",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "MALAMBO",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "MANATI",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "PALMAR DE VARELA",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "PIOJO",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "POLONUEVO",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "PONEDERA",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "PUERTO COLOMBIA",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "REPELON",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "SABANAGRANDE",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "SABANALARGA",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "SANTA LUCIA",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "SANTO TOMAS",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "SOLEDAD",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "SUAN",
        "OFICINALOCAL": "SABANALARGA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "TUBARA",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "ATLANTICO",
        "MUNICIPIO": "USIACURI",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "ACHI",
        "OFICINALOCAL": "MAGANGUE"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "ALTOS DEL ROSARIO",
        "OFICINALOCAL": "EL BANCO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "ARENAL",
        "OFICINALOCAL": "MORALES"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "ARJONA",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "ARROYOHONDO",
        "OFICINALOCAL": "SAN JUAN NEPOMUCENO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "BARRANCO DE LOBA",
        "OFICINALOCAL": "EL BANCO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "CALAMAR",
        "OFICINALOCAL": "SAN JUAN NEPOMUCENO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "CANTAGALLO",
        "OFICINALOCAL": "SAN PABLO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "CARTAGENA DE INDIAS",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "CICUCO",
        "OFICINALOCAL": "MOMPOS"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "CLEMENCIA",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "CORDOBA",
        "OFICINALOCAL": "EL CARMEN DE BOLIVAR"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "EL CARMEN DE BOLIVAR",
        "OFICINALOCAL": "EL CARMEN DE BOLIVAR"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "EL GUAMO",
        "OFICINALOCAL": "SAN JUAN NEPOMUCENO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "EL PENON",
        "OFICINALOCAL": "EL BANCO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "HATILLO DE LOBA",
        "OFICINALOCAL": "MOMPOS"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "MAGANGUE",
        "OFICINALOCAL": "MAGANGUE"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "MAHATES",
        "OFICINALOCAL": "EL CARMEN DE BOLIVAR"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "MARGARITA",
        "OFICINALOCAL": "MOMPOS"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "MARIA LA BAJA",
        "OFICINALOCAL": "EL CARMEN DE BOLIVAR"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "MOMPOS",
        "OFICINALOCAL": "MOMPOS"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "MONTECRISTO",
        "OFICINALOCAL": "MAGANGUE"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "MORALES",
        "OFICINALOCAL": "MORALES"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "NOROSI",
        "OFICINALOCAL": "MORALES"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "PINILLOS",
        "OFICINALOCAL": "MAGANGUE"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "REGIDOR",
        "OFICINALOCAL": "MORALES"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "RIO VIEJO",
        "OFICINALOCAL": "MORALES"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN CRISTOBAL",
        "OFICINALOCAL": "SAN JUAN NEPOMUCENO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN ESTANISLAO",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN FERNANDO",
        "OFICINALOCAL": "MOMPOS"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN JACINTO",
        "OFICINALOCAL": "EL CARMEN DE BOLIVAR"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN JACINTO DEL CAUCA",
        "OFICINALOCAL": "MAGANGUE"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN JUAN NEPOMUCENO",
        "OFICINALOCAL": "SAN JUAN NEPOMUCENO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN MARTIN DE LOBA",
        "OFICINALOCAL": "EL BANCO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SAN PABLO",
        "OFICINALOCAL": "SAN PABLO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SANTA CATALINA",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SANTA ROSA",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SANTA ROSA DEL SUR",
        "OFICINALOCAL": "SANTA ROSA DEL SUR"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SIMITI",
        "OFICINALOCAL": "SANTA ROSA DEL SUR"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "SOPLAVIENTO",
        "OFICINALOCAL": "SAN JUAN NEPOMUCENO"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "TALAIGUA NUEVO",
        "OFICINALOCAL": "MOMPOS"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "TIQUISIO",
        "OFICINALOCAL": "MAGANGUE"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "TURBACO",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "TURBANA",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "VILLANUEVA",
        "OFICINALOCAL": "CARTAGENA"
    },
    {
        "DEPARTAMENTO": "BOLIVAR",
        "MUNICIPIO": "ZAMBRANO",
        "OFICINALOCAL": "EL CARMEN DE BOLIVAR"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "ALMEIDA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "AQUITANIA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "ARCABUCO",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BELEN",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BERBEO",
        "OFICINALOCAL": "MIRAFLORES"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BETEITIVA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BOAVITA",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BOYACA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BRICENO",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BUENAVISTA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "BUSBANZA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CALDAS",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CAMPOHERMOSO",
        "OFICINALOCAL": "MIRAFLORES"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CERINZA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHINAVITA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHIQUINQUIRA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHIQUIZA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHISCAS",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHITA",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHITARAQUE",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHIVATA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CHIVOR",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CIENEGA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "COMBITA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "COPER",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CORRALES",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "COVARACHIA",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CUBARA",
        "OFICINALOCAL": "CUBARA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CUCAITA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "CUITIVA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "DUITAMA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "EL COCUY",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "EL ESPINO",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "FIRAVITOBA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "FLORESTA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "GACHANTIVA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "GAMEZA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "GARAGOA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "GUACAMAYAS",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "GUATEQUE",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "GUAYATA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "GUICAN",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "IZA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "JENESANO",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "JERICO",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "LABRANZAGRANDE",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "LA CAPILLA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "LA UVITA",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "LA VICTORIA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MACANAL",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MARIPI",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MIRAFLORES",
        "OFICINALOCAL": "MIRAFLORES"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MONGUA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MONGUI",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MONIQUIRA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MOTAVITA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "MUZO",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "NOBSA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "NUEVO COLON",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "OICATA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "OTANCHE",
        "OFICINALOCAL": "PUERTO BOYACA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PACHAVITA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PAEZ",
        "OFICINALOCAL": "MIRAFLORES"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PAIPA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PAJARITO",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PANQUEBA",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PAUNA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PAYA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PAZ DE RIO",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PESCA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PISBA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "PUERTO BOYACA",
        "OFICINALOCAL": "PUERTO BOYACA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "QUIPAMA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "RAMIRIQUI",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "RAQUIRA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "RONDON",
        "OFICINALOCAL": "MIRAFLORES"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SABOYA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SACHICA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SAMACA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SAN EDUARDO",
        "OFICINALOCAL": "MIRAFLORES"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SAN JOSE DE PARE",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SAN LUIS DE GACENO",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SAN MATEO",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SAN MIGUEL DE SEMA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SAN PABLO DE BORBUR",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SANTA MARIA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SANTANA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SANTA ROSA DE VITERBO",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SANTA SOFIA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SATIVANORTE",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SATIVASUR",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SIACHOQUE",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SOATA",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SOCHA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SOCOTA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SOGAMOSO",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SOMONDOCO",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SORA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SORACA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SOTAQUIRA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SUSACON",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SUTAMARCHAN",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "SUTATENZA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TASCO",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TENZA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TIBANA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TIBASOSA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TINJACA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TIPACOQUE",
        "OFICINALOCAL": "SOATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TOCA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TOGUI",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TOPAGA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TOTA",
        "OFICINALOCAL": "SOGAMOSO"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TUNJA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TUNUNGUA",
        "OFICINALOCAL": "CHIQUINQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TURMEQUE",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TUTA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "TUTAZA",
        "OFICINALOCAL": "SURBATA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "UMBITA",
        "OFICINALOCAL": "GARAGOA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "VENTAQUEMADA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "VILLA DE LEYVA",
        "OFICINALOCAL": "MONIQUIRA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "VIRACACHA",
        "OFICINALOCAL": "TUNJA"
    },
    {
        "DEPARTAMENTO": "BOYACA",
        "MUNICIPIO": "ZETAQUIRA",
        "OFICINALOCAL": "MIRAFLORES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "AGUADAS",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "ANSERMA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "ARANZAZU",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "BELALCAZAR",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "CHINCHINA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "FILADELFIA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "LA DORADA",
        "OFICINALOCAL": "LA DORADA"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "LA MERCED",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "MANIZALES",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "MANZANARES",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "MARMATO",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "MARQUETALIA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "MARULANDA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "NEIRA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "NORCASIA",
        "OFICINALOCAL": "LA DORADA"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "PACORA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "PALESTINA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "PENSILVANIA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "RIOSUCIO",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "RISARALDA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "SALAMINA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "SAMANA",
        "OFICINALOCAL": "LA DORADA"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "SAN JOSE",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "SUPIA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "VICTORIA",
        "OFICINALOCAL": "LA DORADA"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "VILLAMARIA",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CALDAS",
        "MUNICIPIO": "VITERBO",
        "OFICINALOCAL": "MANIZALES"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "ALBANIA",
        "OFICINALOCAL": "ALBANIA Cq"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "BELEN DE LOS ANDAQUIES",
        "OFICINALOCAL": "ALBANIA Cq"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "CARTAGENA DEL CHAIRA",
        "OFICINALOCAL": "CARTAGENA DEL CHAIRA"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "CURILLO",
        "OFICINALOCAL": "ALBANIA Cq"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "EL DONCELLO",
        "OFICINALOCAL": "EL DONCELLO"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "EL PAUJIL",
        "OFICINALOCAL": "EL DONCELLO"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "FLORENCIA",
        "OFICINALOCAL": "FLORENCIA"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "LA MONTANITA",
        "OFICINALOCAL": "FLORENCIA"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "MILAN",
        "OFICINALOCAL": "FLORENCIA"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "MORELIA",
        "OFICINALOCAL": "FLORENCIA"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "PUERTO RICO",
        "OFICINALOCAL": "SAN VICENTE DEL CAGUAN"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "SAN JOSE DEL FRAGUA",
        "OFICINALOCAL": "ALBANIA Cq"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "SAN VICENTE DEL CAGUAN",
        "OFICINALOCAL": "SAN VICENTE DEL CAGUAN"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "SOLANO",
        "OFICINALOCAL": "FLORENCIA"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "SOLITA",
        "OFICINALOCAL": "FLORENCIA"
    },
    {
        "DEPARTAMENTO": "CAQUETA",
        "MUNICIPIO": "VALPARAISO",
        "OFICINALOCAL": "FLORENCIA"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "AGUAZUL",
        "OFICINALOCAL": "YOPAL"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "CHAMEZA",
        "OFICINALOCAL": "YOPAL"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "HATO COROZAL",
        "OFICINALOCAL": "PAZ DE ARIPORO"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "LA SALINA",
        "OFICINALOCAL": "PAZ DE ARIPORO"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "MANI",
        "OFICINALOCAL": "YOPAL"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "MONTERREY",
        "OFICINALOCAL": "TAURAMENA"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "NUNCHIA",
        "OFICINALOCAL": "YOPAL"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "OROCUE",
        "OFICINALOCAL": "TRINIDAD"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "PAZ DE ARIPORO",
        "OFICINALOCAL": "PAZ DE ARIPORO"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "PORE",
        "OFICINALOCAL": "PAZ DE ARIPORO"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "RECETOR",
        "OFICINALOCAL": "YOPAL"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "SABANALARGA",
        "OFICINALOCAL": "TAURAMENA"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "SACAMA",
        "OFICINALOCAL": "PAZ DE ARIPORO"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "SAN LUIS DE PALENQUE",
        "OFICINALOCAL": "TRINIDAD"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "TAMARA",
        "OFICINALOCAL": "YOPAL"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "TAURAMENA",
        "OFICINALOCAL": "TAURAMENA"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "TRINIDAD",
        "OFICINALOCAL": "TRINIDAD"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "VILLANUEVA",
        "OFICINALOCAL": "TAURAMENA"
    },
    {
        "DEPARTAMENTO": "CASANARE",
        "MUNICIPIO": "YOPAL",
        "OFICINALOCAL": "YOPAL"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "ALMAGUER",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "ARGELIA",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "BALBOA",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "BOLIVAR",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "BUENOS AIRES",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "CAJIBIO",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "CALDONO",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "CALOTO",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "CORINTO",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "EL TAMBO",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "FLORENCIA",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "GUACHENE",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "GUAPI",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "INZA",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "JAMBALO",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "LA SIERRA",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "LA VEGA",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "LOPEZ DE MICAY",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "MERCADERES",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "MIRANDA",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "MORALES",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "PADILLA",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "PAEZ",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "PATIA",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "PIAMONTE",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "PIENDAMO",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "POPAYAN",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "PUERTO TEJADA",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "PURACE",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "ROSAS",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "SAN SEBASTIAN",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "SANTANDER DE QUILICHAO",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "SANTA ROSA",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "SILVIA",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "SOTARA",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "SUAREZ",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "SUCRE",
        "OFICINALOCAL": "EL BORDO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "TIMBIO",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "TIMBIQUI",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "TORIBIO",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "TOTORO",
        "OFICINALOCAL": "POPAYAN"
    },
    {
        "DEPARTAMENTO": "CAUCA",
        "MUNICIPIO": "VILLA RICA",
        "OFICINALOCAL": "SANTANDER DE QUILICHAO"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "AGUACHICA",
        "OFICINALOCAL": "AGUACHICA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "AGUSTIN CODAZZI",
        "OFICINALOCAL": "AGUSTIN CODAZZI"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "ASTREA",
        "OFICINALOCAL": "BOSCONIA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "BECERRIL",
        "OFICINALOCAL": "AGUSTIN CODAZZI"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "BOSCONIA",
        "OFICINALOCAL": "BOSCONIA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "CHIMICHAGUA",
        "OFICINALOCAL": "BOSCONIA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "CHIRIGUANA",
        "OFICINALOCAL": "CHIRIGUANA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "CURUMANI",
        "OFICINALOCAL": "CHIRIGUANA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "EL COPEY",
        "OFICINALOCAL": "BOSCONIA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "EL PASO",
        "OFICINALOCAL": "CHIRIGUANA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "GAMARRA",
        "OFICINALOCAL": "AGUACHICA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "GONZALEZ",
        "OFICINALOCAL": "AGUACHICA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "LA GLORIA",
        "OFICINALOCAL": "PAILITAS"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "LA JAGUA DE IBIRICO",
        "OFICINALOCAL": "CHIRIGUANA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "LA PAZ",
        "OFICINALOCAL": "VALLEDUPAR"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "MANAURE BALCON DEL CESAR",
        "OFICINALOCAL": "VALLEDUPAR"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "PAILITAS",
        "OFICINALOCAL": "PAILITAS"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "PELAYA",
        "OFICINALOCAL": "PAILITAS"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "PUEBLO BELLO",
        "OFICINALOCAL": "VALLEDUPAR"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "RIO DE ORO",
        "OFICINALOCAL": "AGUACHICA"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "SAN ALBERTO",
        "OFICINALOCAL": "SAN ALBERTO"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "SAN DIEGO",
        "OFICINALOCAL": "AGUSTIN CODAZZI"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "SAN MARTIN",
        "OFICINALOCAL": "SAN ALBERTO"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "TAMALAMEQUE",
        "OFICINALOCAL": "PAILITAS"
    },
    {
        "DEPARTAMENTO": "CESAR",
        "MUNICIPIO": "VALLEDUPAR",
        "OFICINALOCAL": "VALLEDUPAR"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "ACANDI",
        "OFICINALOCAL": "ACANDI"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "ALTO BAUDO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "ATRATO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "BAGADO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "BAHIA SOLANO",
        "OFICINALOCAL": "BAHIA SOLANO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "BAJO BAUDO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "BOJAYA",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "CARMEN DEL DARIEN",
        "OFICINALOCAL": "UNGUIA"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "CERTEGUI",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "CONDOTO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "EL CANTON DEL SAN PABLO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "EL CARMEN DE ATRATO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "EL LITORAL DEL SAN JUAN",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "ISTMINA",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "JURADO",
        "OFICINALOCAL": "BAHIA SOLANO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "LLORO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "MEDIO ATRATO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "MEDIO BAUDO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "MEDIO SAN JUAN",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "NOVITA",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "NUQUI",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "QUIBDO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "RIO IRO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "RIO QUITO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "RIOSUCIO",
        "OFICINALOCAL": "UNGUIA"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "SAN JOSE DEL PALMAR",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "SIPI",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "TADO",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "UNGUIA",
        "OFICINALOCAL": "UNGUIA"
    },
    {
        "DEPARTAMENTO": "CHOCO",
        "MUNICIPIO": "UNION PANAMERICANA",
        "OFICINALOCAL": "QUIBDO"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "AYAPEL",
        "OFICINALOCAL": "AYAPEL"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "BUENAVISTA",
        "OFICINALOCAL": "PLANETA RICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "CANALETE",
        "OFICINALOCAL": "EL EBANO"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "CERETE",
        "OFICINALOCAL": "CERETE"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "CHIMA",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "CHINU",
        "OFICINALOCAL": "SAHAGUN"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "CIENAGA DE ORO",
        "OFICINALOCAL": "CERETE"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "COTORRA",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "LA APARTADA",
        "OFICINALOCAL": "AYAPEL"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "LORICA",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "LOS CORDOBAS",
        "OFICINALOCAL": "EL EBANO"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "MOMIL",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "MONITOS",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "MONTELIBANO",
        "OFICINALOCAL": "MONTELIBANO"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "MONTERIA",
        "OFICINALOCAL": "MONTERIA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "PLANETA RICA",
        "OFICINALOCAL": "PLANETA RICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "PUEBLO NUEVO",
        "OFICINALOCAL": "PLANETA RICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "PUERTO ESCONDIDO",
        "OFICINALOCAL": "EL EBANO"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "PUERTO LIBERTADOR",
        "OFICINALOCAL": "MONTELIBANO"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "PURISIMA DE LA CONCEPCION",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "SAHAGUN",
        "OFICINALOCAL": "SAHAGUN"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "SAN ANDRES DE SOTAVENTO",
        "OFICINALOCAL": "SAHAGUN"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "SAN ANTERO",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "SAN BERNARDO DEL VIENTO",
        "OFICINALOCAL": "LORICA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "SAN CARLOS",
        "OFICINALOCAL": "CERETE"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "SAN JOSE DE URE",
        "OFICINALOCAL": "MONTELIBANO"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "SAN PELAYO",
        "OFICINALOCAL": "CERETE"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "TIERRALTA",
        "OFICINALOCAL": "TIERRALTA"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "TUCHIN",
        "OFICINALOCAL": "SAHAGUN"
    },
    {
        "DEPARTAMENTO": "CORDOBA",
        "MUNICIPIO": "VALENCIA",
        "OFICINALOCAL": "VALENCIA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "AGUA DE DIOS",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "ALBAN",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "ANAPOIMA",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "ANOLAIMA",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "APULO",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "ARBELAEZ",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "BELTRAN",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "BITUIMA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "BOJACA",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CABRERA",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CACHIPAY",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CAJICA",
        "OFICINALOCAL": "CHIA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CAPARRAPI",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CAQUEZA",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CARMEN DE CARUPA",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CHAGUANI",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CHIA",
        "OFICINALOCAL": "CHIA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CHIPAQUE",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CHOACHI",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CHOCONTA",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "COGUA",
        "OFICINALOCAL": "ZIPAQUIRA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "COTA",
        "OFICINALOCAL": "CHIA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "CUCUNUBA",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "EL COLEGIO",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "EL PENON",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "EL ROSAL",
        "OFICINALOCAL": "TABIO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "FACATATIVA",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "FOMEQUE",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "FOSCA",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "FUNZA",
        "OFICINALOCAL": "TIBAITATA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "FUQUENE",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "FUSAGASUGA",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GACHALA",
        "OFICINALOCAL": "GACHETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GACHANCIPA",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GACHETA",
        "OFICINALOCAL": "GACHETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GAMA",
        "OFICINALOCAL": "GACHETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GIRARDOT",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GRANADA",
        "OFICINALOCAL": "SOACHA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUACHETA",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUADUAS",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUASCA",
        "OFICINALOCAL": "CHIA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUATAQUI",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUATAVITA",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUAYABAL DE SIQUIMA",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUAYABETAL",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "GUTIERREZ",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "JERUSALEN",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "JUNIN",
        "OFICINALOCAL": "GACHETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "LA CALERA",
        "OFICINALOCAL": "CHIA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "LA MESA",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "LA PALMA",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "LA PENA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "LA VEGA",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "LENGUAZAQUE",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "MACHETA",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "MADRID",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "MANTA",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "MEDINA",
        "OFICINALOCAL": "MEDINA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "MOSQUERA",
        "OFICINALOCAL": "TIBAITATA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "NARINO",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "NEMOCON",
        "OFICINALOCAL": "ZIPAQUIRA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "NILO",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "NIMAIMA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "NOCAIMA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "PACHO",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "PAIME",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "PANDI",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "PARATEBUENO",
        "OFICINALOCAL": "MEDINA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "PASCA",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "PUERTO SALGAR",
        "OFICINALOCAL": "LA DORADA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "PULI",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "QUEBRADANEGRA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "QUETAME",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "QUIPILE",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "RICAURTE",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SAN ANTONIO DEL TEQUENDAMA",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SAN BERNARDO",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SAN CAYETANO",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SAN FRANCISCO",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SAN JUAN DE RIOSECO",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SASAIMA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SESQUILE",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SIBATE",
        "OFICINALOCAL": "SOACHA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SILVANIA",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SIMIJACA",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SOACHA",
        "OFICINALOCAL": "SOACHA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SOPO",
        "OFICINALOCAL": "CHIA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SUBACHOQUE",
        "OFICINALOCAL": "TABIO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SUESCA",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SUPATA",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SUSA",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "SUTATAUSA",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TABIO",
        "OFICINALOCAL": "TABIO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TAUSA",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TENA",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TENJO",
        "OFICINALOCAL": "TABIO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TIBACUY",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TIBIRITA",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TOCAIMA",
        "OFICINALOCAL": "GIRARDOT"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TOCANCIPA",
        "OFICINALOCAL": "ZIPAQUIRA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "TOPAIPI",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "UBALA",
        "OFICINALOCAL": "GACHETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "UBAQUE",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "UNE",
        "OFICINALOCAL": "CAQUEZA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "UTICA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VENECIA",
        "OFICINALOCAL": "FUSAGASUGA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VERGARA",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VIANI",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VILLA DE SAN DIEGO DE UBATE",
        "OFICINALOCAL": "UBATE"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VILLAGOMEZ",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VILLAPINZON",
        "OFICINALOCAL": "CHOCONTA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VILLETA",
        "OFICINALOCAL": "VILLETA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "VIOTA",
        "OFICINALOCAL": "LA MESA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "YACOPI",
        "OFICINALOCAL": "PACHO"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "ZIPACON",
        "OFICINALOCAL": "FACATATIVA"
    },
    {
        "DEPARTAMENTO": "CUNDINAMARCA",
        "MUNICIPIO": "ZIPAQUIRA",
        "OFICINALOCAL": "ZIPAQUIRA"
    },
    {
        "DEPARTAMENTO": "DISTRITO CAPITAL",
        "MUNICIPIO": "BOGOTA D.C.",
        "OFICINALOCAL": "SOACHA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "BARRANCO MINAS",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "CACAHUAL",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "INIRIDA",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "LA GUADALUPE",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "MAPIRIPANA",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "MORICHAL",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "PANA PANA",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "PUERTO COLOMBIA",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAINIA",
        "MUNICIPIO": "SAN FELIPE",
        "OFICINALOCAL": "INIRIDA"
    },
    {
        "DEPARTAMENTO": "GUAVIARE",
        "MUNICIPIO": "CALAMAR",
        "OFICINALOCAL": "SAN JOSE DEL GUAVIARE"
    },
    {
        "DEPARTAMENTO": "GUAVIARE",
        "MUNICIPIO": "EL RETORNO",
        "OFICINALOCAL": "SAN JOSE DEL GUAVIARE"
    },
    {
        "DEPARTAMENTO": "GUAVIARE",
        "MUNICIPIO": "MIRAFLORES",
        "OFICINALOCAL": "SAN JOSE DEL GUAVIARE"
    },
    {
        "DEPARTAMENTO": "GUAVIARE",
        "MUNICIPIO": "SAN JOSE DEL GUAVIARE",
        "OFICINALOCAL": "SAN JOSE DEL GUAVIARE"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "ACEVEDO",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "AGRADO",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "AIPE",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "ALGECIRAS",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "ALTAMIRA",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "BARAYA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "CAMPOALEGRE",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "COLOMBIA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "ELIAS",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "GARZON",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "GIGANTE",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "GUADALUPE",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "HOBO",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "IQUIRA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "ISNOS",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "LA ARGENTINA",
        "OFICINALOCAL": "LA PLATA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "LA PLATA",
        "OFICINALOCAL": "LA PLATA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "NATAGA",
        "OFICINALOCAL": "LA PLATA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "NEIVA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "OPORAPA",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "PAICOL",
        "OFICINALOCAL": "LA PLATA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "PALERMO",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "PALESTINA",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "PITAL",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "PITALITO",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "RIVERA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "SALADOBLANCO",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "SAN AGUSTIN",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "SANTA MARIA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "SUAZA",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "TARQUI",
        "OFICINALOCAL": "GARZON"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "TELLO",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "TERUEL",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "TESALIA",
        "OFICINALOCAL": "LA PLATA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "TIMANA",
        "OFICINALOCAL": "PITALITO"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "VILLAVIEJA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "HUILA",
        "MUNICIPIO": "YAGUARA",
        "OFICINALOCAL": "NEIVA"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "ALBANIA",
        "OFICINALOCAL": "MAICAO"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "BARRANCAS",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "DIBULLA",
        "OFICINALOCAL": "RIOHACHA"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "DISTRACCION",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "EL MOLINO",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "FONSECA",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "HATO NUEVO",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "LA JAGUA DEL PILAR",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "MAICAO",
        "OFICINALOCAL": "MAICAO"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "MANAURE",
        "OFICINALOCAL": "RIOHACHA"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "RIOHACHA",
        "OFICINALOCAL": "RIOHACHA"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "SAN JUAN DEL CESAR",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "URIBIA",
        "OFICINALOCAL": "MAICAO"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "URUMITA",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "LA GUAJIRA",
        "MUNICIPIO": "VILLANUEVA",
        "OFICINALOCAL": "SAN JUAN DEL CESAR"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "ALGARROBO",
        "OFICINALOCAL": "FUNDACION"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "ARACATACA",
        "OFICINALOCAL": "FUNDACION"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "ARIGUANI",
        "OFICINALOCAL": "ARIGUANI"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "CERRO DE SAN ANTONIO",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "CHIVOLO",
        "OFICINALOCAL": "PLATO"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "CIENAGA",
        "OFICINALOCAL": "SANTA MARTA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "CONCORDIA",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "EL BANCO",
        "OFICINALOCAL": "EL BANCO"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "EL PINON",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "EL RETEN",
        "OFICINALOCAL": "FUNDACION"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "FUNDACION",
        "OFICINALOCAL": "FUNDACION"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "GUAMAL",
        "OFICINALOCAL": "EL BANCO"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "NUEVA GRANADA",
        "OFICINALOCAL": "ARIGUANI"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "PEDRAZA",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "PIJINO DEL CARMEN",
        "OFICINALOCAL": "SANTA ANA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "PIVIJAY",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "PLATO",
        "OFICINALOCAL": "PLATO"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "PUEBLOVIEJO",
        "OFICINALOCAL": "SANTA MARTA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "REMOLINO",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SABANAS DE SAN ANGEL",
        "OFICINALOCAL": "ARIGUANI"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SALAMINA",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SAN SEBASTIAN DE BUENAVISTA",
        "OFICINALOCAL": "SANTA ANA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SANTA ANA",
        "OFICINALOCAL": "SANTA ANA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SANTA BARBARA DE PINTO",
        "OFICINALOCAL": "SANTA ANA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SANTA MARTA",
        "OFICINALOCAL": "SANTA MARTA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SAN ZENON",
        "OFICINALOCAL": "SANTA ANA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "SITIONUEVO",
        "OFICINALOCAL": "BARRANQUILLA"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "TENERIFE",
        "OFICINALOCAL": "PLATO"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "ZAPAYAN",
        "OFICINALOCAL": "PIVIJAY"
    },
    {
        "DEPARTAMENTO": "MAGDALENA",
        "MUNICIPIO": "ZONA BANANERA",
        "OFICINALOCAL": "SANTA MARTA"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "ACACIAS",
        "OFICINALOCAL": "ACACIAS"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "BARRANCA DE UPIA",
        "OFICINALOCAL": "VILLAVICENCIO"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "CABUYARO",
        "OFICINALOCAL": "PUERTO LOPEZ"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "CASTILLA LA NUEVA",
        "OFICINALOCAL": "ACACIAS"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "CUBARRAL",
        "OFICINALOCAL": "SAN MARTIN Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "CUMARAL",
        "OFICINALOCAL": "RESTREPO Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "EL CALVARIO",
        "OFICINALOCAL": "RESTREPO Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "EL CASTILLO",
        "OFICINALOCAL": "GRANADA Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "EL DORADO",
        "OFICINALOCAL": "SAN MARTIN Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "FUENTE DE ORO",
        "OFICINALOCAL": "GRANADA Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "GRANADA",
        "OFICINALOCAL": "GRANADA Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "GUAMAL",
        "OFICINALOCAL": "ACACIAS"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "LA MACARENA",
        "OFICINALOCAL": "LA MACARENA"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "LEJANIAS",
        "OFICINALOCAL": "GRANADA Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "MAPIRIPAN",
        "OFICINALOCAL": "SAN MARTIN Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "MESETAS",
        "OFICINALOCAL": "SAN JUAN DE ARAMA"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "PUERTO CONCORDIA",
        "OFICINALOCAL": "GRANADA Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "PUERTO GAITAN",
        "OFICINALOCAL": "PUERTO GAITAN"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "PUERTO LLERAS",
        "OFICINALOCAL": "GRANADA Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "PUERTO LOPEZ",
        "OFICINALOCAL": "PUERTO LOPEZ"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "PUERTO RICO",
        "OFICINALOCAL": "GRANADA Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "RESTREPO",
        "OFICINALOCAL": "RESTREPO Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "SAN CARLOS DE GUAROA",
        "OFICINALOCAL": "ACACIAS"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "SAN JUAN DE ARAMA",
        "OFICINALOCAL": "SAN JUAN DE ARAMA"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "SAN JUANITO",
        "OFICINALOCAL": "RESTREPO Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "SAN MARTIN",
        "OFICINALOCAL": "SAN MARTIN Me"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "URIBE",
        "OFICINALOCAL": "SAN JUAN DE ARAMA"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "VILLAVICENCIO",
        "OFICINALOCAL": "VILLAVICENCIO"
    },
    {
        "DEPARTAMENTO": "META",
        "MUNICIPIO": "VISTAHERMOSA",
        "OFICINALOCAL": "SAN JUAN DE ARAMA"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "ALBAN",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "ALDANA",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "ANCUYA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "ARBOLEDA",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "BARBACOAS",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "BELEN",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "BUESACO",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "CHACHAGUI",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "COLON",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "CONSACA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "CONTADERO",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "CORDOBA",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "CUASPUD",
        "OFICINALOCAL": "GUACHUCAL"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "CUMBAL",
        "OFICINALOCAL": "GUACHUCAL"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "CUMBITARA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "EL CHARCO",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "EL PENOL",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "EL ROSARIO",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "EL TABLON DE GOMEZ",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "EL TAMBO",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "FRANCISCO PIZARRO",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "FUNES",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "GUACHUCAL",
        "OFICINALOCAL": "GUACHUCAL"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "GUAITARILLA",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "GUALMATAN",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "ILES",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "IMUES",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "IPIALES",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LA CRUZ",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LA FLORIDA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LA LLANADA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LA TOLA",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LA UNION",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LEIVA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LINARES",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "LOS ANDES",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "MAGUI",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "MALLAMA",
        "OFICINALOCAL": "GUACHUCAL"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "MOSQUERA",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "NARINO",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "OLAYA HERRERA",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "OSPINA",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "PASTO",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "POLICARPA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "POTOSI",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "PROVIDENCIA",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "PUERRES",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "PUPIALES",
        "OFICINALOCAL": "IPIALES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "RICAURTE",
        "OFICINALOCAL": "GUACHUCAL"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "ROBERTO PAYAN",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SAMANIEGO",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SAN ANDRES DE TUMACO",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SAN BERNARDO",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SANDONA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SAN LORENZO",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SAN PABLO",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SAN PEDRO DE CARTAGO",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SANTA BARBARA",
        "OFICINALOCAL": "TUMACO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SANTACRUZ",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "SAPUYES",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "TAMINANGO",
        "OFICINALOCAL": "LA CRUZ"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "TANGUA",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "TUQUERRES",
        "OFICINALOCAL": "TUQUERRES"
    },
    {
        "DEPARTAMENTO": "NARINO",
        "MUNICIPIO": "YACUANQUER",
        "OFICINALOCAL": "PASTO"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "ABREGO",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "ARBOLEDAS",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "BOCHALEMA",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "BUCARASICA",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "CACHIRA",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "CACOTA",
        "OFICINALOCAL": "PAMPLONA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "CHINACOTA",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "CHITAGA",
        "OFICINALOCAL": "PAMPLONA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "CONVENCION",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "CUCUTA",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "CUCUTILLA",
        "OFICINALOCAL": "PAMPLONA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "DURANIA",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "EL CARMEN",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "EL TARRA",
        "OFICINALOCAL": "TIBU"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "EL ZULIA",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "GRAMALOTE",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "HACARI",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "HERRAN",
        "OFICINALOCAL": "TOLEDO NS"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "LABATECA",
        "OFICINALOCAL": "TOLEDO NS"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "LA ESPERANZA",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "LA PLAYA",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "LOS PATIOS",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "LOURDES",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "MUTISCUA",
        "OFICINALOCAL": "PAMPLONA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "OCANA",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "PAMPLONA",
        "OFICINALOCAL": "PAMPLONA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "PAMPLONITA",
        "OFICINALOCAL": "PAMPLONA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "PUERTO SANTANDER",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "RAGONVALIA",
        "OFICINALOCAL": "TOLEDO NS"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "SALAZAR",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "SAN CALIXTO",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "SAN CAYETANO",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "SANTIAGO",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "SARDINATA",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "SILOS",
        "OFICINALOCAL": "PAMPLONA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "TEORAMA",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "TIBU",
        "OFICINALOCAL": "TIBU"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "TOLEDO",
        "OFICINALOCAL": "TOLEDO NS"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "VILLA CARO",
        "OFICINALOCAL": "OCAÑA"
    },
    {
        "DEPARTAMENTO": "NORTE SANTANDER",
        "MUNICIPIO": "VILLA DEL ROSARIO",
        "OFICINALOCAL": "CUCUTA"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "COLON",
        "OFICINALOCAL": "SIBUNDOY"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "MOCOA",
        "OFICINALOCAL": "MOCOA"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "ORITO",
        "OFICINALOCAL": "VALLE DEL GUAMUEZ"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "PUERTO ASIS",
        "OFICINALOCAL": "PUERTO ASIS"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "PUERTO CAICEDO",
        "OFICINALOCAL": "PUERTO ASIS"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "PUERTO GUZMAN",
        "OFICINALOCAL": "PUERTO GUZMAN"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "PUERTO LEGUIZAMO",
        "OFICINALOCAL": "PUERTO LEGUIZAMO"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "SAN FRANCISCO",
        "OFICINALOCAL": "SIBUNDOY"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "SAN MIGUEL",
        "OFICINALOCAL": "VALLE DEL GUAMUEZ"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "SANTIAGO",
        "OFICINALOCAL": "SIBUNDOY"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "SIBUNDOY",
        "OFICINALOCAL": "SIBUNDOY"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "VALLE DEL GUAMUEZ",
        "OFICINALOCAL": "VALLE DEL GUAMUEZ"
    },
    {
        "DEPARTAMENTO": "PUTUMAYO",
        "MUNICIPIO": "VILLAGARZON",
        "OFICINALOCAL": "MOCOA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "ARMENIA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "BUENAVISTA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "CALARCA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "CIRCASIA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "CORDOBA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "FILANDIA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "GENOVA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "LA TEBAIDA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "MONTENEGRO",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "PIJAO",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "QUIMBAYA",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "QUINDIO",
        "MUNICIPIO": "SALENTO",
        "OFICINALOCAL": "ARMENIA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "APIA",
        "OFICINALOCAL": "BELEN DE UMBRIA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "BALBOA",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "BELEN DE UMBRIA",
        "OFICINALOCAL": "BELEN DE UMBRIA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "DOSQUEBRADAS",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "GUATICA",
        "OFICINALOCAL": "BELEN DE UMBRIA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "LA CELIA",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "LA VIRGINIA",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "MARSELLA",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "MISTRATO",
        "OFICINALOCAL": "BELEN DE UMBRIA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "PEREIRA",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "PUEBLO RICO",
        "OFICINALOCAL": "BELEN DE UMBRIA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "QUINCHIA",
        "OFICINALOCAL": "BELEN DE UMBRIA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "SANTA ROSA DE CABAL",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "RISARALDA",
        "MUNICIPIO": "SANTUARIO",
        "OFICINALOCAL": "PEREIRA"
    },
    {
        "DEPARTAMENTO": "S.ANDRES/PROVID",
        "MUNICIPIO": "PROVIDENCIA",
        "OFICINALOCAL": "SAN ANDRES"
    },
    {
        "DEPARTAMENTO": "S.ANDRES/PROVID",
        "MUNICIPIO": "SAN ANDRES",
        "OFICINALOCAL": "SAN ANDRES"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "AGUADA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "ALBANIA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "ARATOCA",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "BARBOSA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "BARICHARA",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "BARRANCABERMEJA",
        "OFICINALOCAL": "BARRANCABERMEJA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "BETULIA",
        "OFICINALOCAL": "BARRANCABERMEJA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "BOLIVAR",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "BUCARAMANGA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CABRERA",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CALIFORNIA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CAPITANEJO",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CARCASI",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CEPITA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CERRITO",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CHARALA",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CHARTA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CHIMA",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CHIPATA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CIMITARRA",
        "OFICINALOCAL": "CIMITARRA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CONCEPCION",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CONFINES",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CONTRATACION",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "COROMORO",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "CURITI",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "EL CARMEN DE CHUCURI",
        "OFICINALOCAL": "SAN VICENTE DE CHUCURI"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "EL GUACAMAYO",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "EL PENON",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "EL PLAYON",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "ENCINO",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "ENCISO",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "FLORIAN",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "FLORIDABLANCA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GALAN",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GAMBITA",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GIRON",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GUACA",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GUADALUPE",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GUAPOTA",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GUAVATA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "GUEPSA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "HATO",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "JESUS MARIA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "JORDAN",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "LA BELLEZA",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "LANDAZURI",
        "OFICINALOCAL": "CIMITARRA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "LA PAZ",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "LEBRIJA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "LOS SANTOS",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "MACARAVITA",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "MALAGA",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "MATANZA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "MOGOTES",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "MOLAGAVITA",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "OCAMONTE",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "OIBA",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "ONZAGA",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PALMAR",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PALMAS DEL SOCORRO",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PARAMO",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PIEDECUESTA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PINCHOTE",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PUENTE NACIONAL",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PUERTO PARRA",
        "OFICINALOCAL": "CIMITARRA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "PUERTO WILCHES",
        "OFICINALOCAL": "BARRANCABERMEJA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "RIONEGRO",
        "OFICINALOCAL": "SABANA DE TORRES"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SABANA DE TORRES",
        "OFICINALOCAL": "SABANA DE TORRES"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SAN ANDRES",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SAN BENITO",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SAN GIL",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SAN JOAQUIN",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SAN JOSE DE MIRANDA",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SAN MIGUEL",
        "OFICINALOCAL": "MALAGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SANTA BARBARA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SANTA HELENA DEL OPON",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SAN VICENTE DE CHUCURI",
        "OFICINALOCAL": "SAN VICENTE DE CHUCURI"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SIMACOTA",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SOCORRO",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SUAITA",
        "OFICINALOCAL": "SOCORRO"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SUCRE",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "SURATA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "TONA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "VALLE SAN JOSE",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "VELEZ",
        "OFICINALOCAL": "VELEZ"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "VETAS",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "VILLANUEVA",
        "OFICINALOCAL": "SAN GIL"
    },
    {
        "DEPARTAMENTO": "SANTANDER",
        "MUNICIPIO": "ZAPATOCA",
        "OFICINALOCAL": "BUCARAMANGA"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "BUENAVISTA",
        "OFICINALOCAL": "SINCE"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "CAIMITO",
        "OFICINALOCAL": "SAN MARCOS"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "CHALAN",
        "OFICINALOCAL": "TOLUVIEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "COLOSO",
        "OFICINALOCAL": "TOLUVIEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "COROZAL",
        "OFICINALOCAL": "COROZAL"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "COVENAS",
        "OFICINALOCAL": "TOLUVIEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "EL ROBLE",
        "OFICINALOCAL": "SINCE"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "GALERAS",
        "OFICINALOCAL": "SINCE"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "GUARANDA",
        "OFICINALOCAL": "GUARANDA"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "LA UNION ",
        "OFICINALOCAL": "SAN MARCOS"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "LOS PALMITOS",
        "OFICINALOCAL": "COROZAL"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "MAJAGUAL",
        "OFICINALOCAL": "MAJAGUAL"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "MORROA",
        "OFICINALOCAL": "COROZAL"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "OVEJAS",
        "OFICINALOCAL": "SINCELEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "PALMITO",
        "OFICINALOCAL": "TOLUVIEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SAMPUES",
        "OFICINALOCAL": "SINCELEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SAN BENITO ABAD",
        "OFICINALOCAL": "SAN MARCOS"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SAN JUAN DE BETULIA",
        "OFICINALOCAL": "COROZAL"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SAN LUIS DE SINCE",
        "OFICINALOCAL": "SINCE"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SAN MARCOS",
        "OFICINALOCAL": "SAN MARCOS"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SAN ONOFRE",
        "OFICINALOCAL": "SAN ONOFRE"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SAN PEDRO",
        "OFICINALOCAL": "SINCELEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SANTIAGO DE TOLU",
        "OFICINALOCAL": "TOLUVIEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SINCELEJO",
        "OFICINALOCAL": "SINCELEJO"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "SUCRE",
        "OFICINALOCAL": "SUCRE Su"
    },
    {
        "DEPARTAMENTO": "SUCRE",
        "MUNICIPIO": "TOLUVIEJO",
        "OFICINALOCAL": "TOLUVIEJO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ALPUJARRA",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ALVARADO",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "AMBALEMA",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ANZOATEGUI",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ARMERO GUAYABAL",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ATACO",
        "OFICINALOCAL": "CHAPARRAL"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "CAJAMARCA",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "CARMEN DE APICALA",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "CASABIANCA",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "CHAPARRAL",
        "OFICINALOCAL": "CHAPARRAL"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "COELLO",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "COYAIMA",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "CUNDAY",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "DOLORES",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ESPINAL",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "FALAN",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "FLANDES",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "FRESNO",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "GUAMO",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "HERVEO",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "HONDA",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "IBAGUE",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ICONONZO",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "LERIDA",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "LIBANO",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "MELGAR",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "MURILLO",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "NATAGAIMA",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ORTEGA",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "PALOCABILDO",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "PIEDRAS",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "PLANADAS",
        "OFICINALOCAL": "CHAPARRAL"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "PRADO",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "PURIFICACION",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "RIOBLANCO",
        "OFICINALOCAL": "CHAPARRAL"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "RONCESVALLES",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "ROVIRA",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "SALDANA",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "SAN ANTONIO",
        "OFICINALOCAL": "CHAPARRAL"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "SAN LUIS",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "SAN SEBASTIAN DE MARIQUITA",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "SANTA ISABEL",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "SUAREZ",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "VALLE DE SAN JUAN",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "VENADILLO",
        "OFICINALOCAL": "IBAGUE"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "VILLAHERMOSA",
        "OFICINALOCAL": "MARIQUITA"
    },
    {
        "DEPARTAMENTO": "TOLIMA",
        "MUNICIPIO": "VILLARRICA",
        "OFICINALOCAL": "GUAMO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "ALCALA",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "ANDALUCIA",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "ANSERMANUEVO",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "ARGELIA",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "BOLIVAR",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "BUENAVENTURA",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "BUGALAGRANDE",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "CAICEDONIA",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "CALI",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "CALIMA",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "CANDELARIA",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "CARTAGO",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "DAGUA",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "EL AGUILA",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "EL CAIRO",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "EL CERRITO",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "EL DOVIO",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "FLORIDA",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "GINEBRA",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "GUACARI",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "GUADALAJARA DE BUGA",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "JAMUNDI",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "LA CUMBRE",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "LA UNION",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "LA VICTORIA",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "OBANDO",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "PALMIRA",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "PRADERA",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "RESTREPO",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "RIOFRIO",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "ROLDANILLO",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "SAN PEDRO",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "SEVILLA",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "TORO",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "TRUJILLO",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "TULUA",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "ULLOA",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "VERSALLES",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "VIJES",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "YOTOCO",
        "OFICINALOCAL": "TULUA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "YUMBO",
        "OFICINALOCAL": "PALMIRA"
    },
    {
        "DEPARTAMENTO": "VALLE",
        "MUNICIPIO": "ZARZAL",
        "OFICINALOCAL": "CARTAGO"
    },
    {
        "DEPARTAMENTO": "VAUPES",
        "MUNICIPIO": "CARURU",
        "OFICINALOCAL": "MITU"
    },
    {
        "DEPARTAMENTO": "VAUPES",
        "MUNICIPIO": "MITU",
        "OFICINALOCAL": "MITU"
    },
    {
        "DEPARTAMENTO": "VAUPES",
        "MUNICIPIO": "PACOA",
        "OFICINALOCAL": "MITU"
    },
    {
        "DEPARTAMENTO": "VAUPES",
        "MUNICIPIO": "PAPUNAHUA",
        "OFICINALOCAL": "MITU"
    },
    {
        "DEPARTAMENTO": "VAUPES",
        "MUNICIPIO": "TARAIRA",
        "OFICINALOCAL": "MITU"
    },
    {
        "DEPARTAMENTO": "VAUPES",
        "MUNICIPIO": "YAVARATE",
        "OFICINALOCAL": "MITU"
    },
    {
        "DEPARTAMENTO": "VICHADA",
        "MUNICIPIO": "CUMARIBO",
        "OFICINALOCAL": "PUERTO CARREÑO"
    },
    {
        "DEPARTAMENTO": "VICHADA",
        "MUNICIPIO": "LA PRIMAVERA",
        "OFICINALOCAL": "LA PRIMAVERA"
    },
    {
        "DEPARTAMENTO": "VICHADA",
        "MUNICIPIO": "PUERTO CARRENO",
        "OFICINALOCAL": "PUERTO CARREÑO"
    },
    {
        "DEPARTAMENTO": "VICHADA",
        "MUNICIPIO": "SANTA ROSALIA",
        "OFICINALOCAL": "SANTA ROSALIA"
    }
]