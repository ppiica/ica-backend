export const departmentsArray = [{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5a3"
	},
	"id": 68,
	"name": "SANTANDER",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f58f"
	},
	"id": 50,
	"name": "META",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5a8"
	},
	"id": 5,
	"name": "ANTIOQUIA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f594"
	},
	"id": 73,
	"name": "TOLIMA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f599"
	},
	"id": 52,
	"name": "NARIÑO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5b2"
	},
	"id": 54,
	"name": "NORTE DE SANTANDER",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f595"
	},
	"id": 86,
	"name": "PUTUMAYO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f59a"
	},
	"id": 94,
	"name": "GUAINIA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5ae"
	},
	"id": 17,
	"name": "CALDAS",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5b3"
	},
	"id": 81,
	"name": "ARAUCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fad9"
	},
	"id": 15,
	"name": "BOYACÁ",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f590"
	},
	"id": 41,
	"name": "HUILA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5a4"
	},
	"id": 66,
	"name": "RISARALDA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fae3"
	},
	"id": 25,
	"name": "CUNDINAMARCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8facf"
	},
	"id": 13,
	"name": "BOLÍVAR",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90021"
	},
	"id": 27,
	"name": "CHOCÓ",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb9001c"
	},
	"id": 23,
	"name": "CÓRDOBA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90535"
	},
	"id": 76,
	"name": "VALLE DEL CAUCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90562"
	},
	"id": 19,
	"name": "CAUCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90567"
	},
	"id": 70,
	"name": "SUCRE",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90576"
	},
	"id": 20,
	"name": "CESAR",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5b4"
	},
	"id": 11,
	"name": "BOGOTÁ, D.C.",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f596"
	},
	"id": 18,
	"name": "CAQUETÁ",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fad0"
	},
	"id": 63,
	"name": "QUINDIO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fae9"
	},
	"id": 47,
	"name": "MAGDALENA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fadf"
	},
	"id": 85,
	"name": "CASANARE",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90abb"
	},
	"id": 44,
	"name": "LA GUAJIRA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a96"
	},
	"id": 95,
	"name": "GUAVIARE",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb9000a"
	},
	"id": 99,
	"name": "VICHADA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91fbc"
	},
	"id": 97,
	"name": "VAUPES",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9153d"
	},
	"id": 8,
	"name": "ATLÁNTICO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb92f98"
	},
	"id": 91,
	"name": "AMAZONAS",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05066965613a0cb95e9c"
	},
	"id": 88,
	"name": "SAN ANDRES Y PROVIDENCIA",
	"__v": 0
}]

