export const municipalitiesArray = [{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb6b"
	},
	"id": 76892,
	"name": "YUMBO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb70"
	},
	"id": 76233,
	"name": "DAGUA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7a"
	},
	"id": 76616,
	"name": "RIOFRÍO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb84"
	},
	"id": 73671,
	"name": "SALDAÑA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb9d"
	},
	"id": 73268,
	"name": "ESPINAL",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb93"
	},
	"id": 73449,
	"name": "MELGAR",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb89"
	},
	"id": 73226,
	"name": "CUNDAY",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebac"
	},
	"id": 52838,
	"name": "TÚQUERRES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb75"
	},
	"id": 76126,
	"name": "CALIMA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba2"
	},
	"id": 52233,
	"name": "CUMBITARA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebb6"
	},
	"id": 73168,
	"name": "CHAPARRAL",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebbb"
	},
	"id": 50711,
	"name": "VISTAHERMOSA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb8e"
	},
	"id": 73217,
	"name": "COYAIMA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebc5"
	},
	"id": 52381,
	"name": "LA FLORIDA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebcf"
	},
	"id": 50350,
	"name": "LA MACARENA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba7"
	},
	"id": 52585,
	"name": "PUPIALES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebc0"
	},
	"id": 63302,
	"name": "GÉNOVA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebd4"
	},
	"id": 50577,
	"name": "PUERTO LLERAS",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebde"
	},
	"id": 50330,
	"name": "MESETAS",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebd9"
	},
	"id": 50318,
	"name": "GUAMAL",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe8"
	},
	"id": 50001,
	"name": "VILLAVICENCIO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebf2"
	},
	"id": 19142,
	"name": "CALOTO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebed"
	},
	"id": 19075,
	"name": "BALBOA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebf7"
	},
	"id": 19212,
	"name": "CORINTO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec01"
	},
	"id": 41206,
	"name": "COLOMBIA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec06"
	},
	"id": 41001,
	"name": "NEIVA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec0b"
	},
	"id": 19743,
	"name": "SILVIA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec10"
	},
	"id": 19532,
	"name": "PATÍA (EL BORDO)",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec1a"
	},
	"id": 19355,
	"name": "INZÁ",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec24"
	},
	"id": 41518,
	"name": "PAICOL",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec1f"
	},
	"id": 19001,
	"name": "POPAYÁN",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec29"
	},
	"id": 41357,
	"name": "ÍQUIRA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec33"
	},
	"id": 19824,
	"name": "TOTORÓ",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec3d"
	},
	"id": 41396,
	"name": "LA PLATA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec38"
	},
	"id": 41799,
	"name": "TELLO",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec42"
	},
	"id": 41676,
	"name": "SANTA MARÍA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec4c"
	},
	"id": 41668,
	"name": "SAN AGUSTÍN",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec56"
	},
	"id": 18094,
	"name": "BELÉN DE LOS ANDAQUÍES",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7f"
	},
	"id": 19473,
	"name": "MORALES",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec65"
	},
	"id": 73067,
	"name": "ATACO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb98"
	},
	"id": 73616,
	"name": "RIOBLANCO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec6f"
	},
	"id": 52411,
	"name": "LINARES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebb1"
	},
	"id": 52693,
	"name": "SAN PABLO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebca"
	},
	"id": 52254,
	"name": "EL PEÑOL",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec7e"
	},
	"id": 86568,
	"name": "PUERTO ASIS",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe3"
	},
	"id": 50006,
	"name": "ACACÍAS",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec88"
	},
	"id": 52540,
	"name": "POLICARPA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebfc"
	},
	"id": 19022,
	"name": "ALMAGUER",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec97"
	},
	"id": 18001,
	"name": "FLORENCIA",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca1"
	},
	"id": 18592,
	"name": "PUERTO RICO",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecb0"
	},
	"id": 86757,
	"name": "SAN MIGUEL",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec6a"
	},
	"id": 18150,
	"name": "CARTAGENA DEL CHAIRÁ",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecba"
	},
	"id": 52323,
	"name": "GUALMATAN",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec47"
	},
	"id": 41551,
	"name": "PITALITO",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec60"
	},
	"id": 41006,
	"name": "ACEVEDO",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc9"
	},
	"id": 19785,
	"name": "SUCRE",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec79"
	},
	"id": 76275,
	"name": "FLORIDA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec83"
	},
	"id": 86569,
	"name": "PUERTO CAICEDO",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd3"
	},
	"id": 52250,
	"name": "EL CHARCO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec92"
	},
	"id": 18753,
	"name": "SAN VICENTE DEL CAGUÁN",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece2"
	},
	"id": 52079,
	"name": "BARBACOAS",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecab"
	},
	"id": 86885,
	"name": "VILLAGARZON",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec9c"
	},
	"id": 18756,
	"name": "SOLANO",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecec"
	},
	"id": 68615,
	"name": "RIONEGRO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc4"
	},
	"id": 52612,
	"name": "RICAURTE",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec5b"
	},
	"id": 41801,
	"name": "TERUEL",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecfb"
	},
	"id": 94663,
	"name": "MAPIRIPANA",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecb5"
	},
	"id": 52687,
	"name": "SAN LORENZO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed14"
	},
	"id": 5107,
	"name": "BRICEÑO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca6"
	},
	"id": 86573,
	"name": "LEGUIZAMO",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecce"
	},
	"id": 50573,
	"name": "PUERTO LOPEZ",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed2d"
	},
	"id": 76041,
	"name": "ANSERMANUEVO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd8"
	},
	"id": 50313,
	"name": "GRANADA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece7"
	},
	"id": 52621,
	"name": "ROBERTO PAYAN",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed37"
	},
	"id": 54800,
	"name": "TEORAMA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed00"
	},
	"id": 99773,
	"name": "CUMARIBO",
	"departmentId": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed46"
	},
	"id": 73283,
	"name": "FRESNO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed0a"
	},
	"id": 76036,
	"name": "ANDALUCIA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed23"
	},
	"id": 5129,
	"name": "CALDAS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed50"
	},
	"id": 73686,
	"name": "SANTA ISABEL",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecdd"
	},
	"id": 76109,
	"name": "BUENAVENTURA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed5f"
	},
	"id": 5890,
	"name": "YOLOMBÓ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecf6"
	},
	"id": 50568,
	"name": "PUERTO GAITÁN",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed69"
	},
	"id": 73001,
	"name": "IBAGUÉ",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed78"
	},
	"id": 5895,
	"name": "ZARAGOZA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed28"
	},
	"id": 76845,
	"name": "ULLOA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed41"
	},
	"id": 5197,
	"name": "COCORNÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed32"
	},
	"id": 70215,
	"name": "COROZAL",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed91"
	},
	"id": 23001,
	"name": "MONTERÍA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed5a"
	},
	"id": 5887,
	"name": "YARUMAL",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed73"
	},
	"id": 20614,
	"name": "RIO DE ORO",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed9b"
	},
	"id": 68101,
	"name": "BOLÍVAR",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed4b"
	},
	"id": 5234,
	"name": "DABEIBA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8c"
	},
	"id": 54128,
	"name": "CÁCHIRA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edaa"
	},
	"id": 54206,
	"name": "CONVENCIÓN",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb4"
	},
	"id": 68397,
	"name": "LA PAZ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed64"
	},
	"id": 73200,
	"name": "COELLO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edbe"
	},
	"id": 68377,
	"name": "LA BELLEZA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc3"
	},
	"id": 68271,
	"name": "FLORIÁN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed3c"
	},
	"id": 73411,
	"name": "LÍBANO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edcd"
	},
	"id": 66456,
	"name": "MISTRATÓ",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed7d"
	},
	"id": 5893,
	"name": "YONDÓ (CASABE)",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed55"
	},
	"id": 5212,
	"name": "COPACABANA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed6e"
	},
	"id": 20400,
	"name": "LA JAGUA DE IBIRICO",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed96"
	},
	"id": 23182,
	"name": "CHINÚ",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed87"
	},
	"id": 23660,
	"name": "SAHAGÚN",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede6"
	},
	"id": 25386,
	"name": "LA MESA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edaf"
	},
	"id": 68464,
	"name": "MOGOTES",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc8"
	},
	"id": 66318,
	"name": "GUÁTICA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edff"
	},
	"id": 25772,
	"name": "SUESCA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb9"
	},
	"id": 68264,
	"name": "ENCINO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede1"
	},
	"id": 25402,
	"name": "LA VEGA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd2"
	},
	"id": 63548,
	"name": "PIJAO",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edfa"
	},
	"id": 25758,
	"name": "SOPÓ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edeb"
	},
	"id": 25878,
	"name": "VIOTÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee18"
	},
	"id": 25823,
	"name": "TOPAIPÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee13"
	},
	"id": 25862,
	"name": "VERGARA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee04"
	},
	"id": 25779,
	"name": "SUSA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1d"
	},
	"id": 25815,
	"name": "TOCAIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eddc"
	},
	"id": 68001,
	"name": "BUCARAMANGA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee31"
	},
	"id": 5686,
	"name": "SANTA ROSA DE OSOS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd7"
	},
	"id": 63212,
	"name": "CÓRDOBA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf5"
	},
	"id": 66594,
	"name": "QUINCHÍA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0e"
	},
	"id": 5628,
	"name": "SABANALARGA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee27"
	},
	"id": 5667,
	"name": "SAN RAFAEL",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee4a"
	},
	"id": 5790,
	"name": "TARAZÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee09"
	},
	"id": 25407,
	"name": "LENGUAZAQUE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee40"
	},
	"id": 5837,
	"name": "TURBO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee22"
	},
	"id": 5665,
	"name": "SAN PEDRO DE URABÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee59"
	},
	"id": 5361,
	"name": "ITUANGO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee63"
	},
	"id": 5854,
	"name": "VALDIVIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee72"
	},
	"id": 15842,
	"name": "UMBITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee54"
	},
	"id": 5490,
	"name": "NECOCLÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7c"
	},
	"id": 5411,
	"name": "LIBORINA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee6d"
	},
	"id": 5604,
	"name": "REMEDIOS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee86"
	},
	"id": 15808,
	"name": "TINJACÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee8b"
	},
	"id": 25148,
	"name": "CAPARRAPÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee36"
	},
	"id": 5652,
	"name": "SAN FRANCISCO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee95"
	},
	"id": 15837,
	"name": "TUTA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee4f"
	},
	"id": 5756,
	"name": "SONSÓN",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee68"
	},
	"id": 5250,
	"name": "EL BAGRE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee81"
	},
	"id": 5425,
	"name": "MACEO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeae"
	},
	"id": 25095,
	"name": "BITUIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee9a"
	},
	"id": 15764,
	"name": "SORACÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5e"
	},
	"id": 5847,
	"name": "URRAO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb3"
	},
	"id": 25299,
	"name": "GAMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec7"
	},
	"id": 5045,
	"name": "APARTADÓ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb8"
	},
	"id": 25297,
	"name": "GACHETÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee90"
	},
	"id": 68092,
	"name": "BETULIA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee0"
	},
	"id": 17495,
	"name": "NORCASIA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed1"
	},
	"id": 5038,
	"name": "ANGOSTURA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea9"
	},
	"id": 25040,
	"name": "ANOLAIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeea"
	},
	"id": 17513,
	"name": "PÁCORA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec2"
	},
	"id": 25372,
	"name": "JUNÍN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eef9"
	},
	"id": 5036,
	"name": "ANGELÓPOLIS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eecc"
	},
	"id": 5042,
	"name": "SANTA FE DE ANTIOQUIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee5"
	},
	"id": 17867,
	"name": "VICTORIA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eefe"
	},
	"id": 15455,
	"name": "MIRAFLORES",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef03"
	},
	"id": 15491,
	"name": "NOBSA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eef4"
	},
	"id": 17541,
	"name": "PENSILVANIA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef12"
	},
	"id": 15090,
	"name": "BERBEO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef17"
	},
	"id": 68689,
	"name": "SAN VICENTE DE CHUCURÍ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef0d"
	},
	"id": 17653,
	"name": "SALAMINA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef1c"
	},
	"id": 13654,
	"name": "SAN JACINTO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef26"
	},
	"id": 15325,
	"name": "GUAYATÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef2b"
	},
	"id": 17042,
	"name": "ANSERMA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef30"
	},
	"id": 23682,
	"name": "SAN JOSE DE URE",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eebd"
	},
	"id": 25260,
	"name": "EL ROSAL",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef35"
	},
	"id": 23855,
	"name": "VALENCIA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed6"
	},
	"id": 5034,
	"name": "ANDES",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef44"
	},
	"id": 47053,
	"name": "ARACATACA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef49"
	},
	"id": 13894,
	"name": "ZAMBRANO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4e"
	},
	"id": 13670,
	"name": "SAN PABLO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5d"
	},
	"id": 23580,
	"name": "PUERTO LIBERTADOR",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef62"
	},
	"id": 68673,
	"name": "SAN BENITO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef67"
	},
	"id": 27615,
	"name": "RIOSUCIO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef76"
	},
	"id": 27001,
	"name": "QUIBDO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef7b"
	},
	"id": 68872,
	"name": "VILLANUEVA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef80"
	},
	"id": 13688,
	"name": "SANTA ROSA DEL SUR",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef53"
	},
	"id": 68895,
	"name": "ZAPATOCA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef8f"
	},
	"id": 13430,
	"name": "MAGANGUE",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef94"
	},
	"id": 27425,
	"name": "MEDIO ATRATO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa8"
	},
	"id": 66572,
	"name": "PUEBLO RICO",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efad"
	},
	"id": 27077,
	"name": "BAJO BAUDO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef9e"
	},
	"id": 17662,
	"name": "SAMANA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc1"
	},
	"id": 23417,
	"name": "LORICA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efcb"
	},
	"id": 23815,
	"name": "TUCHÍN",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd0"
	},
	"id": 70771,
	"name": "SUCRE",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efda"
	},
	"id": 20383,
	"name": "LA GLORIA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc6"
	},
	"id": 70678,
	"name": "SAN BENITO ABAD",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe9"
	},
	"id": 85225,
	"name": "NUNCHÍA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef3f"
	},
	"id": 23466,
	"name": "MONTELÍBANO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe4"
	},
	"id": 15299,
	"name": "GARAGOA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff3"
	},
	"id": 85139,
	"name": "MANÍ",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff8"
	},
	"id": 85010,
	"name": "AGUAZUL",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f002"
	},
	"id": 23555,
	"name": "PLANETA RICA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef58"
	},
	"id": 47058,
	"name": "ARIGUANÍ",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f00c"
	},
	"id": 85162,
	"name": "MONTERREY",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f016"
	},
	"id": 85263,
	"name": "PORE",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f01b"
	},
	"id": 54680,
	"name": "SANTIAGO",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f011"
	},
	"id": 54743,
	"name": "SILOS",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef71"
	},
	"id": 13458,
	"name": "MONTECRISTO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f025"
	},
	"id": 68468,
	"name": "MOLAGAVITA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02a"
	},
	"id": 54820,
	"name": "TOLEDO",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f034"
	},
	"id": 81220,
	"name": "CRAVO NORTE",
	"departmentId": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02f"
	},
	"id": 54174,
	"name": "CHITAGÁ",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f03e"
	},
	"id": 81591,
	"name": "PUERTO RONDÓN",
	"departmentId": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef8a"
	},
	"id": 13212,
	"name": "CORDOBA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f048"
	},
	"id": 68160,
	"name": "CEPITÁ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f04d"
	},
	"id": 11001,
	"name": "BOGOTÁ, D.C.",
	"departmentId": 11,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f043"
	},
	"id": 81065,
	"name": "ARAUQUITA",
	"departmentId": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa3"
	},
	"id": 76403,
	"name": "LA VICTORIA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efbc"
	},
	"id": 27075,
	"name": "BAHIA SOLANO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd5"
	},
	"id": 13006,
	"name": "ACHI",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efee"
	},
	"id": 99624,
	"name": "SANTA ROSALÍA",
	"departmentId": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f007"
	},
	"id": 54673,
	"name": "SAN CAYETANO",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f020"
	},
	"id": 15673,
	"name": "SAN MATEO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb71"
	},
	"id": 76377,
	"name": "LA CUMBRE",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7b"
	},
	"id": 76001,
	"name": "CALI",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb85"
	},
	"id": 73675,
	"name": "SAN ANTONIO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb94"
	},
	"id": 73504,
	"name": "ORTEGA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb9e"
	},
	"id": 73555,
	"name": "PLANADAS",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba3"
	},
	"id": 52258,
	"name": "EL TABLÓN",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebbc"
	},
	"id": 52207,
	"name": "CONSACÁ",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebc6"
	},
	"id": 52354,
	"name": "IMUÉS",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb80"
	},
	"id": 19533,
	"name": "PIAMONTE",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebd0"
	},
	"id": 19050,
	"name": "ARGELIA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb76"
	},
	"id": 76122,
	"name": "CAICEDONIA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebdf"
	},
	"id": 25845,
	"name": "UNE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb99"
	},
	"id": 73624,
	"name": "ROVIRA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe9"
	},
	"id": 50150,
	"name": "CASTILLA LA NUEVA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebee"
	},
	"id": 19100,
	"name": "BOLÍVAR",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec07"
	},
	"id": 19698,
	"name": "SANTANDER DE QUILICHAO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec11"
	},
	"id": 19693,
	"name": "SAN SEBASTIÁN",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba8"
	},
	"id": 52694,
	"name": "SAN PEDRO DE CARTAGO (CARTAGO)",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec20"
	},
	"id": 19364,
	"name": "JAMBALÓ",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec2a"
	},
	"id": 41524,
	"name": "PALERMO",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec39"
	},
	"id": 41378,
	"name": "LA ARGENTINA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe4"
	},
	"id": 50270,
	"name": "EL DORADO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec5c"
	},
	"id": 41020,
	"name": "ALGECIRAS",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec16"
	},
	"id": 19517,
	"name": "PÁEZ (BELALCÁZAR)",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec0c"
	},
	"id": 19821,
	"name": "TORIBÍO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec52"
	},
	"id": 41770,
	"name": "SUAZA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec2f"
	},
	"id": 41078,
	"name": "BARAYA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec6b"
	},
	"id": 25335,
	"name": "GUAYABETAL",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec48"
	},
	"id": 41503,
	"name": "OPORAPA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec84"
	},
	"id": 86571,
	"name": "PUERTO GUZMAN",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec3e"
	},
	"id": 41660,
	"name": "SALADOBLANCO",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec57"
	},
	"id": 41306,
	"name": "GIGANTE",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec7a"
	},
	"id": 18410,
	"name": "LA MONTAÑITA",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec93"
	},
	"id": 86001,
	"name": "MOCOA",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca7"
	},
	"id": 86760,
	"name": "SANTIAGO",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc0"
	},
	"id": 18460,
	"name": "MILAN",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc5"
	},
	"id": 18479,
	"name": "ALBANIA",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec1b"
	},
	"id": 19397,
	"name": "LA VEGA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecbb"
	},
	"id": 52685,
	"name": "SAN BERNARDO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eccf"
	},
	"id": 50325,
	"name": "MAPIRIPÁN",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eced"
	},
	"id": 85300,
	"name": "SABANALARGA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece8"
	},
	"id": 52390,
	"name": "LA TOLA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed01"
	},
	"id": 68655,
	"name": "SABANA DE TORRES",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed06"
	},
	"id": 19318,
	"name": "GUAPI",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed1a"
	},
	"id": 54398,
	"name": "LA PLAYA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed1f"
	},
	"id": 68780,
	"name": "SURATÁ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd9"
	},
	"id": 41791,
	"name": "TARQUI",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed33"
	},
	"id": 70233,
	"name": "EL ROBLE",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecb1"
	},
	"id": 86755,
	"name": "SAN FRANCISCO",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed0b"
	},
	"id": 70204,
	"name": "COLOSÓ",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed4c"
	},
	"id": 13873,
	"name": "VILLANUEVA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece3"
	},
	"id": 73585,
	"name": "PURIFICACIÓN",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecfc"
	},
	"id": 94001,
	"name": "INIRIDA",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed65"
	},
	"id": 73043,
	"name": "ANZOÁTEGUI",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed15"
	},
	"id": 68572,
	"name": "PUENTE NACIONAL",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed56"
	},
	"id": 13042,
	"name": "ARENAL",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed51"
	},
	"id": 73124,
	"name": "CAJAMARCA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed2e"
	},
	"id": 70717,
	"name": "SAN PEDRO",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed6f"
	},
	"id": 13780,
	"name": "TALAIGUA NUEVO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed47"
	},
	"id": 73443,
	"name": "MARIQUITA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed7e"
	},
	"id": 13468,
	"name": "MOMPÓS",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed88"
	},
	"id": 23068,
	"name": "AYAPEL",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecde"
	},
	"id": 76736,
	"name": "SEVILLA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed97"
	},
	"id": 20710,
	"name": "SAN ALBERTO",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed79"
	},
	"id": 13744,
	"name": "SIMITÍ",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda1"
	},
	"id": 54344,
	"name": "HACARÍ",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed83"
	},
	"id": 20011,
	"name": "AGUACHICA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edba"
	},
	"id": 68235,
	"name": "EL CARMEN DE CHUCURÍ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc9"
	},
	"id": 66170,
	"name": "DOSQUEBRADAS",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd3"
	},
	"id": 63272,
	"name": "FILANDIA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eddd"
	},
	"id": 63594,
	"name": "QUIMBAYA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede2"
	},
	"id": 25486,
	"name": "NEMOCÓN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed29"
	},
	"id": 76246,
	"name": "EL CAIRO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb5"
	},
	"id": 68121,
	"name": "CABRERA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed42"
	},
	"id": 5209,
	"name": "CONCORDIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edfb"
	},
	"id": 25489,
	"name": "NIMAIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf6"
	},
	"id": 25885,
	"name": "YACOPÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edce"
	},
	"id": 66075,
	"name": "BALBOA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee05"
	},
	"id": 25596,
	"name": "QUIPILE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed74"
	},
	"id": 20770,
	"name": "SAN MARTÍN",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0f"
	},
	"id": 25851,
	"name": "ÚTICA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1e"
	},
	"id": 25839,
	"name": "UBALÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede7"
	},
	"id": 25426,
	"name": "MACHETA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda6"
	},
	"id": 54385,
	"name": "LA ESPERANZA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee2d"
	},
	"id": 5679,
	"name": "SANTA BÁRBARA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee37"
	},
	"id": 5483,
	"name": "NARIÑO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee00"
	},
	"id": 25736,
	"name": "SESQUILÉ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee46"
	},
	"id": 5789,
	"name": "TÁMESIS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee50"
	},
	"id": 5792,
	"name": "TARSO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd8"
	},
	"id": 63130,
	"name": "CALARCÁ",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee41"
	},
	"id": 5873,
	"name": "VIGIA DEL FUERTE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf1"
	},
	"id": 25394,
	"name": "LA PALMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5f"
	},
	"id": 5360,
	"name": "ITAGÜÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0a"
	},
	"id": 25599,
	"name": "APULO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee69"
	},
	"id": 5579,
	"name": "PUERTO BERRÍO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee23"
	},
	"id": 5761,
	"name": "SOPETRÁN",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5a"
	},
	"id": 5313,
	"name": "GRANADA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee78"
	},
	"id": 5440,
	"name": "MARINILLA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee32"
	},
	"id": 5660,
	"name": "SAN LUIS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee82"
	},
	"id": 15667,
	"name": "SAN LUIS DE GACENO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee73"
	},
	"id": 68020,
	"name": "ALBANIA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee91"
	},
	"id": 15835,
	"name": "TURMEQUÉ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee55"
	},
	"id": 5376,
	"name": "LA CEJA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee4b"
	},
	"id": 5690,
	"name": "SANTO DOMINGO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee9b"
	},
	"id": 15681,
	"name": "SAN PABLO DE BORBUR",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeaa"
	},
	"id": 25123,
	"name": "CACHIPAY",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee87"
	},
	"id": 15621,
	"name": "RONDÓN",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb4"
	},
	"id": 25317,
	"name": "GUACHETÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea0"
	},
	"id": 25154,
	"name": "CARMEN DE CARUPA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec3"
	},
	"id": 25286,
	"name": "FUNZA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea5"
	},
	"id": 25178,
	"name": "CHIPAQUE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb9"
	},
	"id": 25322,
	"name": "GUASCA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eecd"
	},
	"id": 5093,
	"name": "BETULIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7d"
	},
	"id": 5467,
	"name": "MONTEBELLO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed2"
	},
	"id": 5040,
	"name": "ANORÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eedc"
	},
	"id": 5031,
	"name": "AMALFI",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eebe"
	},
	"id": 15572,
	"name": "PUERTO BOYACÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee6"
	},
	"id": 17873,
	"name": "VILLAMARIA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed7"
	},
	"id": 5001,
	"name": "MEDELLÍN",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee96"
	},
	"id": 5240,
	"name": "EBÉJICO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeeb"
	},
	"id": 17272,
	"name": "FILADELFIA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeff"
	},
	"id": 15204,
	"name": "CÓMBITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef09"
	},
	"id": 15511,
	"name": "PACHAVITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef0e"
	},
	"id": 15293,
	"name": "GACHANTIVÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeaf"
	},
	"id": 25175,
	"name": "CHÍA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef27"
	},
	"id": 15322,
	"name": "GUATEQUE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef31"
	},
	"id": 23807,
	"name": "TIERRALTA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef40"
	},
	"id": 15542,
	"name": "PESCA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4f"
	},
	"id": 47745,
	"name": "SITIONUEVO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef59"
	},
	"id": 47555,
	"name": "PLATO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee1"
	},
	"id": 17001,
	"name": "MANIZALES",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef63"
	},
	"id": 27600,
	"name": "RIO QUITO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef72"
	},
	"id": 27430,
	"name": "MEDIO BAUDO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef7c"
	},
	"id": 13490,
	"name": "RIO VIEJO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eefa"
	},
	"id": 5086,
	"name": "BELMIRA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef95"
	},
	"id": 13268,
	"name": "EL PENON",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef86"
	},
	"id": 27099,
	"name": "BOJAYA",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef13"
	},
	"id": 17088,
	"name": "BELALCAZAR",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef81"
	},
	"id": 13549,
	"name": "PINILLOS",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa4"
	},
	"id": 27025,
	"name": "ALTO BAUDO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efbd"
	},
	"id": 13160,
	"name": "CANTAGALLO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef2c"
	},
	"id": 13244,
	"name": "EL CARMEN DE BOLÍVAR",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef45"
	},
	"id": 8372,
	"name": "JUAN DE ACOSTA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd6"
	},
	"id": 76400,
	"name": "LA UNIÓN",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5e"
	},
	"id": 68324,
	"name": "GUAVATÁ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efcc"
	},
	"id": 66400,
	"name": "LA VIRGINIA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe5"
	},
	"id": 15632,
	"name": "SABOYÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef90"
	},
	"id": 27372,
	"name": "JURADO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8effe"
	},
	"id": 23570,
	"name": "PUEBLO NUEVO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f008"
	},
	"id": 54874,
	"name": "VILLA DEL ROSARIO",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa9"
	},
	"id": 73870,
	"name": "VILLAHERMOSA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f017"
	},
	"id": 54660,
	"name": "SALAZAR",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f021"
	},
	"id": 15755,
	"name": "SOCOTÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efdb"
	},
	"id": 20250,
	"name": "EL PASO",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f030"
	},
	"id": 54223,
	"name": "CUCUTILLA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f03a"
	},
	"id": 54480,
	"name": "MUTISCUA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff4"
	},
	"id": 85230,
	"name": "OROCUÉ",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f049"
	},
	"id": 15047,
	"name": "AQUITANIA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f057"
	},
	"id": 25594,
	"name": "QUETAME",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f026"
	},
	"id": 15774,
	"name": "SUSACÓN",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f066"
	},
	"id": 85400,
	"name": "TAMARA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f070"
	},
	"id": 19760,
	"name": "SOTARA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f05c"
	},
	"id": 44847,
	"name": "URIBIA",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f075"
	},
	"id": 44855,
	"name": "URUMITA",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc7"
	},
	"id": 68406,
	"name": "LEBRIJA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efea"
	},
	"id": 85250,
	"name": "PAZ DE ARIPORO",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f003"
	},
	"id": 68669,
	"name": "SAN ANDRÉS",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f01c"
	},
	"id": 68147,
	"name": "CAPITANEJO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f035"
	},
	"id": 54313,
	"name": "GRAMALOTE",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f052"
	},
	"id": 85325,
	"name": "SAN LUIS DE PALENQUE",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06b"
	},
	"id": 5380,
	"name": "LA ESTRELLA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02b"
	},
	"id": 54261,
	"name": "EL ZULIA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f061"
	},
	"id": 68152,
	"name": "CARCASÍ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f07a"
	},
	"id": 50110,
	"name": "BARRANCA DE UPÍA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f089"
	},
	"id": 19137,
	"name": "CALDONO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f08e"
	},
	"id": 19450,
	"name": "MERCADERES",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0a7"
	},
	"id": 73770,
	"name": "SUÁREZ",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0bb"
	},
	"id": 73236,
	"name": "DOLORES",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0cf"
	},
	"id": 52051,
	"name": "ARBOLEDA (BERRUECOS)",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d4"
	},
	"id": 52260,
	"name": "EL TAMBO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0ca"
	},
	"id": 52203,
	"name": "COLÓN (GÉNOVA)",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f11f"
	},
	"id": 19809,
	"name": "TIMBIQUÍ",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f133"
	},
	"id": 41298,
	"name": "GARZÓN",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f12e"
	},
	"id": 19290,
	"name": "FLORENCIA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f138"
	},
	"id": 41349,
	"name": "HOBO",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f147"
	},
	"id": 41132,
	"name": "CAMPOALEGRE",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0c5"
	},
	"id": 73275,
	"name": "FLANDES",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0f2"
	},
	"id": 25649,
	"name": "SAN BERNARDO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f10b"
	},
	"id": 41359,
	"name": "ISNOS",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f197"
	},
	"id": 86320,
	"name": "ORITO",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f110"
	},
	"id": 50606,
	"name": "RESTREPO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f13d"
	},
	"id": 41319,
	"name": "GUADALUPE",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f129"
	},
	"id": 19256,
	"name": "EL TAMBO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f183"
	},
	"id": 18247,
	"name": "EL DONCELLO",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f174"
	},
	"id": 18205,
	"name": "CURILLO",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1a1"
	},
	"id": 19622,
	"name": "ROSAS",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1ce"
	},
	"id": 52399,
	"name": "LA UNION",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f200"
	},
	"id": 68547,
	"name": "PIEDECUESTA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1c9"
	},
	"id": 52356,
	"name": "IPIALES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1d8"
	},
	"id": 18256,
	"name": "EL PAUJIL",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1f1"
	},
	"id": 52696,
	"name": "NARIÑO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f232"
	},
	"id": 5154,
	"name": "CAUCASIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1fb"
	},
	"id": 95025,
	"name": "EL RETORNO",
	"departmentId": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f20a"
	},
	"id": 94343,
	"name": "BARRANCO MINA",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f223"
	},
	"id": 70670,
	"name": "SAMPUÉS",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f24b"
	},
	"id": 73861,
	"name": "VENADILLO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1d3"
	},
	"id": 52022,
	"name": "ALDANA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f264"
	},
	"id": 5172,
	"name": "CHIGORODÓ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f26e"
	},
	"id": 13433,
	"name": "MAHATES",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f246"
	},
	"id": 70230,
	"name": "CHALÁN",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f287"
	},
	"id": 20228,
	"name": "CURUMANÍ",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2af"
	},
	"id": 68500,
	"name": "OIBA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f291"
	},
	"id": 20310,
	"name": "GONZÁLEZ",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2aa"
	},
	"id": 68211,
	"name": "CONTRATACIÓN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f205"
	},
	"id": 68533,
	"name": "PÁRAMO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2c3"
	},
	"id": 68498,
	"name": "OCAMONTE",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f20f"
	},
	"id": 95001,
	"name": "SAN JOSÉ DEL GUAVIARE",
	"departmentId": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f282"
	},
	"id": 47960,
	"name": "ZAPAYÁN",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f29b"
	},
	"id": 54003,
	"name": "ÁBREGO",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f228"
	},
	"id": 68573,
	"name": "PUERTO PARRA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f241"
	},
	"id": 5125,
	"name": "CAICEDO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2a5"
	},
	"id": 20443,
	"name": "MANAURE BALCÓN DEL CESAR",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2cd"
	},
	"id": 68245,
	"name": "EL GUACAMAYO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f318"
	},
	"id": 25398,
	"name": "LA PEÑA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f331"
	},
	"id": 25645,
	"name": "SAN ANTONIO DEL TEQUENDAMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f34a"
	},
	"id": 5858,
	"name": "VEGACHÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2eb"
	},
	"id": 68418,
	"name": "LOS SANTOS",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f37c"
	},
	"id": 5543,
	"name": "PEQUE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f304"
	},
	"id": 25873,
	"name": "VILLAPINZÓN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f395"
	},
	"id": 15686,
	"name": "SANTANA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3ae"
	},
	"id": 15693,
	"name": "SANTA ROSA DE VITERBO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3f9"
	},
	"id": 17444,
	"name": "MARQUETALIA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f412"
	},
	"id": 15135,
	"name": "CAMPOHERMOSO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f368"
	},
	"id": 5318,
	"name": "GUARNE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f444"
	},
	"id": 68385,
	"name": "LANDÁZURI",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f45d"
	},
	"id": 47675,
	"name": "SALAMINA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f39a"
	},
	"id": 15806,
	"name": "TIBASOSA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3b3"
	},
	"id": 25258,
	"name": "EL PEÑÓN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3cc"
	},
	"id": 25326,
	"name": "GUATAVITA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4da"
	},
	"id": 70708,
	"name": "SAN MARCOS",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3e5"
	},
	"id": 5051,
	"name": "ARBOLETES",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4f3"
	},
	"id": 20032,
	"name": "ASTREA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3fe"
	},
	"id": 5079,
	"name": "BARBOSA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f50c"
	},
	"id": 23419,
	"name": "LOS CORDOBAS",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f417"
	},
	"id": 15172,
	"name": "CHINAVITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f53e"
	},
	"id": 54377,
	"name": "LABATECA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f313"
	},
	"id": 25518,
	"name": "PAIME",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f32c"
	},
	"id": 25841,
	"name": "UBAQUE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f430"
	},
	"id": 13001,
	"name": "CARTAGENA DE INDIAS",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f557"
	},
	"id": 54099,
	"name": "BOCHALEMA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f35e"
	},
	"id": 5697,
	"name": "SANTUARIO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f377"
	},
	"id": 5541,
	"name": "PEÑOL",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f58a"
	},
	"id": 19418,
	"name": "LÓPEZ DE MICAY",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5bc"
	},
	"id": 76318,
	"name": "GUACARÍ",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f462"
	},
	"id": 8549,
	"name": "PIOJO",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4ad"
	},
	"id": 17442,
	"name": "MARMATO",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4f8"
	},
	"id": 85001,
	"name": "YOPAL",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f511"
	},
	"id": 23090,
	"name": "CANALETE",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f52a"
	},
	"id": 54599,
	"name": "RAGONVALIA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f543"
	},
	"id": 81794,
	"name": "TAME",
	"departmentId": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f576"
	},
	"id": 15790,
	"name": "TASCO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5c1"
	},
	"id": 76563,
	"name": "PRADERA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5da"
	},
	"id": 73622,
	"name": "RONCESVALLES",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5f3"
	},
	"id": 52405,
	"name": "LEIVA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f60c"
	},
	"id": 52215,
	"name": "CÓRDOBA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6a2"
	},
	"id": 41872,
	"name": "VILLAVIEJA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f706"
	},
	"id": 52378,
	"name": "LA CRUZ",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f751"
	},
	"id": 94887,
	"name": "PANÁ PANÁ",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f620"
	},
	"id": 50400,
	"name": "LEJANÍAS",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f639"
	},
	"id": 19110,
	"name": "BUENOS AIRES",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f652"
	},
	"id": 19701,
	"name": "SANTA ROSA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6e8"
	},
	"id": 41483,
	"name": "NÁTAGA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f765"
	},
	"id": 54720,
	"name": "SARDINATA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3c2"
	},
	"id": 25368,
	"name": "JERUSALÉN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3f4"
	},
	"id": 17433,
	"name": "MANZANARES",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f48a"
	},
	"id": 13657,
	"name": "SAN JUAN NEPOMUCENO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4a3"
	},
	"id": 27450,
	"name": "MEDIO SAN JUAN",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4bc"
	},
	"id": 66687,
	"name": "SANTUARIO",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4d5"
	},
	"id": 70473,
	"name": "MORROA",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4ee"
	},
	"id": 17614,
	"name": "RIOSUCIO",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f552"
	},
	"id": 81001,
	"name": "ARAUCA",
	"departmentId": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5e9"
	},
	"id": 52788,
	"name": "TANGUA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f602"
	},
	"id": 50223,
	"name": "SAN LUIS DE CUBARRAL",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f67f"
	},
	"id": 41016,
	"name": "AIPE",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6fc"
	},
	"id": 52036,
	"name": "ANCUYA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f747"
	},
	"id": 76520,
	"name": "PALMIRA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f309"
	},
	"id": 25740,
	"name": "SIBATÉ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f322"
	},
	"id": 5649,
	"name": "SAN CARLOS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f33b"
	},
	"id": 5674,
	"name": "SAN VICENTE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f354"
	},
	"id": 5736,
	"name": "SEGOVIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3b8"
	},
	"id": 25035,
	"name": "ANAPOIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3d1"
	},
	"id": 25245,
	"name": "EL COLEGIO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f403"
	},
	"id": 5055,
	"name": "ARGELIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f41c"
	},
	"id": 15480,
	"name": "MUZO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f44e"
	},
	"id": 54250,
	"name": "EL TARRA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f480"
	},
	"id": 13650,
	"name": "SAN FERNANDO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f499"
	},
	"id": 13300,
	"name": "HATILLO DE LOBA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f516"
	},
	"id": 54518,
	"name": "PAMPLONA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f52f"
	},
	"id": 15550,
	"name": "PISBA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5c6"
	},
	"id": 76834,
	"name": "TULUÁ",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2dc"
	},
	"id": 66088,
	"name": "BELÉN DE UMBRÍA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2f5"
	},
	"id": 25438,
	"name": "MEDINA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30e"
	},
	"id": 25718,
	"name": "SASAIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f643"
	},
	"id": 41548,
	"name": "PITAL",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f68e"
	},
	"id": 41530,
	"name": "PALESTINA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f372"
	},
	"id": 5842,
	"name": "URAMITA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6a7"
	},
	"id": 41807,
	"name": "TIMANÁ",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3bd"
	},
	"id": 25181,
	"name": "CHOACHÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3d6"
	},
	"id": 25214,
	"name": "COTA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f421"
	},
	"id": 17777,
	"name": "SUPIA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f43a"
	},
	"id": 15109,
	"name": "BUENAVISTA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f46c"
	},
	"id": 68745,
	"name": "SIMACOTA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f485"
	},
	"id": 27495,
	"name": "NUQUI",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4b7"
	},
	"id": 76895,
	"name": "ZARZAL",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4d0"
	},
	"id": 15367,
	"name": "JENESANO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4e9"
	},
	"id": 20178,
	"name": "CHIRIGUANA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f502"
	},
	"id": 54405,
	"name": "LOS PATIOS",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f51b"
	},
	"id": 85125,
	"name": "HATO COROZAL",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f534"
	},
	"id": 54051,
	"name": "ARBOLEDAS",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f54d"
	},
	"id": 15218,
	"name": "COVARACHÍA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f567"
	},
	"id": 50124,
	"name": "CABUYARO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5fd"
	},
	"id": 73483,
	"name": "NATAGAIMA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f729"
	},
	"id": 52490,
	"name": "OLAYA HERRERA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f742"
	},
	"id": 68773,
	"name": "SUCRE",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f75b"
	},
	"id": 70001,
	"name": "SINCELEJO",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb6d"
	},
	"id": 76890,
	"name": "YOTOCO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7c"
	},
	"id": 19455,
	"name": "MIRANDA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb90"
	},
	"id": 73319,
	"name": "GUAMO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba9"
	},
	"id": 52835,
	"name": "TUMACO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebcc"
	},
	"id": 52317,
	"name": "GUACHUCAL",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebae"
	},
	"id": 52678,
	"name": "SAMANIEGO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe5"
	},
	"id": 50251,
	"name": "EL CASTILLO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec7b"
	},
	"id": 18610,
	"name": "SAN JOSÉ DEL FRAGUA",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec08"
	},
	"id": 19585,
	"name": "AREA EN LITIGIO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc6"
	},
	"id": 52110,
	"name": "BUESACO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec5d"
	},
	"id": 41885,
	"name": "YAGUARÁ",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed11"
	},
	"id": 73408,
	"name": "LÉRIDA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca8"
	},
	"id": 86865,
	"name": "VALLE DEL GUAMUEZ",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc1"
	},
	"id": 52435,
	"name": "MALLAMA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed43"
	},
	"id": 5480,
	"name": "MUTATÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed5c"
	},
	"id": 5885,
	"name": "YALÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed75"
	},
	"id": 23574,
	"name": "PUERTO ESCONDIDO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8e"
	},
	"id": 47318,
	"name": "GUAMAL",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda7"
	},
	"id": 54245,
	"name": "EL CARMEN",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed34"
	},
	"id": 76622,
	"name": "ROLDANILLO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed4d"
	},
	"id": 5206,
	"name": "CONCEPCIÓN",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc0"
	},
	"id": 68344,
	"name": "HATO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda2"
	},
	"id": 68229,
	"name": "CURITÍ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd4"
	},
	"id": 66682,
	"name": "SANTA ROSA DE CABAL",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0b"
	},
	"id": 25535,
	"name": "PASCA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee06"
	},
	"id": 25530,
	"name": "PARATEBUENO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee24"
	},
	"id": 5670,
	"name": "SAN ROQUE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1f"
	},
	"id": 25793,
	"name": "TAUSA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edfc"
	},
	"id": 25488,
	"name": "NILO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee15"
	},
	"id": 25473,
	"name": "MOSQUERA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd5"
	},
	"id": 50683,
	"name": "SAN JUAN DE ARAMA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee51"
	},
	"id": 5308,
	"name": "GIRARDOTA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee6a"
	},
	"id": 27787,
	"name": "TADÓ",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee83"
	},
	"id": 15763,
	"name": "SOTAQUIRÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee9c"
	},
	"id": 15832,
	"name": "TUNUNGUÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed52"
	},
	"id": 5145,
	"name": "CARAMANTA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb5"
	},
	"id": 5030,
	"name": "AMAGÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeba"
	},
	"id": 5021,
	"name": "ALEJANDRÍA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed9d"
	},
	"id": 68298,
	"name": "GAMBITA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb6"
	},
	"id": 66440,
	"name": "MARSELLA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee7"
	},
	"id": 17174,
	"name": "CHINCHINÁ",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeec"
	},
	"id": 17013,
	"name": "AGUADAS",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edcf"
	},
	"id": 66383,
	"name": "LA CELIA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede8"
	},
	"id": 25436,
	"name": "MANTA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef00"
	},
	"id": 15514,
	"name": "PÁEZ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef05"
	},
	"id": 15469,
	"name": "MONIQUIRÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef19"
	},
	"id": 5120,
	"name": "CÁCERES",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee33"
	},
	"id": 5664,
	"name": "SAN PEDRO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee92"
	},
	"id": 15664,
	"name": "SAN JOSÉ DE PARE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeab"
	},
	"id": 25183,
	"name": "CHOCONTÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7e"
	},
	"id": 5368,
	"name": "JERICÓ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef0f"
	},
	"id": 15442,
	"name": "MARIPÍ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efb4"
	},
	"id": 13052,
	"name": "ARJONA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4b"
	},
	"id": 13655,
	"name": "SAN JACINTO DEL CAUCA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef14"
	},
	"id": 15272,
	"name": "FIRAVITOBA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef41"
	},
	"id": 27160,
	"name": "CÉRTEGUI",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eccb"
	},
	"id": 18785,
	"name": "SOLITA",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efff"
	},
	"id": 70429,
	"name": "MAJAGUAL",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecfd"
	},
	"id": 50450,
	"name": "PUERTO CONCORDIA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5f"
	},
	"id": 47692,
	"name": "SAN SEBASTIÁN DE BUENAVISTA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed48"
	},
	"id": 13836,
	"name": "TURBACO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f04a"
	},
	"id": 15097,
	"name": "BOAVITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efdc"
	},
	"id": 15531,
	"name": "PAUNA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edac"
	},
	"id": 47268,
	"name": "EL RETÉN",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff5"
	},
	"id": 68820,
	"name": "TONA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef96"
	},
	"id": 13222,
	"name": "CLEMENCIA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f027"
	},
	"id": 15753,
	"name": "SOATÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc8"
	},
	"id": 15212,
	"name": "COPER",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f040"
	},
	"id": 81300,
	"name": "FORTUL",
	"departmentId": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe1"
	},
	"id": 68307,
	"name": "GIRON",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee74"
	},
	"id": 68077,
	"name": "BARBOSA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee8d"
	},
	"id": 15814,
	"name": "TOCA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f05d"
	},
	"id": 15368,
	"name": "JERICÓ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea6"
	},
	"id": 25279,
	"name": "FOMEQUE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5a"
	},
	"id": 8421,
	"name": "LURUACO",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed8"
	},
	"id": 5004,
	"name": "ABRIAQUÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eef1"
	},
	"id": 17380,
	"name": "LA DORADA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef23"
	},
	"id": 70713,
	"name": "SAN ONOFRE",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff0"
	},
	"id": 85410,
	"name": "TAURAMENA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f03b"
	},
	"id": 81736,
	"name": "SARAVENA",
	"departmentId": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f053"
	},
	"id": 50590,
	"name": "PUERTO RICO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd2"
	},
	"id": 76250,
	"name": "EL DOVIO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06c"
	},
	"id": 68867,
	"name": "VETAS",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efeb"
	},
	"id": 85430,
	"name": "TRINIDAD",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f036"
	},
	"id": 68684,
	"name": "SAN JOSÉ DE MIRANDA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f094"
	},
	"id": 73873,
	"name": "VILLARRICA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d5"
	},
	"id": 52352,
	"name": "ILES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f080"
	},
	"id": 76606,
	"name": "RESTREPO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f09e"
	},
	"id": 73352,
	"name": "ICONONZO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d0"
	},
	"id": 52224,
	"name": "CUASPUD (CARLOSAMA)",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f102"
	},
	"id": 19130,
	"name": "CAJIBÍO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f14d"
	},
	"id": 41615,
	"name": "RIVERA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0cb"
	},
	"id": 52019,
	"name": "ALBÁN (SAN JOSÉ)",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f201"
	},
	"id": 68524,
	"name": "PALMAS DEL SOCORRO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1ca"
	},
	"id": 52573,
	"name": "PUERRES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f21a"
	},
	"id": 76113,
	"name": "BUGA LA GRANDE",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f12f"
	},
	"id": 19573,
	"name": "PUERTO TEJADA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f24c"
	},
	"id": 20570,
	"name": "PUEBLO BELLO",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2a1"
	},
	"id": 20517,
	"name": "PAILITAS",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f297"
	},
	"id": 47551,
	"name": "PIVIJAY",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f242"
	},
	"id": 54871,
	"name": "VILLA CARO",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f189"
	},
	"id": 25120,
	"name": "CABRERA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2c9"
	},
	"id": 68250,
	"name": "EL PEÑÓN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f274"
	},
	"id": 73520,
	"name": "PALOCABILDO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2c4"
	},
	"id": 68276,
	"name": "FLORIDABLANCA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f238"
	},
	"id": 5190,
	"name": "CISNEROS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f26a"
	},
	"id": 73026,
	"name": "ALVARADO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f283"
	},
	"id": 20750,
	"name": "SAN DIEGO",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2dd"
	},
	"id": 66045,
	"name": "APÍA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30a"
	},
	"id": 25769,
	"name": "SUBACHOQUE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30f"
	},
	"id": 25777,
	"name": "SUPATÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f341"
	},
	"id": 5656,
	"name": "SAN JERÓNIMO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f373"
	},
	"id": 5856,
	"name": "VALPARAISO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3a5"
	},
	"id": 27580,
	"name": "RIO IRÓ (SANTA RITA)",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3b9"
	},
	"id": 15861,
	"name": "VENTAQUEMADA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f436"
	},
	"id": 15516,
	"name": "PAIPA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f454"
	},
	"id": 47189,
	"name": "CIÉNAGA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f468"
	},
	"id": 47460,
	"name": "NUEVA GRANADA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4e5"
	},
	"id": 27006,
	"name": "ACANDI",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f517"
	},
	"id": 54810,
	"name": "TIBÚ",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f51c"
	},
	"id": 54001,
	"name": "CÚCUTA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f563"
	},
	"id": 15317,
	"name": "GUACAMAYAS",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6c1"
	},
	"id": 18860,
	"name": "VALPARAÍSO",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f773"
	},
	"id": 5147,
	"name": "CAREPA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f854"
	},
	"id": 25572,
	"name": "PUERTO SALGAR",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8b8"
	},
	"id": 68013,
	"name": "AGUADA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f73e"
	},
	"id": 68522,
	"name": "PALMAR",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7d2"
	},
	"id": 47030,
	"name": "ALGARROBO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f89a"
	},
	"id": 5315,
	"name": "GUADALUPE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f949"
	},
	"id": 15362,
	"name": "IZA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f994"
	},
	"id": 47170,
	"name": "CHIVOLO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa5c"
	},
	"id": 54418,
	"name": "LOURDES",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa75"
	},
	"id": 68266,
	"name": "ENCISO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8faa7"
	},
	"id": 15820,
	"name": "TOPAGA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8d1"
	},
	"id": 15696,
	"name": "SANTA SOFÍA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f903"
	},
	"id": 25295,
	"name": "GACHANCIPÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f935"
	},
	"id": 17446,
	"name": "MARULANDA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f94e"
	},
	"id": 15226,
	"name": "CUÍTIVA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f967"
	},
	"id": 70508,
	"name": "OVEJAS",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9e4"
	},
	"id": 27245,
	"name": "EL CARMEN DE ATRATO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa16"
	},
	"id": 23079,
	"name": "BUENAVISTA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa2f"
	},
	"id": 20175,
	"name": "CHIMICHAGUA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f31e"
	},
	"id": 5647,
	"name": "SAN ANDRÉS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa7a"
	},
	"id": 54109,
	"name": "BUCARASICA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f463"
	},
	"id": 68079,
	"name": "BARICHARA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f47c"
	},
	"id": 13440,
	"name": "MARGARITA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f577"
	},
	"id": 15759,
	"name": "SOGAMOSO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5db"
	},
	"id": 73024,
	"name": "ALPUJARRA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f60d"
	},
	"id": 52240,
	"name": "CHACHAGUÍ",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f626"
	},
	"id": 25506,
	"name": "VENECIA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f314"
	},
	"id": 25592,
	"name": "QUEBRADANEGRA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f346"
	},
	"id": 5659,
	"name": "SAN JUAN DE URABÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3aa"
	},
	"id": 27050,
	"name": "ATRATO (YUTO)",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3c3"
	},
	"id": 25293,
	"name": "GACHALA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3dc"
	},
	"id": 5044,
	"name": "ANZÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f40e"
	},
	"id": 15380,
	"name": "LA CAPILLA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f752"
	},
	"id": 76670,
	"name": "SAN PEDRO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f459"
	},
	"id": 8520,
	"name": "PALMAR DE VARELA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f472"
	},
	"id": 68190,
	"name": "CIMITARRA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f79b"
	},
	"id": 5148,
	"name": "CARMEN DE VIBORAL",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f508"
	},
	"id": 27150,
	"name": "CARMEN DEL DARIEN",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f831"
	},
	"id": 25513,
	"name": "PACHO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f53a"
	},
	"id": 68318,
	"name": "GUACA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f56d"
	},
	"id": 15720,
	"name": "SATIVANORTE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5d1"
	},
	"id": 52885,
	"name": "YACUANQUER",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8f9"
	},
	"id": 25126,
	"name": "CAJICÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f92b"
	},
	"id": 15104,
	"name": "BOYACÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f95d"
	},
	"id": 15407,
	"name": "VILLA DE LEYVA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f667"
	},
	"id": 19300,
	"name": "GUACHENE",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f98f"
	},
	"id": 13810,
	"name": "TIQUISIO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9a8"
	},
	"id": 44650,
	"name": "SAN JUAN DEL CESAR",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9c1"
	},
	"id": 13683,
	"name": "SANTA ROSA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9f3"
	},
	"id": 76497,
	"name": "OBANDO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa0c"
	},
	"id": 23686,
	"name": "SAN PELAYO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa57"
	},
	"id": 44430,
	"name": "MAICAO",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa70"
	},
	"id": 54172,
	"name": "CHINÁCOTA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2ce"
	},
	"id": 68322,
	"name": "GUAPOTÁ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2e7"
	},
	"id": 63190,
	"name": "CIRCASIA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fabb"
	},
	"id": 20045,
	"name": "BECERRIL",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f364"
	},
	"id": 5347,
	"name": "HELICONIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f396"
	},
	"id": 15816,
	"name": "TOGÜÍ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f413"
	},
	"id": 15189,
	"name": "CIÉNEGA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8a4"
	},
	"id": 5364,
	"name": "JARDÍN",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f908"
	},
	"id": 25224,
	"name": "CUCUNUBÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4db"
	},
	"id": 23464,
	"name": "MOMIL",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f99e"
	},
	"id": 68575,
	"name": "PUERTO WILCHES",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5bd"
	},
	"id": 76130,
	"name": "CANDELARIA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5ef"
	},
	"id": 52786,
	"name": "TAMINANGO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f685"
	},
	"id": 41797,
	"name": "TESALIA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f71b"
	},
	"id": 97161,
	"name": "CARURÚ",
	"departmentId": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f734"
	},
	"id": 68855,
	"name": "VALLE DE SAN JOSÉ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7af"
	},
	"id": 73547,
	"name": "PIEDRAS",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9ee"
	},
	"id": 15425,
	"name": "MACANAL",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa52"
	},
	"id": 44874,
	"name": "VILLANUEVA",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fab6"
	},
	"id": 91407,
	"name": "LA PEDRERA",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fb5c"
	},
	"id": 50287,
	"name": "FUENTE DE ORO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc3d"
	},
	"id": 52427,
	"name": "MAGUI",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc88"
	},
	"id": 68679,
	"name": "SAN GIL",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fca1"
	},
	"id": 68770,
	"name": "SUAITA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcba"
	},
	"id": 73347,
	"name": "HERVEO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe18"
	},
	"id": 68081,
	"name": "BARRANCABERMEJA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe31"
	},
	"id": 25320,
	"name": "GUADUAS",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe7c"
	},
	"id": 5091,
	"name": "BETANIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe95"
	},
	"id": 15051,
	"name": "ARCABUCO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff44"
	},
	"id": 70400,
	"name": "LA UNION",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff8f"
	},
	"id": 54520,
	"name": "PAMPLONITA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffd6"
	},
	"id": 68705,
	"name": "SANTA BÁRBARA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffef"
	},
	"id": 19392,
	"name": "LA SIERRA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8faee"
	},
	"id": 76869,
	"name": "VIJES",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fb07"
	},
	"id": 73563,
	"name": "PRADO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc33"
	},
	"id": 52560,
	"name": "POTOSI",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcc9"
	},
	"id": 73461,
	"name": "MURILLO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcfb"
	},
	"id": 13473,
	"name": "MORALES",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fddc"
	},
	"id": 5284,
	"name": "FRONTINO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe8b"
	},
	"id": 15238,
	"name": "DUITAMA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fed6"
	},
	"id": 8137,
	"name": "CAMPO DE LA CRUZ",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff08"
	},
	"id": 27413,
	"name": "LLORO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff9e"
	},
	"id": 15180,
	"name": "CHISCAS",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb901a7"
	},
	"id": 68861,
	"name": "VÉLEZ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc79"
	},
	"id": 50689,
	"name": "SAN MARTIN",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd41"
	},
	"id": 68296,
	"name": "GALÁN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fdd7"
	},
	"id": 5264,
	"name": "ENTRERRIOS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe54"
	},
	"id": 25377,
	"name": "LA CALERA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe86"
	},
	"id": 15232,
	"name": "CHÍQUIZA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff67"
	},
	"id": 68502,
	"name": "ONZAGA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffb2"
	},
	"id": 15223,
	"name": "CUBARÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90157"
	},
	"id": 86749,
	"name": "SIBUNDOY",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8faf3"
	},
	"id": 76364,
	"name": "JAMUNDÍ",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fb25"
	},
	"id": 52506,
	"name": "OSPINA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fce7"
	},
	"id": 73270,
	"name": "FALAN",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd19"
	},
	"id": 68176,
	"name": "CHIMA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd32"
	},
	"id": 68444,
	"name": "MATANZA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd64"
	},
	"id": 25430,
	"name": "MADRID",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fdfa"
	},
	"id": 5400,
	"name": "LA UNIÓN",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe13"
	},
	"id": 15761,
	"name": "SOMONDOCO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe5e"
	},
	"id": 5002,
	"name": "ABEJORRAL",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc74"
	},
	"id": 95200,
	"name": "MIRAFLORES",
	"departmentId": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe1d"
	},
	"id": 25168,
	"name": "CHAGUANÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe4f"
	},
	"id": 17050,
	"name": "ARANZAZU",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe81"
	},
	"id": 15131,
	"name": "CALDAS",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fecc"
	},
	"id": 8638,
	"name": "SABANALARGA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffc6"
	},
	"id": 15518,
	"name": "PAJARITO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffdb"
	},
	"id": 15183,
	"name": "CHITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fff4"
	},
	"id": 44110,
	"name": "EL MOLINO",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb902b6"
	},
	"id": 25743,
	"name": "SILVANIA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb902cf"
	},
	"id": 5607,
	"name": "RETIRO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb9040a"
	},
	"id": 47570,
	"name": "PUEBLOVIEJO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90482"
	},
	"id": 23350,
	"name": "LA APARTADA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90365"
	},
	"id": 25019,
	"name": "ALBÁN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90504"
	},
	"id": 15087,
	"name": "BELÉN",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905e4"
	},
	"id": 50370,
	"name": "URIBE",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90643"
	},
	"id": 41013,
	"name": "AGRADO",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb903fb"
	},
	"id": 27135,
	"name": "EL CANTÓN DEL SAN PABLO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9067a"
	},
	"id": 25339,
	"name": "GUTIÉRREZ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9050e"
	},
	"id": 44378,
	"name": "HATO NUEVO",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905bc"
	},
	"id": 52720,
	"name": "SAPUYES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90729"
	},
	"id": 54670,
	"name": "SAN CALIXTO",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90701"
	},
	"id": 68755,
	"name": "SOCORRO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90733"
	},
	"id": 54498,
	"name": "OCAÑA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9074c"
	},
	"id": 70742,
	"name": "SINCÉ",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90887"
	},
	"id": 5585,
	"name": "PUERTO NARE",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908a0"
	},
	"id": 15600,
	"name": "RÁQUIRA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908b9"
	},
	"id": 25151,
	"name": "CAQUEZA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907c9"
	},
	"id": 63111,
	"name": "BUENAVISTA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90814"
	},
	"id": 25658,
	"name": "SAN FRANCISCO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90927"
	},
	"id": 15001,
	"name": "TUNJA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90882"
	},
	"id": 27800,
	"name": "UNGUÍA",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9089b"
	},
	"id": 15599,
	"name": "RAMIRIQUÍ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908b4"
	},
	"id": 15580,
	"name": "QUÍPAMA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90931"
	},
	"id": 5342,
	"name": "CAMPAMENTO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904d7"
	},
	"id": 15723,
	"name": "SATIVASUR",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90509"
	},
	"id": 76828,
	"name": "TRUJILLO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90747"
	},
	"id": 5113,
	"name": "BURITICÁ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908be"
	},
	"id": 15879,
	"name": "VIRACACHÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90306"
	},
	"id": 5809,
	"name": "TITIRIBÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904e1"
	},
	"id": 54125,
	"name": "CÁCOTA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90738"
	},
	"id": 5138,
	"name": "CAÑASGORDAS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90751"
	},
	"id": 73349,
	"name": "HONDA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907ce"
	},
	"id": 68255,
	"name": "EL PLAYÓN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90819"
	},
	"id": 25580,
	"name": "PULÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90896"
	},
	"id": 15690,
	"name": "SANTA MARÍA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908af"
	},
	"id": 15740,
	"name": "SIACHOQUE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908fa"
	},
	"id": 17486,
	"name": "NEIRA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90913"
	},
	"id": 15507,
	"name": "OTANCHE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9095e"
	},
	"id": 8560,
	"name": "PONEDERA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909a9"
	},
	"id": 13248,
	"name": "EL GUAMO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909dd"
	},
	"id": 15798,
	"name": "TENZA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909e7"
	},
	"id": 23189,
	"name": "CIÉNAGA DE ORO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909c9"
	},
	"id": 13030,
	"name": "ALTOS DEL ROSARIO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a8c"
	},
	"id": 91540,
	"name": "PUERTO NARIÑO",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a1e"
	},
	"id": 44098,
	"name": "DISTRACCIÓN",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a69"
	},
	"id": 15533,
	"name": "PAYA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf3"
	},
	"id": 66001,
	"name": "PEREIRA",
	"departmentId": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee25"
	},
	"id": 5819,
	"name": "TOLEDO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf8"
	},
	"id": 25871,
	"name": "VILLAGÓMEZ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5c"
	},
	"id": 5237,
	"name": "DON MATÍAS",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee75"
	},
	"id": 5576,
	"name": "PUEBLORRICO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef51"
	},
	"id": 13838,
	"name": "TURBANA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe7"
	},
	"id": 85440,
	"name": "VILLANUEVA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f019"
	},
	"id": 54239,
	"name": "DURANIA",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f04f"
	},
	"id": 76100,
	"name": "BOLÍVAR",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec72"
	},
	"id": 25053,
	"name": "ARBELÁEZ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecbd"
	},
	"id": 19548,
	"name": "PIENDAMÓ",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee02"
	},
	"id": 25491,
	"name": "NOCAIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1b"
	},
	"id": 25807,
	"name": "TIBIRITA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef60"
	},
	"id": 8436,
	"name": "MANATI",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef79"
	},
	"id": 13673,
	"name": "SANTA CATALINA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f041"
	},
	"id": 15248,
	"name": "EL ESPINO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f072"
	},
	"id": 15822,
	"name": "TOTA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8a"
	},
	"id": 47605,
	"name": "REMOLINO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edbc"
	},
	"id": 68368,
	"name": "JESÚS MARÍA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee6b"
	},
	"id": 5266,
	"name": "ENVIGADO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee8"
	},
	"id": 17616,
	"name": "RISARALDA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4c"
	},
	"id": 8078,
	"name": "BARANOA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f077"
	},
	"id": 15276,
	"name": "FLORESTA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebbe"
	},
	"id": 52210,
	"name": "CONTADERO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeac"
	},
	"id": 15897,
	"name": "ZETAQUIRA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec5"
	},
	"id": 25281,
	"name": "FOSCA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef29"
	},
	"id": 15022,
	"name": "ALMEIDA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5b"
	},
	"id": 13442,
	"name": "MARIA LA BAJA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d1"
	},
	"id": 52001,
	"name": "PASTO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f211"
	},
	"id": 97511,
	"name": "PACOA (COR. DEPARTAMENTAL)",
	"departmentId": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f29d"
	},
	"id": 47545,
	"name": "PIJIÑO  DEL CARMEN",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1fd"
	},
	"id": 68720,
	"name": "SANTA HELENA DEL OPÓN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f234"
	},
	"id": 5142,
	"name": "CARACOLÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3c9"
	},
	"id": 25328,
	"name": "GUAYABAL DE SÍQUIMA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3fb"
	},
	"id": 5088,
	"name": "BELLO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4dc"
	},
	"id": 8832,
	"name": "TUBARÁ",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7e7"
	},
	"id": 68179,
	"name": "CHIPATÁ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f800"
	},
	"id": 68167,
	"name": "CHARALÁ",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f864"
	},
	"id": 25805,
	"name": "TIBACUY",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f990"
	},
	"id": 8001,
	"name": "BARRANQUILLA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f310"
	},
	"id": 25662,
	"name": "SAN JUAN DE RÍO SECO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f423"
	},
	"id": 17665,
	"name": "SAN JOSÉ",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4a0"
	},
	"id": 27361,
	"name": "ISTMINA",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f582"
	},
	"id": 99524,
	"name": "LA PRIMAVERA",
	"departmentId": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa3f"
	},
	"id": 68682,
	"name": "SAN JOAQUÍN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa71"
	},
	"id": 44035,
	"name": "ALBANIA",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f85a"
	},
	"id": 5615,
	"name": "RIONEGRO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8a5"
	},
	"id": 5306,
	"name": "GIRALDO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8d7"
	},
	"id": 15646,
	"name": "SAMACÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f36a"
	},
	"id": 5310,
	"name": "GÓMEZ PLATA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f383"
	},
	"id": 5495,
	"name": "NECHÍ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f400"
	},
	"id": 5059,
	"name": "ARMENIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f513"
	},
	"id": 68686,
	"name": "SAN MIGUEL",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f60e"
	},
	"id": 52227,
	"name": "CUMBAL",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f672"
	},
	"id": 41244,
	"name": "ELÍAS",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f81e"
	},
	"id": 63401,
	"name": "LA TEBAIDA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f837"
	},
	"id": 25898,
	"name": "ZIPACÓN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8b4"
	},
	"id": 5282,
	"name": "FREDONIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f995"
	},
	"id": 47798,
	"name": "TENERIFE",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa76"
	},
	"id": 68207,
	"name": "CONCEPCIÓN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa8f"
	},
	"id": 15537,
	"name": "PAZ DE RÍO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4ff"
	},
	"id": 85136,
	"name": "LA SALINA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f564"
	},
	"id": 25290,
	"name": "FUSAGASUGÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f57d"
	},
	"id": 50245,
	"name": "EL CALVARIO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f613"
	},
	"id": 52385,
	"name": "LA LLANADA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f774"
	},
	"id": 5150,
	"name": "CAROLINA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7bf"
	},
	"id": 23162,
	"name": "CERETÉ",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7d8"
	},
	"id": 23672,
	"name": "SAN ANTERO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8a0"
	},
	"id": 5353,
	"name": "HISPANIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9b3"
	},
	"id": 13620,
	"name": "SAN CRISTOBAL",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa49"
	},
	"id": 20060,
	"name": "BOSCONIA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2fc"
	},
	"id": 25899,
	"name": "ZIPAQUIRÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f32e"
	},
	"id": 25653,
	"name": "SAN CAYETANO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4d7"
	},
	"id": 20295,
	"name": "GAMARRA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8dc"
	},
	"id": 5101,
	"name": "CIUDAD BOLÍVAR",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8f5"
	},
	"id": 25001,
	"name": "AGUA DE DIOS",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f90e"
	},
	"id": 25288,
	"name": "FÚQUENE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd06"
	},
	"id": 47205,
	"name": "CONCORDIA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe19"
	},
	"id": 15638,
	"name": "SÁCHICA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffc2"
	},
	"id": 15522,
	"name": "PANQUEBA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc5c"
	},
	"id": 52473,
	"name": "MOSQUERA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd88"
	},
	"id": 25781,
	"name": "SUTATAUSA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe1e"
	},
	"id": 25307,
	"name": "GIRARDOT",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff18"
	},
	"id": 13188,
	"name": "CICUCO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fffa"
	},
	"id": 20550,
	"name": "PELAYA",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe91"
	},
	"id": 15185,
	"name": "CHITARAQUE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8feaa"
	},
	"id": 15236,
	"name": "CHIVOR",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff40"
	},
	"id": 70702,
	"name": "SAN JUAN BETULIA",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffbd"
	},
	"id": 15244,
	"name": "EL COCUY",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb901b2"
	},
	"id": 68549,
	"name": "PINCHOTE",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc48"
	},
	"id": 52699,
	"name": "SANTACRUZ",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd42"
	},
	"id": 68209,
	"name": "CONFINES",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff1d"
	},
	"id": 17388,
	"name": "LA MERCED",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd2e"
	},
	"id": 68169,
	"name": "CHARTA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe73"
	},
	"id": 17524,
	"name": "PALESTINA",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff09"
	},
	"id": 13140,
	"name": "CALAMAR",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90230"
	},
	"id": 13667,
	"name": "SAN MARTIN DE LOBA",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb903c0"
	},
	"id": 15476,
	"name": "MOTAVITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904a1"
	},
	"id": 85315,
	"name": "SÁCAMA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90519"
	},
	"id": 44560,
	"name": "MANAURE",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90523"
	},
	"id": 85015,
	"name": "CHÁMEZA",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90649"
	},
	"id": 41026,
	"name": "ALTAMIRA",
	"departmentId": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb906ad"
	},
	"id": 86219,
	"name": "COLON",
	"departmentId": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90716"
	},
	"id": 97777,
	"name": "PAPUNAUA (COR. DEPARTAMENTAL)",
	"departmentId": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90748"
	},
	"id": 76823,
	"name": "TORO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90824"
	},
	"id": 50226,
	"name": "CUMARAL",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907de"
	},
	"id": 25867,
	"name": "VIANÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90874"
	},
	"id": 5321,
	"name": "GUATAPÉ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908f1"
	},
	"id": 25312,
	"name": "GRANADA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904dd"
	},
	"id": 15839,
	"name": "TUTAZÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9058b"
	},
	"id": 76111,
	"name": "BUGA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905d6"
	},
	"id": 52083,
	"name": "BELÉN",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9082e"
	},
	"id": 25843,
	"name": "VILLA DE SAN DIEGO DE UBATE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908ab"
	},
	"id": 15778,
	"name": "SUTATENZA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9081a"
	},
	"id": 25612,
	"name": "RICAURTE",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908b0"
	},
	"id": 27810,
	"name": "UNIÓN PANAMERICANA ( ANIMAS)",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9095f"
	},
	"id": 20001,
	"name": "VALLEDUPAR",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a3d"
	},
	"id": 44001,
	"name": "RIOHACHA",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9096e"
	},
	"id": 8675,
	"name": "SANTA LUCIA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a8d"
	},
	"id": 20621,
	"name": "LA PAZ",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a83"
	},
	"id": 19807,
	"name": "TIMBÍO",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90b77"
	},
	"id": 19780,
	"name": "SUÁREZ",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90b4f"
	},
	"id": 25524,
	"name": "PANDI",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90c71"
	},
	"id": 97889,
	"name": "YABARATÉ",
	"departmentId": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90ca3"
	},
	"id": 70820,
	"name": "TOLÚ",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90d25"
	},
	"id": 68320,
	"name": "GUADALUPE",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90d9d"
	},
	"id": 5658,
	"name": "SAN JOSÉ DE LA MONTAÑA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90dcf"
	},
	"id": 5861,
	"name": "VENECIA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90faa"
	},
	"id": 15332,
	"name": "GÜICÁN",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fdd"
	},
	"id": 27250,
	"name": "EL LITORAL DEL SAN JUAN",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91186"
	},
	"id": 52520,
	"name": "FRANCISCO PIZARRO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9148e"
	},
	"id": 73055,
	"name": "ARMERO",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90e79"
	},
	"id": 15176,
	"name": "CHIQUINQUIRÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f8c"
	},
	"id": 15757,
	"name": "SOCHA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90dde"
	},
	"id": 5591,
	"name": "PUERTO TRIUNFO",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb911ae"
	},
	"id": 94883,
	"name": "SAN FELIPE",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9146b"
	},
	"id": 23500,
	"name": "MOÑITOS",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91524"
	},
	"id": 91798,
	"name": "TARAPACÁ",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb916af"
	},
	"id": 52287,
	"name": "FUNES",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9160a"
	},
	"id": 19513,
	"name": "PADILLA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9187b"
	},
	"id": 15762,
	"name": "SORA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9188a"
	},
	"id": 15660,
	"name": "SAN EDUARDO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91952"
	},
	"id": 47245,
	"name": "EL BANCO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91709"
	},
	"id": 70823,
	"name": "TOLUVIEJO",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb919c3"
	},
	"id": 15776,
	"name": "SUTAMARCHÁN",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90cc6"
	},
	"id": 73030,
	"name": "AMBALEMA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f37"
	},
	"id": 23586,
	"name": "PURISIMA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fce"
	},
	"id": 85279,
	"name": "RECETOR",
	"departmentId": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a6d"
	},
	"id": 25817,
	"name": "TOCANCIPÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a72"
	},
	"id": 44420,
	"name": "LA JAGUA DEL PILAR",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91ca8"
	},
	"id": 23168,
	"name": "CHIMA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91e88"
	},
	"id": 8758,
	"name": "SOLEDAD",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91eec"
	},
	"id": 27073,
	"name": "BAGADO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee8a"
	},
	"id": 15804,
	"name": "TIBANÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7b"
	},
	"id": 5475,
	"name": "MURINDÓ",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06e"
	},
	"id": 91536,
	"name": "PUERTO ARICA",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc5"
	},
	"id": 70418,
	"name": "LOS PALMITOS",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed0e"
	},
	"id": 70124,
	"name": "CAIMITO",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed72"
	},
	"id": 20238,
	"name": "EL COPEY",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8b"
	},
	"id": 44090,
	"name": "DIBULLA",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4d"
	},
	"id": 47541,
	"name": "PEDRAZA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efca"
	},
	"id": 70110,
	"name": "BUENAVISTA",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02e"
	},
	"id": 54347,
	"name": "HERRÁN",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f078"
	},
	"id": 5642,
	"name": "SALGAR",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f082"
	},
	"id": 76306,
	"name": "GINEBRA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f091"
	},
	"id": 73854,
	"name": "VALLE DE SAN JUAN",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f21c"
	},
	"id": 70523,
	"name": "PALMITO",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2b2"
	},
	"id": 68217,
	"name": "COROMORO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f26c"
	},
	"id": 73152,
	"name": "CASABIANCA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f415"
	},
	"id": 15500,
	"name": "OICATÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f460"
	},
	"id": 47980,
	"name": "ZONA BANANERA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f574"
	},
	"id": 15377,
	"name": "LABRANZAGRANDE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f833"
	},
	"id": 25875,
	"name": "VILLETA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8e2"
	},
	"id": 15676,
	"name": "SAN MIGUEL DE SEMA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f946"
	},
	"id": 15494,
	"name": "NUEVO COLÓN",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f991"
	},
	"id": 8558,
	"name": "POLONUEVO",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4e2"
	},
	"id": 23678,
	"name": "SAN CARLOS",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f86a"
	},
	"id": 25799,
	"name": "TENJO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa13"
	},
	"id": 23670,
	"name": "SAN ANDRES DE SOTAVENTO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30c"
	},
	"id": 25745,
	"name": "SIMIJACA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f41f"
	},
	"id": 17877,
	"name": "VITERBO",
	"departmentId": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f678"
	},
	"id": 19845,
	"name": "VILLA RICA",
	"departmentId": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f775"
	},
	"id": 76020,
	"name": "ALCALÁ",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f410"
	},
	"id": 15224,
	"name": "CUCAITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f32a"
	},
	"id": 25797,
	"name": "TENA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa54"
	},
	"id": 44279,
	"name": "FONSECA",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f46f"
	},
	"id": 47703,
	"name": "SAN ZENÓN",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8f1"
	},
	"id": 25099,
	"name": "BOJACÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa81"
	},
	"id": 15215,
	"name": "CORRALES",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe65"
	},
	"id": 15106,
	"name": "BRICEÑO",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc3a"
	},
	"id": 52683,
	"name": "SANDONÁ",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fff1"
	},
	"id": 27491,
	"name": "NÓVITA",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb902a4"
	},
	"id": 25483,
	"name": "NARIÑO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90407"
	},
	"id": 47161,
	"name": "CERRO DE SAN ANTONIO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90587"
	},
	"id": 76248,
	"name": "EL CERRITO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90816"
	},
	"id": 25754,
	"name": "SOACHA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb906ef"
	},
	"id": 97001,
	"name": "MITU",
	"departmentId": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907ee"
	},
	"id": 63470,
	"name": "MONTENEGRO",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9087f"
	},
	"id": 5501,
	"name": "OLAYA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90965"
	},
	"id": 47288,
	"name": "FUNDACIÓN",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9096a"
	},
	"id": 47707,
	"name": "SANTA ANA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9097e"
	},
	"id": 13647,
	"name": "SAN ESTANISLAO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90960"
	},
	"id": 47660,
	"name": "SABANAS DE SAN ÁNGEL",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90c63"
	},
	"id": 95015,
	"name": "CALAMAR",
	"departmentId": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90dfd"
	},
	"id": 68051,
	"name": "ARATOCA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f1f"
	},
	"id": 13062,
	"name": "ARROYOHONDO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fe3"
	},
	"id": 76054,
	"name": "ARGELIA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fcf"
	},
	"id": 15466,
	"name": "MONGUI",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f88"
	},
	"id": 68162,
	"name": "CERRITO",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fd9"
	},
	"id": 91405,
	"name": "LA CHORRERA",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91282"
	},
	"id": 63690,
	"name": "SALENTO",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb916c4"
	},
	"id": 52320,
	"name": "GUAITARILLA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb916ce"
	},
	"id": 73148,
	"name": "CARMEN APICALA",
	"departmentId": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91782"
	},
	"id": 47001,
	"name": "SANTA MARTA",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb918fe"
	},
	"id": 15162,
	"name": "CERINZA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9193f"
	},
	"id": 20013,
	"name": "AGUSTIN CODAZZI",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91908"
	},
	"id": 15401,
	"name": "LA VICTORIA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a1e"
	},
	"id": 68425,
	"name": "MACARAVITA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb919c4"
	},
	"id": 23300,
	"name": "COTORRA",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a37"
	},
	"id": 15464,
	"name": "MONGUA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a73"
	},
	"id": 91669,
	"name": "PUERTO SANTANDER",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91ae7"
	},
	"id": 52418,
	"name": "LOS ANDES (SOTOMAYOR)",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91c45"
	},
	"id": 94888,
	"name": "PANÁ PANÁ",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91c4f"
	},
	"id": 70265,
	"name": "GUARANDA",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91de9"
	},
	"id": 25200,
	"name": "COGUA",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91e84"
	},
	"id": 8433,
	"name": "MALAMBO",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92425"
	},
	"id": 76863,
	"name": "VERSALLES",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92a17"
	},
	"id": 99001,
	"name": "PUERTO CARREÑO",
	"departmentId": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92475"
	},
	"id": 54553,
	"name": "PUERTO SANTANDER",
	"departmentId": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb928f5"
	},
	"id": 8770,
	"name": "SUAN",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb929f9"
	},
	"id": 15092,
	"name": "BETEITIVA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92c46"
	},
	"id": 13074,
	"name": "BOLÍVAR",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92caa"
	},
	"id": 68327,
	"name": "GÜEPSA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92e62"
	},
	"id": 13580,
	"name": "REGIDOR",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92d9f"
	},
	"id": 25324,
	"name": "GUATAQUÍ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92d95"
	},
	"id": 25086,
	"name": "BELTRÁN",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92e44"
	},
	"id": 8606,
	"name": "REPELON",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb934a7"
	},
	"id": 91530,
	"name": "PUERTO ALEGRÍA",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06a"
	},
	"id": 15403,
	"name": "LA UVITA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f079"
	},
	"id": 88001,
	"name": "SAN ANDRES",
	"departmentId": 88,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f466"
	},
	"id": 8141,
	"name": "CANDELARIA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f52e"
	},
	"id": 15114,
	"name": "BUSBANZÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f556"
	},
	"id": 15810,
	"name": "TIPACOQUE",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3a3"
	},
	"id": 50686,
	"name": "SAN JUANITO",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f45c"
	},
	"id": 8849,
	"name": "USIACURI",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f750"
	},
	"id": 94886,
	"name": "CACAHUAL",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f385"
	},
	"id": 5390,
	"name": "LA PINTADA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9bf"
	},
	"id": 13760,
	"name": "SOPLAVIENTO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcef"
	},
	"id": 47720,
	"name": "SANTA BÁRBARA DE PINTO",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe25"
	},
	"id": 25269,
	"name": "FACATATIVÁ",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905c4"
	},
	"id": 52480,
	"name": "NARIÑO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff4c"
	},
	"id": 23675,
	"name": "SAN BERNARDO DEL VIENTO",
	"departmentId": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9078b"
	},
	"id": 20787,
	"name": "TAMALAMEQUE",
	"departmentId": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffba"
	},
	"id": 15296,
	"name": "GAMEZA",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb901ec"
	},
	"id": 70221,
	"name": "COVEÑAS",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90534"
	},
	"id": 68132,
	"name": "CALIFORNIA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90c28"
	},
	"id": 52256,
	"name": "EL ROSARIO",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92711"
	},
	"id": 13600,
	"name": "RIOVIEJO",
	"departmentId": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92725"
	},
	"id": 47258,
	"name": "EL PIÑÓN",
	"departmentId": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8faba"
	},
	"id": 63001,
	"name": "ARMENIA",
	"departmentId": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa51"
	},
	"id": 44078,
	"name": "BARRANCAS",
	"departmentId": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd9f"
	},
	"id": 25785,
	"name": "TABIO",
	"departmentId": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb933f9"
	},
	"id": 15187,
	"name": "CHIVATÁ",
	"departmentId": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcaa"
	},
	"id": 76147,
	"name": "CARTAGO",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90615"
	},
	"id": 50680,
	"name": "SAN CARLOS DE GUAROA",
	"departmentId": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9095d"
	},
	"id": 8634,
	"name": "SABANAGRANDE",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a31"
	},
	"id": 68432,
	"name": "MÁLAGA",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91518"
	},
	"id": 91263,
	"name": "EL ENCANTO",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91941"
	},
	"id": 8296,
	"name": "GALAPA",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a66"
	},
	"id": 91430,
	"name": "LA VICTORIA",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb9212f"
	},
	"id": 52565,
	"name": "PROVIDENCIA",
	"departmentId": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb9216b"
	},
	"id": 94885,
	"name": "LA GUADALUPE",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb9349a"
	},
	"id": 27745,
	"name": "SIPÍ",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb934a4"
	},
	"id": 27660,
	"name": "SAN JOSÉ DEL PALMAR",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ff6965613a0cb93bda"
	},
	"id": 70235,
	"name": "GALERAS",
	"departmentId": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05006965613a0cb941b2"
	},
	"id": 68370,
	"name": "JORDÁN",
	"departmentId": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05006965613a0cb9463f"
	},
	"id": 97666,
	"name": "TARAIRA",
	"departmentId": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05016965613a0cb949ae"
	},
	"id": 27205,
	"name": "CONDOTO",
	"departmentId": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90d79"
	},
	"id": 5631,
	"name": "SABANETA",
	"departmentId": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9116c"
	},
	"id": 18029,
	"name": "ALBANIA",
	"departmentId": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91776"
	},
	"id": 8685,
	"name": "SANTO TOMÁS",
	"departmentId": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91523"
	},
	"id": 88564,
	"name": "PROVIDENCIA",
	"departmentId": 88,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91fa9"
	},
	"id": 91001,
	"name": "LETICIA",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb9216c"
	},
	"id": 94884,
	"name": "PUERTO COLOMBIA (COR. DEPARTAMENTAL)",
	"departmentId": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05006965613a0cb9446d"
	},
	"id": 76243,
	"name": "EL ÁGUILA",
	"departmentId": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05076965613a0cb963b0"
	},
	"id": 91460,
	"name": "MIRITÍ - PARANÁ",
	"departmentId": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05086965613a0cb967b4"
	},
	"id": 8573,
	"name": "PUERTO COLOMBIA",
	"departmentId": 8,
	"__v": 0
}]

