export const requirementList = {
    "sanitary": [
        {
            "id": 1,
            "description": 'Se cumple con los requisitos establecidos para las enfermedades de control oficial, declaracion obligatoria y de vacunacion 1',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 2,
            "description": 'Se realizan actividades de prevencion y control de otras enfermedades infecciosas y parasitarias que afectan al predio',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 3,
            "description": 'La zona de produccion dispone de cercos, broches, puertas, barreras de aislamiento natural u otros mecanismos que delimita el paso de otros animales o personas ajenas al predio',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 4,
            "description": 'Si el sistema productivo es estabulado cuenta con registro de ingreso de salida de personas y vehiculos, que contenga como minimo: nombre, fecha, nro identificaion o placa del vehículo, telefono, origen y objeto de la visita',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 5,
            "description": 'La mortalidad generada en el predio no se destina para consumo humano o animal, ni su disposición ﬁnal genera riesgo sanitario y de inocuidad',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 6,
            "description": 'Los animales se encuentran identiﬁcados de forma individual o por lotes y facilita la trazabilidad de los productos derivados de los mismos',
            "maxScore": 5,
            "observation": ''
        }
    ],
    "sanitation": [
        {
            "id": 7,
            "description": 'Se impide el acceso de manera efectiva de los animales a basureros, rellenos sanitarios, fuentes de contaminación lugares que respresentan riesgo para la inocuidad de la carne y/o fuente de agua declaradas como contaminadas o de riesgo para la inocuidad de la carne o la sanidad animal',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 8,
            "description": 'El predio dispone de un lugar donde se clasifica y almacena temporalmente los residuos y basuras en el predio, de forma tal que se evita el riesgo sanitario deivado de la proliferación de plagas. Para este mismo propósito se evita la acumulación de residuos orgánicos, los escombros, la maquinaria y equipos en desuso',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 9,
            "description": 'El manejo y disposición de envases vacios de productos veterinarios y de los productos veterinarios vencidos se realiza de acuerdo con las instrucciones del etiquetado rotulado del producto y la normatividad vigente',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 10,
            "description": 'Se conoce cuáles son la(s) fuente(s) de agua. En caso de tener tanques para el almacenamiento del agua debe estar construidos con materiales que faciliten su limpieza, desinfección cuando sea requerida y permanecer tapados y limpios',
            "maxScore": 5,
            "observation": ''
        }
    ],
    "bestPractices": [
        {
            "id": 11,
            "description": 'Los medicamentos y biológicos veterinarios, alimentos comerciales para animales, suplementos nutricionales, sales mineralizadas, plaguicidas, fertilizantes y demás productos veterinarios utilizados en el predio cuentas con registro del ICA',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 12,
            "description": 'Se evidencia que en el predio no se utilizan sustancias prohibidas por el ICA (Las sustancias actualmente prohibidas por el ICA para su uso en producción pecuaria son; dimetridazol, cloranfenicol, furazolidona, nitrofurazona y furaltadona, olaquindox, dietilestilbestrol, violeta de genciana via oral y arsénico)',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 13,
            "description": 'No se emplean productos veterinarios vencidos',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 14,
            "description": 'Los tratamientos veterinarios hormonales, antibióticos, anestésicos y relajantes musculares son prescritos únicamente por un médico veterinario o médico veterinario zootecnista con matrícula profesional. Se verifica la existencia de las respectivas prescripciones por un periodo mínimo de 6 meses',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 15,
            "description": 'Se evidencia la utilización de Medicamentos Veterinarios de control especial con su respectiva prescripción Médico veterinaria en su fórmula oﬁcial. De acuerdo a la normatividad vigente las sustancias de control especial son: La Oxitocina, las Prostaglandinas, la Ketamina, Pentobarbital Sódico y Tiopental Sódico, Etiproston, D-Cloprostenol Sódico, Butorfanol, Lupostinol y Tiaprost prometamina. Podrá identiﬁcar estos medicamentos por la franja de color violeta que se encuentra en su etiqueta.',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 16,
            "description": 'Cuenta con áreas, instalaciones independientes o contenedores cerrados y separados fisicamente para el almacenamiento de medicamentos veterinarios, alimentos para animales, suplementos nutricionales, sales mineralizadas, fertilizantes, plaguicidas, equipos y/o herramientas, de tal forma que se mantiene su calidad y se evita el riesgo de contaminación cruzada',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 17,
            "description": 'El uso y almacenamiento de los Medicamentos y Biológicos Veterinarios se realiza de acuerdo a las condiciones e instrucciones consignadas en su rotulado',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 18,
            "description": 'El predio cuenta con el Registro oficial de Tratamientos Veterinarios. Este registro debe incluir como mínimo: fecha de inicio del tratamiento, nombre del medicamento, número del registro ICA, número de lote del medicamento, dosis administrativa, via de administración, especie e identificación individual del animal o lote de animales tratados, fecha de finalización de tratamiento, nombre y apellido del aplicador del tratamiento, tiempo de retiro (en días y horas), nombre, apellido, número de tarjeta profesional vigente y firma del médico veterinario o médico veterinario zootecnista, para los tratamientos de los medicamentos que asi lo requieran. Debe tener un antecedente o historial de mínimo 3 meses',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 19,
            "description": 'Se respeta el tiempo de retiro de los medicamentos veterinarios en los animales que están bajo tratamiento, de acuerdo con lo establecido en el rotulado y dicho tiempo de retiro se encuentra debidamente consignado en el registroofical de tratamientos veterinarios',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 20,
            "description": 'Se utilizan en el predio productos veterinarios como promotores de crecimiento, cuando el registro ICA expresamente autorice su uso',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 21,
            "description": 'No se emplean en la alimentación de los animales productos o subproductos de consechas de cultivos ornamentales, leche de retiro, mortalidades, despojos de animales, excretas (Pollinaza, Gallinaza, Porquinaza, etc.), desechos de la alimentación humana (lavanzas)',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 22,
            "description": 'No se emplean alimentos balanceados y/o suplementos alimenticios y minerales que contengan harinas de carne, sangre y hueso vaporizado; de carne y hueso y despojos de mamíferos',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 23,
            "description": 'Se respeta el periodo de carencia de los plaguicidas y demás insumos agrícolas utilizados en los forrajes, cultivos y subproductos de cosechas destinados para la alimentación de los animales, de acuerdo con el rotulado del producto',
            "maxScore": 2.14,
            "observation": ''
        },
        {
            "id": 24,
            "description": 'Las agujas empleadas en la administración de medicamentos son almacenadas en un recipiente seguro o guardián',
            "maxScore": 2.14,
            "observation": ''
        }
    ],
    "goodness": [
        {
            "id": 25,
            "description": 'En el predio se evita el maltrato y el dolor en los animales de acuerdo a las cinco libertades (que no sufran hambre ni sed, que no fugran injustificadamente malestar físico ni dolor, que no les sean provocadas enfermendades por negligencia o descuido, que no sean sometidos a condiciones de miedo ni estrés y que puedan manifestar su comportamiento natural',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 26,
            "description": 'Los animales disponen de agua de bebida a voluntad y alimentos en condiciones higiénicas que no efecten la salud de los animales ni la inocuidad de la carne',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 27,
            "description": 'Los bebederos permanecen limpios y en buen estado',
            "maxScore": 5,
            "observation": ''
        },
        {
            "id": 28,
            "description": 'El personal vinculado, recibe entrenamiento en los temas relacionados con el desempeño de sus actividades en el predio. Se evidecia su conocimiento por medio de entrevista o soportes escritos',
            "maxScore": 5,
            "observation": ''
        }
    ]}