import { Coordinator } from './coordinator';
import { BaseProfile } from "./base-profile";
import { Entity, OneToMany } from "typeorm";

@Entity()
export class Manager extends BaseProfile {

  @OneToMany(type => Coordinator, coordinator => coordinator.manager)
  public coordinators: Coordinator[];

  public setData(data: Manager) {
    if(data) {
      super.setData(data);
    }
  }

}