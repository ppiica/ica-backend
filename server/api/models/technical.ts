import { Visit } from './visit';
import { Column, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn, RelationId, PrimaryColumn } from 'typeorm';
import { Entity } from 'typeorm';
import { BaseProfile } from "./base-profile";
import { Coordinator } from './coordinator';

@Entity()
export class Technical extends BaseProfile {

  @Column({ length: 45 })
  public professionalRegistration: string;

  @RelationId((technical: Technical) => technical.coordinator)
  @Column( { nullable: true, length: 12 } )
  public coordinatorId?: string;

  @ManyToOne(type => Coordinator, coordinator => coordinator.technicians)
  public coordinator: Coordinator;

  @OneToMany(type => Visit, visit => visit.technical)
  public visits: Visit[];

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  public setData(data: Technical) {
    if (data) {
      super.setData(data);
      if (data.professionalRegistration) this.professionalRegistration = data.professionalRegistration;
      if (data.coordinatorId) this.coordinatorId = data.coordinatorId;
    }
  }

}