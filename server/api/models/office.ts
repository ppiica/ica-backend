import { Coordinator } from './coordinator';
import { Farm } from './farm';
import { Entity, Column, PrimaryColumn, getRepository, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Office {

  constructor(data?: any) {
    if (data) {
      this.setData(data);
    }
  }

  @PrimaryColumn()
  public id: string;

  @Column()
  public name: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @OneToMany(type => Farm, farm => farm.office)
  public farms: Farm[];

  @OneToMany(type => Coordinator, coordinator => coordinator.office)
  public coordinators: Coordinator[];

  public setData(data) {    
    if (data.id) this.id = data.id;
    if (data.name) this.name = data.name;
    if (typeof data.isRemove === 'boolean') {
      this.isRemove = data.isRemove;
    }
}

}
