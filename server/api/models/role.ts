import { PrimaryGeneratedColumn } from 'typeorm';
import { RoleForm } from './role-form';
import { User } from './user';
import { OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class Role {

  @PrimaryGeneratedColumn({ type: 'int' })
  public id: number;

  @Column({ length: 45, unique: true })
  public description: string;

  @OneToMany(type => User, user => user.role)
  public users: User[];

  @OneToMany(type => RoleForm, roleForm => roleForm)
  public roleForms: RoleForm[];

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;
}