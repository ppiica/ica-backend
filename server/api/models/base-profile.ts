import { Column, CreateDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { PrimaryColumn } from 'typeorm';
import { Entity } from 'typeorm';

@Entity()
export abstract class BaseProfile {

  @PrimaryColumn({ length: 12 })
  public id: string;

  @Column({ length: 45 })
  public firstName: string;

  @Column({ length: 45 })
  public lastNameOne: string;

  @Column({ nullable: true, length: 45 })
  public lastNameTwo?: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({asExpression: "concat(`firstName`, ' ', `lastNameOne`, IFNULL(concat(' ', `lastNameTwo`), ''))"})
  public fullName: string;

  public setData(data: BaseProfile) {
    if (data.id) this.id = data.id;
    if (data.firstName) this.firstName = data.firstName;
    if (data.lastNameOne) this.lastNameOne = data.lastNameOne;
    if (data.lastNameTwo) this.lastNameTwo = data.lastNameTwo;
  }

}