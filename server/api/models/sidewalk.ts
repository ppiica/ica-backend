import { Farm } from './farm';
import { Entity, PrimaryColumn, Column, OneToMany, ManyToOne, CreateDateColumn, UpdateDateColumn, RelationId } from 'typeorm';
import { Municipality } from './municipality';

@Entity()
export class Sidewalk {

  @PrimaryColumn({ type: 'int' })
  public id: number;

  @Column({ length: 50 })
  public name: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @ManyToOne(type => Municipality, municipality => municipality.sidewalks)
  public municipality?: Municipality;

  @OneToMany(type => Farm, farm => farm.sidewalk)
  public farms: Farm[];

  @RelationId((sidewalk: Sidewalk) => sidewalk.municipality)
  @Column()
  public municipalityId: number;

}