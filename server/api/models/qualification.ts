import { Visit } from './visit';
import { Item } from './item';
import { PrimaryColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, RelationId, PrimaryGeneratedColumn } from 'typeorm';
import { Entity, Column, getRepository } from 'typeorm';

@Entity()
export class Qualification {

  @PrimaryGeneratedColumn({ type: 'int' })
  public id: number;

  @Column({ type: 'double' })
  public score: number;

  @Column({ nullable: true, length: 1000 })
  public observations?: string;

  @Column({ nullable: true, length: 500 })
  public evidence?: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @ManyToOne(type => Item, item => item.qualifications)
  public item: Item;

  @ManyToOne(type => Visit, visit => visit.qualifications)
  public visit: Visit;

  @RelationId((qualification: Qualification) => qualification.item)
  @Column()  
  public itemId: number;

  @RelationId((qualification: Qualification) => qualification.visit)
  @Column()  
  public visitId: number;

}
