import { Item } from './item';
import { Entity, PrimaryColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Category {

  @PrimaryColumn({ type: 'int' })
  public id: number;

  @Column({ length: 1000 })
  public description: string;

  @Column({ type: 'double' })
  public maxScore: number;

  @Column({ type: 'double' })
  public categoryPercent: number;

  @OneToMany(type => Item, item => item.category)
  public items: Item[];

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

}