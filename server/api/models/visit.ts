import { Farm } from './farm';
import { VisitStatus } from './visitStatus';
import { getRepository, Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn, RelationId } from 'typeorm';
import { Qualification } from './qualification';
import { ProductiveActivity } from './productive-activity';
import { Technical } from './technical';

@Entity()
export class Visit {

  constructor(data?: any) {
    if (data) {
      this.setData(data);
    }
  }

  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public date: Date;

  @Column({nullable: true})
  public nextVisitDate: Date;

  @Column({nullable: true})
  public observations?: string;

  @Column({nullable: true})
  public gradualCompliancePlan?: string;

  @Column({nullable: true})
  public ownerFarmSign?: string;

  @Column({nullable: true})
  public inspectorSign?: string;

  @RelationId((visit: Visit) => visit.farm )
  @Column()  
  public farmRegisterNumber: number;

  @RelationId((visit: Visit) => visit.status )
  @Column()  
  public statusId: number;

  @RelationId((visit: Visit) => visit.technical )
  @Column()  
  public technicalId: string;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @ManyToOne(type => VisitStatus, visitStatus => visitStatus.visits)
  public status: VisitStatus;

  @OneToMany(type => ProductiveActivity, productiveActivity => productiveActivity.visit)
  public productiveActivities: ProductiveActivity[];

  @OneToMany(type => Qualification, qualification => qualification.visit)
  public qualifications: Qualification[];

  @ManyToOne(type => Technical, technical => technical.visits)
  public technical: Technical;

  @ManyToOne(type => Farm, farm => farm.visits)
  public farm: Farm;

  @Column({ type: 'int', nullable: true })
  public personWhoAttendsId: number;

  @Column({ length: 150, nullable: true })
  public personWhoAttendsName: string;

  @Column({ type: 'int', nullable: true })
  public personWhoAttendsPhone: number;

  public setData(data) {
    if (data.id) this.id = data.id;
    if (data.date) this.date = data.date;
    if (data.nextVisitDate) this.nextVisitDate = data.nextVisitDate;
    if (data.observations) this.observations = data.observations;
    if (data.gradualCompliancePlan) this.gradualCompliancePlan = data.gradualCompliancePlan;
    if (data.ownerFarmSign) this.ownerFarmSign = data.ownerFarmSign;
    if (data.inspectorSign) this.inspectorSign = data.inspectorSign;
    if (data.farmRegisterNumber) this.farmRegisterNumber = data.farmRegisterNumber;
    if (data.statusId) this.statusId = data.statusId;
    if (data.technicalId) this.technicalId = data.technicalId;
    if (data.isRemove !== null && data.isRemove !== undefined) this.isRemove = data.isRemove;
    if (data.personWhoAttendsId) this.personWhoAttendsId = data.personWhoAttendsId;
    if (data.personWhoAttendsName) this.personWhoAttendsName = data.personWhoAttendsName;
    if (data.personWhoAttendsPhone) this.personWhoAttendsPhone = data.personWhoAttendsPhone;
  }

}
