import { Municipality } from './municipality';
import { Entity, PrimaryColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';

/**
 * Department entity  
 * 
 * @export
 * @class Department
 */
@Entity()
export class Department {

  /**
   * Department identification
   * 
   * @type {number}
   * @memberof Department
   */
  @PrimaryColumn({ type: 'int' })
  public id: number;

  /**
   * Department name
   * 
   * @type {string}
   * @memberof Department
   */
  @Column({ length: 50 })
  public name: string;

  /**
   * Date of department entity creation
   * 
   * @type {Date}
   * @memberof Department
   */
  @CreateDateColumn()
  public createdAt?: Date;

  /**
   * Date of department entity update
   * 
   * @type {Date}
   * @memberof Department
   */
  @UpdateDateColumn()
  public updatedAt?: Date;

  /**
   *  
   * 
   * @type {boolean}
   * @memberof Department
   */
  @Column({ default: false })
  public isRemove?: boolean;

  /**
   * 
   * 
   * @type {Department[]}
   * @memberof Department
   */
  @OneToMany(type => Municipality, municipality => municipality.department)
  public municipalities: Department[];

}