import { Office } from './office';
import { Technical } from './technical';
import { OneToMany, Column, RelationId } from 'typeorm';
import { Manager } from './manager';
import { Entity, ManyToOne } from 'typeorm';
import { BaseProfile } from './base-profile';
import L from "../../common/logger";


@Entity()
export class Coordinator extends BaseProfile {

  @ManyToOne(type => Manager, manager => manager.coordinators)
  public manager?: Manager;

  @ManyToOne(type => Office, office => office.coordinators)
  public office?: Office;

  @OneToMany(type => Technical, technical => technical.coordinator)
  public technicians?: Technical[];

  @RelationId((coordinator: Coordinator) => coordinator.manager)
  @Column({nullable:true, length: 12})
  public managerId: string;

  @RelationId((coordinator: Coordinator) => coordinator.office)
  @Column()  
  public officeId: number;

  public setData(data: Coordinator) {
    if (data) {
      super.setData(data);
      if (data.officeId) this.officeId = data.officeId;
      if (data.managerId) this.managerId = data.managerId;
    }
  }
}