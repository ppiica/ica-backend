import { Visit } from './visit';
import { Specie } from './specie';
import { Column, PrimaryColumn, getRepository, ManyToOne, CreateDateColumn, UpdateDateColumn, RelationId } from 'typeorm';
import { Qualification } from './qualification';
import { Entity } from 'typeorm';


@Entity()
export class ProductiveActivity {

  @Column({ type: 'int', nullable: true })
  public breed?: number;

  @Column({ type: 'int', nullable: true })
  public lift?: number;

  @Column({ type: 'int', nullable: true })
  public fatten?: number;

  @Column({ type: 'int', nullable: true })
  public fullCycle?: number;

  @CreateDateColumn()
  public createdAt?: Date;

  @UpdateDateColumn()
  public updatedAt?: Date;

  @Column({ default: false })
  public isRemove?: boolean;

  @ManyToOne(type => Specie, specie => specie.productiveActivities, { primary: true })
  public specie: Specie;

  @ManyToOne(type => Visit, visit => visit.productiveActivities, { primary: true })
  public visit: Visit;

  @RelationId((productiveActivity: ProductiveActivity) => productiveActivity.specie)
  @PrimaryColumn()
  public specieId: number;

  @RelationId((productiveActivity: ProductiveActivity) => productiveActivity.visit)
  @PrimaryColumn()
  public visitId: number;

}
