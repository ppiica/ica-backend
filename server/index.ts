import { Utils } from './api/utils/utils';
import { Connection } from 'typeorm';
import "reflect-metadata";
import { initDB } from './api/database';
import './common/env';
import Server from './common/server';
import routes from './routes';
// import * as mongoose from "mongoose";
import * as express from "express";
import { sendEmail } from "./api/utils/email";
import { seedDB } from './api/database/db.seeder';

// sendEmail('pernett98@gmail.com', 'HI', 'HO')
// .then(console.log).catch(console.error);

initDB()
  .then((connection: Connection) => {
    seedDB(connection)
      .then(() => {
        console.log('db seeded');
      })
      .catch((e) => {
        console.error('error populating db: ', e);
      });
  });

  const port = parseInt(process.env.PORT);
  new Server()
  .router(routes)
  .listen(port);
  
  // Utils.generateUploadsFolder();

// mongoose.connect(process.env.MONGO_URL);
