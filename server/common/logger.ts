import * as pino from 'pino';

const l = pino({
  name: process.env.APP_ID,
  level: "debug",
});

export default l;
