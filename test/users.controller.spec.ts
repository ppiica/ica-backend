import 'mocha';
import { expect } from 'chai';
import * as request from 'supertest';
import Server from '../server';

describe('Users', () => {
  it('should get users empty', () =>
    request(Server)
      .get('/api/v1/users')
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object');
        expect(r.body.code)
          .equals(201);
      }));

  it('should create user', () => {
    request(Server)
      .post('/api/v1/users')
      .send({
        "id": "1000101010",
        "firstName": "Pablo",
        "lastNameOne": "Rios",
        "lastNameTwo": "Rios",
        "email": "Pablo@rmail.com",
        "phone": 1518438,
        "officeId": "25223",
        "idUserType": 0,
        "techniciansIds": [
          "252",
          "552"
        ]
      })
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body).to.be.an('object');
        it('should get users', () =>
          request(Server)
            .get('/api/v1/users')
            .expect('Content-Type', /json/)
            .then(r => {
              expect(r.body)
                .to.be.an('array');

              it('should get user with id: 1000101010', () =>
                request(Server)
                  .get('/api/v1/users/1000101010')
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.firstName)
                      .equals('Pablo');
                  }));

              it('should update user with id: 1000101010', () =>
                request(Server)
                  .put('/api/v1/users/1000101010')
                  .send({
                    firstName: 'Carlos'
                  })
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.updated)
                      .equals(true)

                    it('should get user with id: 1000101010', () =>
                      request(Server)
                        .get('/api/v1/users/1000101010')
                        .expect('Content-Type', /json/)
                        .then(r => {
                          expect(r.body)
                            .to.be.an('object');
                          expect(r.body.firstName)
                            .equals('Carlos');
                        }));
                  }));

              it('should remove user with id: 1000101010', () =>
                request(Server)
                  .put('/api/v1/users/1000101010')
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.removed)
                      .equals(true);
                  }));
            }));
      });
  });


});
