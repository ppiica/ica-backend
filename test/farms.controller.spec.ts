import 'mocha';
import { expect } from 'chai';
import * as request from 'supertest';
import Server from '../server';

describe('Farms', () => {
  it('should get farms empty', () =>
    request(Server)
      .get('/api/v1/farms')
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object');
        expect(r.body.code)
          .equals(205);
      }));

  it('should create farm', () => {
    request(Server)
      .post('/api/v1/farms')
      .send({
        "registerNumber": "101100",
        "name": "The Big farm",
        "latitude": 6.25184,
        "longitude": -75.56359,
        "area": 5000,
        "sidewalkId": "5",
        "ownerId": "1000101010",
        "officeId": "25223"
      })
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body).to.be.an('object');
        it('should get farms', () =>
          request(Server)
            .get('/api/v1/farms')
            .expect('Content-Type', /json/)
            .then(r => {
              expect(r.body)
                .to.be.an('array');

              it('should get farm with id: 101100', () =>
                request(Server)
                  .get('/api/v1/farms/101100')
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.name)
                      .equals('The Big farm');
                  }));

              it('should update farm with id: 101100', () =>
                request(Server)
                  .put('/api/v1/farms/101100')
                  .send({
                    firstName: 'The Big farm updated'
                  })
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.updated)
                      .equals(true)

                    it('should get farm with id: 101100', () =>
                      request(Server)
                        .get('/api/v1/farms/101100')
                        .expect('Content-Type', /json/)
                        .then(r => {
                          expect(r.body)
                            .to.be.an('object');
                          expect(r.body.firstName)
                            .equals('The Big farm updated');
                        }));
                  }));

              it('should remove farm with id: 101100', () =>
                request(Server)
                  .put('/api/v1/farms/101100')
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.removed)
                      .equals(true);
                  }));
            }));
      });
  });


});
