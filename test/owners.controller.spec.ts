import 'mocha';
import { expect } from 'chai';
import * as request from 'supertest';
import Server from '../server';

describe('Owners', () => {
  it('should get owners empty', () =>
    request(Server)
      .get('/api/v1/owners')
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body)
          .to.be.an('object');
        expect(r.body.code)
          .equals(201);
      }));

  it('should create owner', () => {
    request(Server)
      .post('/api/v1/owners')
      .send({
        "id": "1000101010",
        "firstName": "Pablo",
        "lastNameOne": "Rios",
        "lastNameTwo": "Rios",
        "email": "Pablo@rmail.com",
        "phone": 1518438,
        "address": "cr 65"
      })
      .expect('Content-Type', /json/)
      .then(r => {
        expect(r.body).to.be.an('object');
        it('should get owners', () =>
          request(Server)
            .get('/api/v1/owners')
            .expect('Content-Type', /json/)
            .then(r => {
              expect(r.body)
                .to.be.an('array');

              it('should get owner with id: 1000101010', () =>
                request(Server)
                  .get('/api/v1/owners/1000101010')
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.firstName)
                      .equals('Pablo');
                  }));

              it('should update owner with id: 1000101010', () =>
                request(Server)
                  .put('/api/v1/owners/1000101010')
                  .send({
                    firstName: 'Carlos'
                  })
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.updated)
                      .equals(true)

                    it('should get owner with id: 1000101010', () =>
                      request(Server)
                        .get('/api/v1/owners/1000101010')
                        .expect('Content-Type', /json/)
                        .then(r => {
                          expect(r.body)
                            .to.be.an('object');
                          expect(r.body.firstName)
                            .equals('Carlos');
                        }));
                  }));

              it('should remove owner with id: 1000101010', () =>
                request(Server)
                  .put('/api/v1/owners/1000101010')
                  .expect('Content-Type', /json/)
                  .then(r => {
                    expect(r.body)
                      .to.be.an('object');
                    expect(r.body.removed)
                      .equals(true);
                  }));
            }));
      });
  });


});
